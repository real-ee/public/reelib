#!/usr/bin/python3

import json
import sys
import getopt
import io
import datetime
import time
import copy
import operator
from collections import OrderedDict

#TODO
# if type = struct check special tag "struct" then print its detail
# add generation date to header file



class REEmember(object):
	def __init__(self, arg):
		super(REEmember, self).__init__()
		self.arg = arg
		
	VERBOSE =				False
	JSONfile =				'../doc/template_REEmember-Map.json'		# default input to template file 
	CHEADERfile =			'../doc/template_REEmember-map.h'			# default output to template file
	serialString =			''
	data =					{}
	compressKeyDict =		{}
	rev_compressKeyDict =	{}
	compressTypeDict =		{}
	rev_compressTypeDict =	{}


# --------------------------------------------- #
#												#
#				Constant						#
#												#
# --------------------------------------------- #
	# // ================================================== \\ #
	# == JSON keys
	# Required
	__keyRegArray =		'reg'
	__keyDict =			'dict'
	__keyType =			'type'

	# Required for each register
	__keyName =			'name'
	__keyPerm =			'perm'
	__keyDesc =			'desc'

	# Optional
	__keyStruct =		'struct'
	__keyVersion =		'version'
	__keyKey =			'key'
	__keyBit =			'bit'
	__keyTime =			'time'
	__keyNumber =		'number'
	__keyModule =		'module'

	__requiredTopKeys =	{__keyRegArray, __keyName, __keyType}	# Required key at top level of JSON, if not found, nothing can be done
	__requiredRegKeys =	{__keyName, __keyType}					# Minimum required for a valid register definition
	__unsafeCharDict =	{	'-':	'_',
							' ':	'_',
							'+':	'_',
							'*':	'_',
							'/':	'_',
							'\\':	'_',
							',':	'_',
							'.':	'_',
							'{{':	'_',
							'}':	'_',
							'[':	'_',
							']':	'_',
							'(':	'_',
							')':	'_'}
	# \\ ================================================== // #

	# // ================================================== \\ #
	# == Display "graphics"			...oooooooooooooh yeah!
	dispHeaderBar =			'='
	dispHeaderStart =		'/'
	dispHeaderMid =			'='
	dispHeaderStop =		'\\'

	dispDelimiterBar =		'-'
	dispDelimiterStart =	'|'
	dispDelimiterMid =		'|'
	dispDelimiterStop =		'|'

	dispFooterBar =			'='
	dispFooterStart =		'\\'
	dispFooterMid =			'='
	dispFooterStop =		'/'

	dispEdge =				'|'
	dispSpacer =			' '

	dispTitleAddress =		'Address'
	dispTitleAccess =		'RW Type'
	dispTitleName =			'Name'
	dispTitleDesc =			'Description'

	dispAddressWidth =		5+6+3		# 5 for decimal, 6 for hex, 3 spacer
	dispAccessTypeWidth =	2+16+3		# 2 for access, 16 for type, 3 spacer
	dispMinWidth =			5
	# \\ ================================================== // #

	# // ================================================== \\ #
	__constReadPerm =		'R'
	__constWritePerm =		'W'
	__constNoPerm =			' '
	# \\ ================================================== // #

	# // ================================================== \\ #
	CHstring_TopProtect =		'#ifndef __{0}_H__\n#define __{0}_H__\t1\n#ifdef __cplusplus\n\textern "C" {{\n#endif\n'
	CHstring_BotProtect =		'#ifdef __cplusplus\n}\n#endif\n#endif\n\n\n'

	CHstring_header_serialObjDeclaration ='static const uint8_t REEmember_{0}[] ="'
	CHstring_headerStart =		'/*\n'
	CHstring_headerStop =		'*/\n'
	CHstring_header = 			''
	_CHheader_maxKeyWidth =		32

	CHstring_headerLicense =	'All right reserved RealEE inc. ('+str(datetime.datetime.now().year)+')\n'

	CHstring_blockInclude =		'/* -------------------------------- +\n|\t\t\t\t\t\t\t\t\t|\n|\tInclude\t\t\t\t\t\t\t|\n|\t\t\t\t\t\t\t\t\t|\n+ ---------------------------------*/\n'
	CHstring_blockType =		'/* -------------------------------- +\n|\t\t\t\t\t\t\t\t\t|\n|\tType\t\t\t\t\t\t\t|\n|\t\t\t\t\t\t\t\t\t|\n+ ---------------------------------*/\n'
	CHstring_blockConstant =	'/* -------------------------------- +\n|\t\t\t\t\t\t\t\t\t|\n|\tConstant\t\t\t\t\t\t|\n|\t\t\t\t\t\t\t\t\t|\n+ ---------------------------------*/\n'
	CHstring_blockGlobal =		'/* -------------------------------- +\n|\t\t\t\t\t\t\t\t\t|\n|\tGlobal\t\t\t\t\t\t\t|\n|\t\t\t\t\t\t\t\t\t|\n+ ---------------------------------*/\n'
	CHstring_includes =			'#include <stdint.h>\n'
	CHstring_serialObjSize =	'#define\t{0}\t\t\t\t{1}\t\t\t/* {2} */\n'
	# \\ ================================================== // #


# --------------------------------------------- #
#												#
#				Private methods					#
#												#
# --------------------------------------------- #
	def __replaceKey(self, target=None, source=None):
		try:
			result = dict((source[key], value) for (key, value) in target.items())
			print(result)
			return result
		except Exception as e:
			raise e
		#TODO: This is untested!!!

	def __replaceValue(self, target=None, source=None):
		try:
			for tKey, tValue in target.items():
				for key, value in source.items() :
					#print('TARGET key: {} | val: {}\nTABLE key: {} | val: {}'.format(tKey, tValue, key, value))
					if tValue == key:
						print('INF: replacing {0} by {1}'.format(tValue, value))
						target[tKey] = value
						#tValue = value
			return target
		except Exception as e:
			raise e

	def __getRegSize(self, reg=None):
		size = 0
		# Check the type of this register
		try:
			regType = reg[self.__keyType]
		except Exception:
			regType = 'void'

		# Return the complete size of this reg
		if regType == 'struct':
			if self.VERBOSE:
				print('DBG: Struct found counting elements'.format(reg))
			for element in reg['struct']:
				if self.VERBOSE:
					print('DBG:\telement {0} '.format(element))
				size += self.__getRegSize(element)
		elif regType == 'bool':
			size = 1
		return size

	def __getSafeName(self, data=None):
		if data is None:
			data = self.data

		resultStr = str(data[self.__keyName])
		for problem, solution in self.__unsafeCharDict.items():
			resultStr = resultStr.replace(problem, solution)
		return resultStr
	
	def __timeStamp(self, data=None):
		if (data is None) and (self.data is not None):
			data = self.data
		else:
			return
		ts = int(datetime.datetime.now().timestamp())
		data[self.__keyTime] = ts
		if self.VERBOSE:
			print('INF: Timestamp @ {}'.format(ts))
		return ts

	def __extractDict(self, data=None):
		try:
			self.compressKeyDict = data[self.__keyDict]
			self.rev_compressKeyDict = {v: k for k, v in self.compressKeyDict.items()}
		except Exception as e:
			if self.VERBOSE:
				raise e
			else:
				print('ERR: Dictionnary not found')
			return False

		if self.VERBOSE:
			print('INF: Key dict found: '+str(self.compressKeyDict))
			print('DBG: Reverse key dict: '+str(self.rev_compressKeyDict))
		return True

	def __extractType(self, data=None):
		try:
			compressedKeyList = list( data[self.__keyType].values())
			keyList = list(data[self.__keyType].keys())
			for i in range(len(keyList)):
				self.compressTypeDict[keyList[i]] = compressedKeyList[i]
				self.rev_compressTypeDict[compressedKeyList[i]] = keyList[i]
		except Exception as e:
			if self.VERBOSE:
				raise e
			else:
				print('ERR: Valid types not found')
			return False

		if self.VERBOSE:
			print('INF: Type dict: '+str(self.compressTypeDict))
			print('DBG: Reverse type dict: '+str(self.rev_compressTypeDict))
		return True

	def __normalizeKeys(self, data=None):

		for key,value in data.items():
			if (key == self.__keyDict) or ((key == self.__keyType) and (self.__keyRegArray in data)):
				if self.VERBOSE:
					print('{} ignored'.format(key))
			else:
				if isinstance(value, OrderedDict):
					if self.VERBOSE:
						print('Possible reg found')
					self.__normalizeKeys(value)
				elif isinstance(value, list):
					if self.VERBOSE:
						print('Multi-instance register found')
					for item in value:
						if isinstance(value, OrderedDict):
							if self.VERBOSE:
								print('Possible reg found')
							self.__normalizeKeys(item)
		
	def __normalizeTypes(self, data=None):
		if (data is None):
			data = self.data

		errorFlag = False
		validFlag = False
		if self.__keyRegArray in data:
			# -- Verify type
			for reg in data[self.__keyRegArray]:
				if self.VERBOSE:
					self.__cutePrint(reg)



				validFlag = False
			# # - Replace compressed type by expanded one (unification)
			# regArray[i] = self.__replaceValue(target=regArray[i], table=self.rev_compressTypeDict)
			# # - Test all types
			# for testType in self.compressTypeDict:
			# 	#print('IF '+str(regArray[i][self.__keyType])+' == '+str(testType))
			# 	if regArray[i][self.__keyType] == testType:
			# 		validFlag = True
			# # - Did we found a known type?
			# if validFlag == False:
			# 	print('ERR:No valid type found in reg '+str(i))
			# 	errorFlag = True
		if self.VERBOSE and errorFlag == False:
			print('INF: Valid type found for each register')

	def __normalizePerms(self, data=None, parentPerm=None):
		# If given the top level, ensure we get to the first register correctly
		if self.__keyRegArray in data:
			data = data[self.__keyRegArray]

		# Normalize all the registers
		for reg in data:
			# -- Sanitizing
			if self.__keyPerm in reg:
				readAccess = reg[self.__keyPerm].count(self.__constReadPerm.lower()) + reg[self.__keyPerm].count(self.__constReadPerm.upper())
				writeAccess = reg[self.__keyPerm].count(self.__constWritePerm.lower()) + reg[self.__keyPerm].count(self.__constWritePerm.upper())
				if readAccess > 0:
					reg[self.__keyPerm] = self.__constReadPerm
				else:
					reg[self.__keyPerm] = self.__constNoPerm

				if writeAccess > 0:
					reg[self.__keyPerm] += self.__constWritePerm
				else:
					reg[self.__keyPerm] += self.__constNoPerm

			# -- Inheritance
			# A. No permission from parent and in register definition
			if (parentPerm is None) and not(self.__keyPerm in reg):
				reg[self.__keyPerm] = self.__constNoPerm + self.__constNoPerm	# No perms at all
			# B. Current level inherit parent's permission
			elif not(self.__keyPerm in reg):
				reg[self.__keyPerm] = str(parentPerm)

			# -- Recursivity
			if reg[self.__keyType] == 'struct':
				self.__normalizePerms(reg['struct'], reg[self.__keyPerm])

	def __cutePrint(self, data=None):
		if data is None:
			data = self.data
		print(json.dumps(data, indent=4))

	def __usage(self):
		print('IMPLEMENT usage guide')

	def __loadModule(self, data=None):
		if data is None:
			data = self.data
		regArr = data[self.__keyRegArray]
			
		count = 0
		for n, reg in enumerate(regArr):
			if self.__keyModule in reg:
				path = reg[self.__keyModule]
				
				# -- Open file
				if self.VERBOSE:
					print('DBG: loading module {}'.format(path))
				try:
					file = open(path).read()
					newRegArr = json.loads(file, object_pairs_hook=OrderedDict)
					if self.VERBOSE:
						print('DBG: module loaded:')
						self.__cutePrint(newRegArr)
					count += 1
				except Exception as e:
					print('ERR: failed to load {}'.format(path))
					if self.VERBOSE:
						raise e
					else:
						sys.exit(-1)

				# -- valide and concat
				if self.validateJSONformat(newRegArr):
					# add module's entry in global dict and remove local one
					if self.__keyDict in newRegArr:
						for newKey in newRegArr[self.__keyDict]:
							if newKey not in data[self.__keyDict]:
								data[self.__keyDict][newKey] = newRegArr[self.__keyDict][newKey]
								if self.VERBOSE:
									print('INF: adding new compressed key: \'{}\':\'{}\''.format(newKey, newRegArr[self.__keyDict][newKey]))
						newRegArr.pop(self.__keyDict)

					# add module's entry in type list and remove local one
					if self.__keyType in newRegArr:
						for newType in newRegArr[self.__keyType]:
							if newType not in data[self.__keyType]:
								data[self.__keyType][newType] = newRegArr[self.__keyType][newType]
								if self.VERBOSE:
									print('INF: adding new type: \'{}\':\'{}\''.format(newType, newRegArr[self.__keyType][newType]))
						newRegArr.pop(self.__keyType)

					# replace some module's internal key
					newRegArr[self.__keyType] = 'struct'			#TODO should be in constants or something
					newRegArr[self.__keyStruct] = newRegArr[self.__keyRegArray]
					newRegArr.pop(self.__keyRegArray, None)
					regArr[n] = newRegArr

		return count

# --------------------------------------------- #
#												#
#				Private sub-routine				#
#												#
# --------------------------------------------- #
	def _CHconstructHeader(self, headerDict=None):
		if headerDict is not None:
			string = self.CHstring_headerStart
			widthMin = self._CHheader_maxKeyWidth
			for key,val in headerDict.items():
				string += '\t@'+str(key)
				widthMin = min(len(str(key)), widthMin)

				if len(str(key)) <= 8:		# TODO: alignment is all broken...
					string += '\t'
				if len(str(key)) <= 4:
					string += '\t'
				string += str(val)+'\n'
			string += self.CHstring_headerStop
			if self.VERBOSE:
				print('DBG: CH-header:\n'+string)
			return string

	def _CHconstructTypedef(self, data=None):
		if data is None:
			return '/* Invalid data:{0} */'.format(str(data))

		# -- Compute some stats
		regNumber = len(data[self.__keyRegArray])
		# Compute struct size
		structSize = 0
		for reg in data[self.__keyRegArray]:
			structSize += self.__getRegSize(reg)
		if self.VERBOSE:
			print('INF: Total struct size of {0}B'.format(structSize))

		# select smallest address type necessary
		regNb = len(data[self.__keyRegArray])
		addressType = 'uint8_t'
		if (regNb > 255) and (regNb <= 65535):
			addressType = 'uint16_t'
		elif regNb > 65535:
			addressType = 'uint32_t'

		if self.VERBOSE:
			print('INF: {0} register found, using {1} for reg address'.format(regNb,addressType))

		# -- Prepare strings
		str_header = 'typedef union _{0}_u {{\n'.format(self.__getSafeName())
		str_byteArray = '\tuint8_t\t_byte[{0}];\n\tstruct __attribute__((packed)){{\n'.format('')
		str_endStruct = '\n\t};\n'
		str_footer = '}}{0}_t;\n'.format(self.__getSafeName())
		str_struct = ''

		# -- Go trough all register definitions
		for reg in data[self.__keyRegArray]:
			if self.VERBOSE:
				print('DBG: Adding register: {0}'.format(reg))

			# - Special handling for lists (arrays)
			try:
				if reg[self.__keyNumber] in reg:
					print('IMPLEMENT List definition!')
			except Exception as e:
				pass
			
			# - Special handling for struct
			if reg[self.__keyType] == 'struct':
				print('IMPLEMENT Struct exploration!')
			else:
				str_struct += '\t\t{0}\t\t{1};\n'.format(reg[self.__keyType], reg[self.__keyName])

		return str_header + str_byteArray + str_struct + str_endStruct + str_footer


# --------------------------------------------- #
#												#
#				Public API						#
#												#
# --------------------------------------------- #
	"""
	@brief		Check the format and values of the input JSON
				Will report to stdout 
	"""
	def validateJSONformat(self, data=None):
		if data is None:
			data = self.data
		errorFlag = False

		# 0. -- Find required keys
		for keys in self.__requiredTopKeys:
			try:
				test = data[keys]
			except Exception as e:
				print('ERR:\''+keys+'\' not found at top level')
				return False
		if self.VERBOSE:
			print('INF: All required keys have been found')

		regArray = data[self.__keyRegArray]
		regNb = len(regArray)

		# 1. -- Extract dictionnary
		if self.__keyDict in data:
			if not self.__extractDict(data):
				errorFlag = True

		# 2. -- Extract valid types
		if not self.__extractType(data):
			errorFlag = True

		# 3. -- Normalize keys
		self.__normalizeKeys(data)
		if self.VERBOSE:
			print('INF: Keys have been normalized')

		# 3.5 -- Load modules
		bkup = self.VERBOSE
		self.VERBOSE = True
		modCnt = self.__loadModule(data)
		if self.VERBOSE:
			print('INF: {} module loaded'.format(modCnt))
		self.VERBOSE = bkup

		# 4. -- Normalize types
		self.__normalizeTypes(data)
		if self.VERBOSE:
			print('INF: Types have been normalized')

		# 5. -- Normalize perm
		self.__normalizePerms(data)
		if self.VERBOSE:
			print('INF: Permission have been normalized')

		# 6. -- Check for all required registers key
		for i in range(regNb):
			for keys in self.__requiredRegKeys:
				try:
					test = regArray[i][keys]
				except Exception as e:
					print('ERR:\''+keys+'\' not found in reg '+str(i))
					errorFlag = True

		if errorFlag == True:
			return False		#early stop as we didn't find all required keys
		else:
			if self.VERBOSE:
				print('INF: All register required keys have been found')

		# 7. -- Check that all reg info are valid
		for i in range(regNb):
			if regArray[i][self.__keyType] == 'struct':
				try:
					test = regArray[i]['struct']
				except Exception as e:
					print('ERR: No \'struct\' definition found in reg '+str(i))
					errorFlag = True

		if self.VERBOSE and errorFlag == False:
			print('INF: All struct have definition')
		


		if errorFlag:
			return False
		else:
			self.__timeStamp(data)		# Timestamp when we are sure this is a valid REEmember definition
			return True

	"""
	@brief		Replace the keys and values with smaller one using the internal dictionnaries
	"""
	def compress(self, data=None):
		if data is None:
			return
		cdata = copy.deepcopy(data)

		#TODO: This should be iterative and go through n depth tree, not just 2 level...
		# Top level
		tagListTop = list(cdata.keys())
		for keyId in range(len(cdata)):
			try:
				newKeyTop = self.compressKeyDict[tagListTop[keyId]]
			except Exception as e:
				newKeyTop = None
				if self.VERBOSE:
					print('DBG: \''+str(tagListTop[keyId])+'\' uncompressable')
			if newKeyTop is not None:
				if self.VERBOSE:
					print('DBG: Compressing \''+str(tagListTop[keyId])+'\' with \''+str(newKeyTop)+'\'')
				cdata[newKeyTop] = cdata.pop(tagListTop[keyId])

		# Compress each register definition
		if cdata[self.compressKeyDict[self.__keyRegArray]] != None:
			for regId in range(len(cdata[self.compressKeyDict[self.__keyRegArray]])):
				# - Compress known keys
				tagList = list(cdata[self.compressKeyDict[self.__keyRegArray]][regId].keys())
				for i in range(len(tagList)):
					try:
						newKeyReg = self.compressKeyDict[tagList[i]]
					except Exception as e:
						newKeyReg = None
						if self.VERBOSE:
							print('DBG: \''+str(tagList[i])+'\' uncompressable')
						
					if newKeyReg is not None:
						if self.VERBOSE:
							print('DBG: Compressing \''+str(tagList[i])+'\' with \''+str(newKeyReg)+'\'')
						cdata[self.compressKeyDict[self.__keyRegArray]][regId][newKeyReg] = cdata[self.compressKeyDict[self.__keyRegArray]][regId].pop(tagList[i])

				# - compress known types
				try:
					compressedType = self.compressTypeDict[cdata[self.compressKeyDict[self.__keyRegArray]][regId][self.compressKeyDict[self.__keyType]]]
					if self.VERBOSE:
						print('DBG: Compressing type \'{0}\' with \'{1}\''.format(cdata[self.compressKeyDict[self.__keyRegArray]][regId][self.compressKeyDict[self.__keyType]], compressedType))
				except Exception as e:
					compressedType = None

				if compressedType is not None:
					cdata[self.compressKeyDict[self.__keyRegArray]][regId][self.compressKeyDict[self.__keyType]] = compressedType
				

		return cdata

	"""
	@brief		Return a stripped down escaped string of data 
	"""
	def serialize(self, data=None):
		if data is None:
			return ''
	
		return str(json.dumps(data)).replace(', ',',').replace(': ',':').translate(str.maketrans({'"': r"\""}))

	"""
	@brief		Return a fancy representation of the parsed data
	"""
	def getRegStructAsFancyString(self, data=None):
		if data is None:
			return ''

		# -- make some measurements
		maxNameWidth = self.dispMinWidth
		maxDescWidth = self.dispMinWidth
		for i in range(0, len(data[self.__keyRegArray])):
			maxNameWidth = max(len(data[self.__keyRegArray][i][self.__keyName]), maxNameWidth)
			if self.__keyDesc in data[self.__keyRegArray][i]:
				maxDescWidth = max(len(data[self.__keyRegArray][i][self.__keyDesc]), maxDescWidth)
		maxNameWidth += 2	# to account for spacing
		maxDescWidth += 2	# to account for spacing
		totalWidth = (self.dispAddressWidth+2) + (self.dispAccessTypeWidth+2) + (maxNameWidth+1) + (maxDescWidth+1) +1	 # +1 for start edge, +3 for spacer, mid edge and stop edge

		# -- Prepare graphic! elements
		dispHeader =		self.dispHeaderStart
		dispFooter =		self.dispFooterStart
		dispDelimiter =		self.dispDelimiterStart
		dispColumnTitle =	self.dispEdge
		# 0. Address
		for i in range(0, self.dispAddressWidth):
			dispHeader +=	self.dispHeaderBar
			dispFooter +=	self.dispFooterBar
			dispDelimiter +=self.dispDelimiterBar
		dispHeader +=		self.dispHeaderMid
		dispFooter +=		self.dispFooterMid
		dispDelimiter +=	self.dispDelimiterMid
		dispColumnTitle +=	self.dispSpacer + self.dispTitleAddress
		for i in range(1, min(self.dispAddressWidth-len(self.dispTitleAddress), self.dispAddressWidth)):	# 1 to account for the spacer
			dispColumnTitle += self.dispSpacer
		dispColumnTitle +=	self.dispEdge
		# 1. Access and Type
		for i in range(0, self.dispAccessTypeWidth):
			dispHeader +=	self.dispHeaderBar
			dispFooter +=	self.dispFooterBar
			dispDelimiter +=self.dispDelimiterBar
		dispHeader +=		self.dispHeaderMid
		dispFooter +=		self.dispFooterMid
		dispDelimiter +=	self.dispDelimiterMid
		dispColumnTitle +=	self.dispSpacer + self.dispTitleAccess
		for i in range(1, min(self.dispAccessTypeWidth-len(self.dispTitleAccess), self.dispAccessTypeWidth)):	# 1 to account for the spacer
			dispColumnTitle += self.dispSpacer
		dispColumnTitle +=	self.dispEdge
		# 2. Name
		for i in range(0, maxNameWidth):
			dispHeader +=	self.dispHeaderBar
			dispFooter +=	self.dispFooterBar
			dispDelimiter +=self.dispDelimiterBar
		dispHeader +=		self.dispHeaderMid
		dispFooter +=		self.dispFooterMid
		dispDelimiter +=	self.dispDelimiterMid
		dispColumnTitle +=	self.dispSpacer + self.dispTitleName
		for i in range(1, min(maxNameWidth-len(self.dispTitleName), maxNameWidth)):	# 1 to account for the spacer
			dispColumnTitle += self.dispSpacer
		dispColumnTitle +=	self.dispEdge
		# 3. Description
		for i in range(0, maxDescWidth):
			dispHeader +=	self.dispHeaderBar
			dispFooter +=	self.dispFooterBar
			dispDelimiter +=self.dispDelimiterBar
		dispHeader +=		self.dispHeaderStop
		dispFooter +=		self.dispFooterStop
		dispDelimiter +=	self.dispDelimiterStop
		dispColumnTitle +=	self.dispSpacer + self.dispTitleDesc
		for i in range(1, min(maxDescWidth-len(self.dispTitleDesc), maxDescWidth)):	# 1 to account for the spacer
			dispColumnTitle += self.dispSpacer
		dispColumnTitle +=	self.dispEdge


		# -- Assemble all the things!
		completeString = dispHeader + '\n'
		# Centered title is where its at!
		completeString += self.dispEdge + self.dispSpacer
		for i in range(1, int(min(totalWidth-(len(data[self.__keyName])+4), totalWidth)/2)):
			completeString += self.dispSpacer
		completeString += data[self.__keyName]
		for i in range(2, int(min(totalWidth-(len(data[self.__keyName])+4), totalWidth)/2)):
			completeString += self.dispSpacer
		if (len(data[self.__keyName]) & 1):
			completeString += self.dispSpacer
		completeString += self.dispSpacer + self.dispEdge + '\n'
		# Then we titled those straightness
		completeString += dispColumnTitle + '\n'
		completeString += dispDelimiter + '\n'						# Delimiter

		# And we show the registers!
		for i in range(1,len(data[self.__keyRegArray])):
			# Address
			completeString += self.dispEdge + self.dispSpacer + str(i).zfill(5) + self.dispSpacer + format(i, '#6x') + self.dispSpacer
			# Access
			completeString += self.dispEdge + self.dispSpacer + data[self.__keyRegArray][i][self.__keyPerm] + self.dispSpacer
			# Type
			completeString += str(data[self.__keyRegArray][i][self.__keyType])
			for j in range(2, min(self.dispAccessTypeWidth-(len(str(data[self.__keyRegArray][i][self.__keyType]))+2), self.dispAccessTypeWidth)):	# start at 2 to account for spacers (+2 for the access char)
				completeString += self.dispSpacer
			completeString += self.dispEdge
			# Name
			completeString += self.dispSpacer + str(data[self.__keyRegArray][i][self.__keyName]) + self.dispSpacer
			for j in range(0, min(maxNameWidth-(len(str(data[self.__keyRegArray][i][self.__keyName]))+2), maxNameWidth)):
				completeString += self.dispSpacer
			completeString += self.dispEdge
			# Description
			if self.__keyDesc in data[self.__keyRegArray][i]:
				completeString += self.dispSpacer + str(data[self.__keyRegArray][i][self.__keyDesc]) + self.dispSpacer
				descLen = len(str(data[self.__keyRegArray][i][self.__keyDesc]))
			else:
				descLen = 0
			for j in range(min(maxDescWidth-(descLen+2), maxDescWidth)):
				completeString += self.dispSpacer
			# Finished
			completeString += self.dispEdge + '\n'							# Finish the registry entry
			if i < len(data[self.__keyRegArray])-1:							# Skip last delimiter, its prettier
				completeString += dispDelimiter + '\n'						# Delimiter

		completeString += dispFooter

		# -- The anwser must be shared! At all cost!
		return completeString

	"""
	@brief		Create the C header file with the parsed information 
	"""
	def createCHeader(self, data=None):
		if data is None:
			return

		# -- Create the file if not present
		with io.open(self.CHEADERfile, 'wb') as outFile:
			if self.VERBOSE:
				print('DBG: Creating C Header')

			# Update the strings
			serialObjString = self.serialize(self.compress(data))
			serialObjSize = int(len(serialObjString)-(serialObjString.count('\\"')/2))
			if self.VERBOSE:
				print('DBG: Size: '+str(serialObjSize)+'B '+serialObjString)
			self.CHstring_header_serialObjDeclaration = self.CHstring_header_serialObjDeclaration.format(self.__getSafeName())
			self.CHstring_serialObjSize = self.CHstring_serialObjSize.format('REEmember_'+self.__getSafeName().upper()+'_SIZE','({0})'.format(serialObjSize),' Total size of the serialised object')
			self.CHstring_TopProtect = self.CHstring_TopProtect.format(self.__getSafeName().upper())
			headerDict = OrderedDict()
			headerDict['name'] = str(self.__getSafeName())+'.h'
			headerDict['brief'] = 'Register array definition for '+str(self.__getSafeName())
			headerDict['version'] = str(data[self.__keyVersion])
			headerDict['date'] = str(datetime.datetime.now().date())
			headerDict['author'] = 'toi'
			headerDict['note'] = 'je note'
			headerDict['license'] = self.CHstring_headerLicense
			self.CHstring_header = self._CHconstructHeader(headerDict)

			# 0 - Print header
			outFile.write(bytes(self.CHstring_header,'utf8'))

			# 1 - Print Top standard formating stuff
			outFile.write(bytes(self.CHstring_TopProtect,'utf8'))

			# 2 - Include
			outFile.write(bytes(self.CHstring_blockInclude,'utf8'))
			outFile.write(bytes(self.CHstring_includes,'utf8'))
			outFile.write(bytes('\n\n','utf8'))

			# 2 - Type
			outFile.write(bytes(self.CHstring_blockType,'utf8'))
			outFile.write(bytes(self._CHconstructTypedef(data),'utf8'))
			outFile.write(bytes('\n\n','utf8'))

			# 3 - Constant
			outFile.write(bytes(self.CHstring_blockConstant,'utf8'))
			outFile.write(bytes(self.CHstring_serialObjSize, 'utf8'))
			outFile.write(bytes('\n\n','utf8'))

			# 4 - Global
			outFile.write(bytes(self.CHstring_blockGlobal,'utf8'))
			outFile.write(bytes(self.CHstring_header_serialObjDeclaration+serialObjString+'";\n', 'utf8'))
			outFile.write(bytes('\n\n','utf8'))

			# 4 - Print Bottom standard formating stuff
			outFile.write(bytes(self.CHstring_BotProtect,'utf8'))

	"""
	@brief		Direct execution trap 
	"""
	def main(argv):
		self = REEmember(argv)

		# 0 ==== Handle arguments
		try:
			opts, args = getopt.getopt(argv,"hvi:o:",["verbose","ifile=","ofile="])
		except getopt.GetoptError:
			print('test.py -i <inputfile> -o <outputfile>')
			sys.exit(2)
		for opt, arg in opts:
			if opt == '-h':
				print('test.py -v -i <inputfile> -o <outputfile>')
				print(self.__usage())
				sys.exit()
			elif opt in ("-i", "--ifile"):
				self.JSONfile = arg
			elif opt in ("-o", "--ofile"):
				self.CHEADERfile = arg
			elif opt in ("-v", "--verbose"):
				self.VERBOSE = True
		if self.VERBOSE:
			print('Input file is \''+str(self.JSONfile)+'\'')
			print('Output file is \''+str(self.CHEADERfile)+'\'')


		# 1 ==== Open and extract JSON
		try:
			file = open(self.JSONfile).read()
			self.data = json.loads(file, object_pairs_hook=OrderedDict)
		except Exception as e:
			print('ERR: Could not open file \''+str(self.JSONfile)+'\'')
			if self.VERBOSE:
				raise e
			else:
				sys.exit(-1)

		# 2 ==== Validate data
		if not self.validateJSONformat(self.data):
			sys.exit(-2)

		# 2.5 Cute print
		print(self.getRegStructAsFancyString(self.data))

		# 3 ==== Create C representation
		self.createCHeader(self.data)


if __name__ == "__main__":
	REEmember.main(sys.argv[1:])

