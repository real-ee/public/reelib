/* Special deploy/distrib voodoo */
#ifndef __REELIB_DEV__
#warning "This is a template source file, useful for quick start and understanding how to use reelib, comment me when acknowledged"
#else
/*
	@name		template_source.c
	@author		Laurence DV
	@version	1.0.0
	@brief		template for source files
	@note		
	@license	SEE $REElib_root/LICENSE.md
*/
#pragma GCC diagnostic ignored "-Wunused-function"		/* Disable the warning from unused private function in your lib */
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <template_header.h>
#include <REEcore.h>		/* core & clock related utilities */
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */

#include <em_device.h>		/* for HW module addresses */
#include <em_cmu.h>			/* Clock access */


/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */
#ifndef MYAPI_MYCONF					/* If it's not explicitely defined in the project-specific conf.h,
										will load default value for correct code execution */
	#define	MYAPI_MYCONF				(MYAPI_MYCONF_DEF)
#endif
#ifndef MYAPI_THREAD_EXECRATE_MS
	#define MYAPI_THREAD_EXECRATE_MS	(MYAPI_THREAD_EXECRATE_MS_DEF)
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
static const myU8_t * __myPtr = &myConst;	/* Example of private global pointer pointing at global var myConst */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
/* Example enumeration for internal use */
typedef enum __attribute__((packed)) __myEnum_e {
	__myEnum_a =		0x00,
	__myEnum_b =		0x01,
	__myEnum_c =		0x02,
}__myEnum_t;


/* -------------------------------- +
|									|
|	Private Prototype				|
|									|
+ -------------------------------- */
/*  Must be before global var as fct pointer to one of those function may be used in them */
static myU8_t __myPrivFct(void);			/* Example prototype of an internal function */


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
/* Example structure for internal use, with initial value */
static struct __attribute__((packed)) __myAPI_s {
	/* Generic stuff */
	__myEnum_t			state;			/* Custom enum */
	myU8_t				myVar;			/* Example generic var */
	uint32_t			totalPotatoCnt;
	myU8_t (*const		fctPtr)(void);	/* Example function pointer */
	
	/* RTOS stuff */
	#ifdef REELIB_USE_CMSISRTOS2
	struct {
		osRtxThread_t			cb;
		osThreadId_t			id;
		const osThreadAttr_t	conf;
	}thread;
	#endif
}__myAPI = {
	.state =					__myEnum_a,
	.myVar =					MYAPI_MYCONST,
	.totalPotatoCnt =			0,
	.fctPtr =					__myPrivFct,			/* pointing to __myPrivFct() */
	
	#ifdef REELIB_USE_CMSISRTOS2
	.thread.conf.name =			"myAPI_thd",
	.thread.conf.cb_mem =		&__myAPI.thread.cb,
	.thread.conf.cb_size =		sizeof(__myAPI.thread.cb),
	.thread.conf.priority =		osPriorityNormal,
	#endif
};


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
/* Example multi-line macro for internal use */
#define	__MYAPI_MYMACRO(x)			((void)x)	\
									((void)x)


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
static myU8_t __myPrivFct(void) {
	(void)__myPtr;					/* Sush the unused warning, but still keep example */
	return MYAPI_MYCONST;
}


/* -------------------------------- +
|									|
|	Bare Metal API					|
|									|
+ -------------------------------- */
REEcode_t myAPI_Init(const myAPI_conf_t * conf) {
	/* NULL pointer catch in debug mode */
	if ((conf == NULL) || (conf->hw == NULL)) {
		#ifdef DEBUG
		REEbreak();
		#endif

		return REEcode_null;
	}

	CMU_ClockEnable(REEcore_findCMUClock((void *)conf->hw), true);
	return REEcode_success;
}

myU8_t myAPI_myFct(myU8_t potatoCnt) {
	if (__myAPI.totalPotatoCnt < UINT32_MAX) {
		__myAPI.totalPotatoCnt += potatoCnt;		/* Those potatoes are mine now! */
		return 0;
	}
	
	return potatoCnt;
}


/* -------------------------------- +
|									|
|	RTOS API						|
|									|
+ -------------------------------- */
/* This is private but still used in the context of the RTOS API */
static void __myAPI_thread(void * arg) {
	#ifdef REELIB_USE_CMSISRTOS2
	(void)arg;							/* Nothing to do with arg */
	myGlobal = 22;						/* Neither with a public global! */
	/* Locals */
	bool		run =		true;		/* Thread execution control */
	uint32_t	potatos =	0;			/* Because we want a LOT of them! */

	/* Init */
	myAPI_Init(&myAPI_conf_def);

	/* Execute */
	while(run) {
		myAPI_myFct(potatos++);
		osDelay(MYAPI_THREAD_EXECRATE_MS);
	}

	/* Die */
	REEBUGSWO_String("__myAPI_thread finished");

	#endif
}

REEcode_t myAPI_startThread(void) {
	#ifdef REELIB_USE_CMSISRTOS2
	__myAPI.thread.id = osThreadNew(__myAPI_thread, NULL, &__myAPI.thread.conf);
	if (__myAPI.thread.id != NULL) {
		return REEcode_success;
	}
	#endif

	return REEcode_fail;
}

REEcode_t myAPI_stopThread(void) {
	#ifdef REELIB_USE_CMSISRTOS2
	if (osOK == osThreadTerminate(__myAPI.thread.id)) {
		return REEcode_success;
	}
	#endif

	return REEcode_fail;
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void myAPI_ISR(void * hwPtr) {
	/* ISR should be quick, and normally callen with static param (ex: ISR(TIMER0); ) */
	/* Except if the param passed to the ISR is dynamic, keep the check in that case! */
	#ifdef DEBUG
	if (hwPtr == NULL) {
		REEbreak();
	}
	#endif

	/* Actual ISR code */
}
#endif

#pragma GCC diagnostic warning "-Wunused-function"		/* Restore the warnings from unused private function in your lib */
#pragma GCC diagnostic warning "-Wunused-parameter"
