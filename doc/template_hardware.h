/*
	@name		hardware.h
	@author		
	@version	1.0.0
	@note		Project specifics Hardware constants and definitions
	@license	SEE $REElib_root/LICENSE.md
*/
#ifndef __TEMPLATE_HARDWARE_H__
#define __TEMPLATE_HARDWARE_H__ 1

#include <REEutil.h>		/* For GPIO tricks */
#include <em_chip.h>		/* For registers definitions */
#include <em_cmu.h>			/* For registers definitions */
#include <em_gpio.h>		/* For registers definitions */
#include <em_adc.h>			/* For registers definitions */


/* -------------------------------- +
|	Hardware 						|
|	Mapping							|
|									|
+ -------------------------------- */
/*	Pin		NetName			Usage						Peripheral
-------------------------------------------------------------------
	PA0		VSUP_EN			VSUPPLY Power Switch enable	GPIO Output (active high)	(need 10mA drive strengh)
	PA1		
	PA2		VCAM_SNS		VCAMERA sense node 			Analog DY (ADC0)
	PA3		VSUP_SNS		VSUPPLY sense node			Analog DX (ADC0)
	PA4		VBAT_SNS		VBAT sense node				Analog DY (ADC0)
	PA5*	ICAM_SNS		ICAMERA analog voltage		Analog DX (ADC0)

	PB11	RGB0_A			RGB RGB0 common anode		GPIO Output (active high)	(need 10mA drive strengh)
	PB12	RGB1_A			RGB RGB1 common anode		GPIO Output (active high)	(need 10mA drive strengh)
	PB13	BTN0			BTN0 signal					GPIO Input with pull-up (active low)
	PB14	BTN1			BTN1 signal					GPIO Input with pull-up (active low)
	PB15	BTN2			BTN2 signal					GPIO Input with pull-up (active low)

	PC6*	RGBSTRING		RGB Addressable string		USART1 TX#11
	PC7*	REEPER_TX		REEPER interface TX			USART0 TX#12
	PC8*	REEPER_RX		REEPER interface RX			USART0 RX#12
	PC9*	LED_R			LED RGB Red signal			TIM0 CC0#14
	PC10*	LED_G			LED RGB Green signal		TIM0 CC1#14
	PC11*	LED_B			LED RGB Blue signal			TIM0 CC2#14

	PD8*	CHRG_OK			Charger OK status			GPIO Input (active high)
	PD9*	SDA				Charger I2C Data			I2C0 SDA#17
	PD10*	SCL				Charger I2C Clock			I2C0 SCL#17
	PD11*	VBKUP_EN		VBACKUP Power Switch enable	GPIO Output (active high)	(need 10mA drive strengh)
	PD12*	VBKUP_PGOOD		VBACKUP Regulator status	GPIO Input (active low)
	PD13	VBKUP_SNS		VBACKUP sense node			Analog DX (ADC0)
	PD14	
	PD15	VBKUP_CTL		VBACKUP control signal		VDAC0 buffed with OPA0_O

	PF0*	SWCLK_BOOTTX	SWD Clock + Bootloader TX	JTAG dedicated / USART0 TX#24
	PF1*	SWDIO_BOOTRX	SWD Data + Bootloader RX	JTAG dedicated / USART0 RX#24
	PF2*	SWO_WU			SWD Viewer + Wake-up		JTAG dedicated / WU#0
	PF3*	PF3_DBGCLK		MXDBG IO Clock
	PF4*	PF4_DBG0		MXDBG IO Data 0				
	PF5*	PF5_DBG1		MXDBG IO Data 1				
	PF6*	PF6_DBG2		MXDBG IO Data 2				
	PF7*	PF7_DBG3		MXDBG IO Data 3				

	*5V tolerant pins
*/


/* -------------------------------- +
|									|
|	Input/Output Mapping			|
|									|
+ -------------------------------- */
/* -- Debug Port -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit have nice F13 + D4-7 in top right header */
	#define	IOM_DBGD0				REEIO(gpioPortC, 12)
	#define	IOM_DBGD1				REEIO(gpioPortC, 13)
	#define	IOM_DBGD2				REEIO(gpioPortC, 14)
	#define	IOM_DBGD3				REEIO(gpioPortC, 15)
	#define	IOM_DBGCLK				REEIO(gpioPortF, 13)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define	IOM_DBGD0				REEIO(gpioPortF, 4)
	#define	IOM_DBGD1				REEIO(gpioPortF, 5)
	#define	IOM_DBGD2				REEIO(gpioPortF, 6)
	#define	IOM_DBGD3				REEIO(gpioPortF, 7)
	#define	IOM_DBGCLK				REEIO(gpioPortF, 3)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit */
	#define	IOM_DBGD0				REEIO(gpioPortC, 12)
	#define	IOM_DBGD1				REEIO(gpioPortC, 13)
	#define	IOM_DBGD2				REEIO(gpioPortC, 14)
	#define	IOM_DBGD3				REEIO(gpioPortC, 15)
	#define	IOM_DBGCLK				REEIO(gpioPortD, 8)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit */
	#define	IOM_DBGD0				REEIO(gpioPortE, 8)
	#define	IOM_DBGD1				REEIO(gpioPortE, 9)
	#define	IOM_DBGD2				REEIO(gpioPortE, 10)
	#define	IOM_DBGD3				REEIO(gpioPortE, 11)
	#define	IOM_DBGCLK				REEIO(gpioPortE, 13)

#endif

/* Common to all devkit */
#define	IOM_DBGD0_MODE			(gpioModePushPull)
#define	IOM_DBGD0_DEFAULT		(0)
#define	IOM_DBGD1_MODE			(gpioModePushPull)
#define	IOM_DBGD1_DEFAULT		(0)
#define	IOM_DBGD2_MODE			(gpioModePushPull)
#define	IOM_DBGD2_DEFAULT		(0)
#define	IOM_DBGD3_MODE			(gpioModePushPull)
#define	IOM_DBGD3_DEFAULT		(0)
#define	IOM_DBGCLK_MODE			(gpioModePushPull)
#define	IOM_DBGCLK_DEFAULT		(0)


/* -- REEPER -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit VCOM is on PH4, PH5 */
	#define	IOM_REEPER_TX			REEIO(gpioPortH, 4)
	#define	IOM_REEPER_RX			REEIO(gpioPortH, 5)
	#define	IOM_VCOM_EN				REEIO(gpioPortE, 1)
	#define IOM_VCOM_EN_MODE		(gpioModePushPull)
	#define	IOM_VCOM_EN_DEFAULT		(1)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit VCOM is on PA0, PA1 */
	#define	IOM_REEPER_TX			REEIO(gpioPortA, 0)
	#define	IOM_REEPER_RX			REEIO(gpioPortA, 1)
	#define	IOM_VCOM_EN				REEIO(gpioPortA, 5)
	#define IOM_VCOM_EN_MODE		(gpioModePushPull)
	#define	IOM_VCOM_EN_DEFAULT		(1)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit VCOM is on PD0, PD1 */
	#define	IOM_REEPER_TX			REEIO(gpioPortD, 0)
	#define	IOM_REEPER_RX			REEIO(gpioPortD, 1)
	#define	IOM_VCOM_EN				REEIO(gpioPortA, 8)
	#define IOM_VCOM_EN_MODE		(gpioModePushPull)
	#define	IOM_VCOM_EN_DEFAULT		(1)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit VCOM is on PE7, PE6 */
	#define	IOM_REEPER_TX			REEIO(gpioPortE, 7)
	#define	IOM_REEPER_RX			REEIO(gpioPortE, 6)

#endif

/* Common to all devkit */
#define	IOM_REEPER_TX_MODE		(gpioModePushPull)
#define	IOM_REEPER_TX_DEFAULT	(1)
#define	IOM_REEPER_RX_MODE		(gpioModeInput)
#define	IOM_REEPER_RX_DEFAULT	(1)



/* -- LEDs & BTNs -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit got 2 rgb n_n */
	/* Uniform Intf */
	#define	IOM_LED0				REEIO(gpioPortH, 11)
	#define	IOM_LED1				REEIO(gpioPortH, 14)
	#define IOM_BTN0				REEIO(gpioPortC, 8)
	#define IOM_BTN1				REEIO(gpioPortC, 9)
	#define	IOM_WU					REEIO(gpioPortC, 9)

	#define IOM_LED0R				REEIO(gpioPortH, 10)
	#define IOM_LED0R_MODE			(gpioModePushPull)
	#define IOM_LED0R_DEFAULT		(0)
	#define IOM_LED0G				REEIO(gpioPortH, 11)
	#define IOM_LED0G_MODE			(gpioModePushPull)
	#define IOM_LED0G_DEFAULT		(0)
	#define IOM_LED0B				REEIO(gpioPortH, 12)
	#define IOM_LED0B_MODE			(gpioModePushPull)
	#define IOM_LED0B_DEFAULT		(0)
	#define IOM_LED1R				REEIO(gpioPortH, 13)
	#define IOM_LED1R_MODE			(gpioModePushPull)
	#define IOM_LED1R_DEFAULT		(0)
	#define IOM_LED1G				REEIO(gpioPortH, 14)
	#define IOM_LED1G_MODE			(gpioModePushPull)
	#define IOM_LED1G_DEFAULT		(0)
	#define IOM_LED1B				REEIO(gpioPortH, 15)
	#define IOM_LED1B_MODE			(gpioModePushPull)
	#define IOM_LED1B_DEFAULT		(0)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit has only 2 amber -_- */
	/* Uniform Intf */
	#define	IOM_LED0				REEIO(gpioPortF, 4)
	#define IOM_LED1				REEIO(gpioPortF, 5)
	#define IOM_BTN0				REEIO(gpioPortF, 6)
	#define IOM_BTN1				REEIO(gpioPortF, 7)
	#define	IOM_WU					REEIO(gpioPortF, 7)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit has only 2 amber -_- */
	/* Uniform Intf */
	#define	IOM_LED0				REEIO(gpioPortD, 2)
	#define IOM_LED1				REEIO(gpioPortC, 2)
	#define IOM_BTN0				REEIO(gpioPortD, 5)
	#define IOM_BTN1				REEIO(gpioPortC, 9)
	#define	IOM_WU					REEIO(gpioPortC, 9)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit has 2 RGB! yay! */
	#define	IOM_LED0R				REEIO(gpioPortA, 12)
	#define	IOM_LED0G				REEIO(gpioPortA, 13)
	#define	IOM_LED0B				REEIO(gpioPortA, 14)
	#define	IOM_LED1R				REEIO(gpioPortD, 6)
	#define	IOM_LED1G				REEIO(gpioPortF, 12)
	#define	IOM_LED1B				REEIO(gpioPortE, 12)
	#define	IOM_BTN0				REEIO(gpioPortD, 5)
	#define	IOM_BTN1				REEIO(gpioPortD, 8)

#endif

/* Common to all devkit */
#define IOM_LED0_MODE			(gpioModePushPull)
#define IOM_LED0_DEFAULT		(0)
#define IOM_LED1_MODE			(gpioModePushPull)
#define IOM_LED1_DEFAULT		(0)

#define IOM_BTN0_MODE			(gpioModeInput)
#define IOM_BTN0_DEFAULT		(0)
#define IOM_BTN1_MODE			(gpioModeInput)
#define IOM_BTN1_DEFAULT		(0)


/* -- Sensors -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit */
	#define	IOM_SCL				REEIO(gpioPortI, 4)
	#define IOM_SDA				REEIO(gpioPortI, 5)
	#define	IOM_I2C_EN			REEIO(gpioPortB, 3)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define	IOM_SCL				REEIO(gpioPortC, 11)
	#define IOM_SDA				REEIO(gpioPortC, 10)
	#define	IOM_I2C_EN			REEIO(gpioPortB, 10)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit */
	#define	IOM_SCL				REEIO(gpioPortD, 7)
	#define IOM_SDA				REEIO(gpioPortD, 6)
	#define	IOM_I2C_EN			REEIO(gpioPortC, 12)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit */
	#define IOM_PDM_CLK			REEIO(gpioPortB, 12)
	#define	IOM_PDM_DATA		REEIO(gpioPortB, 11)
	#define	IOM_MIC_EN			REEIO(gpioPortA, 8)

#endif

/* Common to all devkit */
#define	IOM_SCL_MODE			(gpioModeWiredAndPullUp)
#define	IOM_SCL_DEFAULT			(1)
#define	IOM_SDA_MODE			(gpioModeWiredAndPullUp)
#define	IOM_SDA_DEFAULT			(1)
#define	IOM_I2C_EN_MODE			(gpioModePushPull)
#define	IOM_I2C_EN_DEFAULT		(1)


/* -- Display -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit */
	#define	IOM_DISP_SCK			REEIO(gpioPortC, 15)
	#define	IOM_DISP_SCK_MODE		(gpioModePushPull)
	#define	IOM_DISP_SCK_DEFAULT	(1)
	#define IOM_DISP_MOSI			REEIO(gpioPortA, 14)
	#define IOM_DISP_MOSI_MODE		(gpioModePushPull)
	#define IOM_DISP_MOSI_DEFAULT	(0)
	#define IOM_DISP_SCS			REEIO(gpioPortC, 14)
	#define IOM_DISP_SCS_MODE		(gpioModePushPull)
	#define IOM_DISP_SCS_DEFAULT	(1)
	#define	IOM_DISP_COM			REEIO(gpioPortA, 11)
	#define	IOM_DISP_COM_MODE		(gpioModePushPull)
	#define	IOM_DISP_COM_DEFAULT	(0)
	#define	IOM_DISP_EN				REEIO(gpioPortA, 9)
	#define	IOM_DISP_EN_MODE		(gpioModePushPull)
	#define	IOM_DISP_EN_DEFAULT		(0)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define	IOM_DISP_SCK			REEIO(gpioPortC, 8)
	#define	IOM_DISP_SCK_MODE		(gpioModePushPull)
	#define	IOM_DISP_SCK_DEFAULT	(1)
	#define IOM_DISP_MOSI			REEIO(gpioPortC, 6)
	#define IOM_DISP_MOSI_MODE		(gpioModePushPull)
	#define IOM_DISP_MOSI_DEFAULT	(0)
	#define IOM_DISP_SCS			REEIO(gpioPortD, 14)
	#define IOM_DISP_SCS_MODE		(gpioModePushPull)
	#define IOM_DISP_SCS_DEFAULT	(1)
	#define	IOM_DISP_COM			REEIO(gpioPortD, 13)
	#define	IOM_DISP_COM_MODE		(gpioModePushPull)
	#define	IOM_DISP_COM_DEFAULT	(0)
	#define	IOM_DISP_EN				REEIO(gpioPortD, 15)
	#define	IOM_DISP_EN_MODE		(gpioModePushPull)
	#define	IOM_DISP_EN_DEFAULT		(0)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit */
	/* TODO: define segment pin if really needed */

#endif


#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* -- GG11 devkit extra stuff -- */
	/* QSPI Mem */
	#define	IOM_QSPI_SCK			REEIO(gpioPortG, 0)
	#define	IOM_QSPI_D0				REEIO(gpioPortG, 1)
	#define	IOM_QSPI_D1				REEIO(gpioPortG, 2)
	#define	IOM_QSPI_D2				REEIO(gpioPortG, 3)
	#define	IOM_QSPI_D3				REEIO(gpioPortG, 4)
	#define	IOM_QSPI_CS				REEIO(gpioPortG, 9)
	#define	IOM_QSPI_EN				REEIO(gpioPortG, 13)

	/* uSD card */
	#define	IOM_SDCARD_SCK			REEIO(gpioPortE, 14)
	#define	IOM_SDCARD_D0			REEIO(gpioPortA, 0)
	#define	IOM_SDCARD_D1			REEIO(gpioPortA, 1)
	#define	IOM_SDCARD_D2			REEIO(gpioPortA, 2)
	#define	IOM_SDCARD_D3			REEIO(gpioPortA, 3)
	#define	IOM_SDCARD_CMD			REEIO(gpioPortE, 15)
	#define	IOM_SDCARD_CD			REEIO(gpioPortB, 10)
	#define	IOM_SDCARD_EN			REEIO(gpioPortE, 7)

	/* Ethernet */
	#define	IOM_ETH_EN				REEIO(gpioPortI, 10)
	#define	IOM_ETH_EN_MODE			(gpioModePushPull)
	#define	IOM_ETH_EN_DEFAULT		(0)

	/* USB */
	#define	IOM_USB_DN				REEIO(gpioPortF, 10)
	#define	IOM_USB_DP				REEIO(gpioPortF, 11)
	#define	IOM_USB_ID				REEIO(gpioPortF, 12)
	#define	IOM_USB_VBUSEN			REEIO(gpioPortF, 5)
	#define	IOM_USB_VBUSEN_MODE		(gpioModePushPull)
	#define	IOM_USB_VBUSEN_DEFAULT	(0)
	#define	IOM_USB_OC				REEIO(gpioPortF, 4)
	#define	IOM_USB_OC_MODE			(gpioModeInput)
	#define	IOM_USB_OC_DEFAULT		(0)

	/* MICs */
	#define	IOM_MIC_DATA			REEIO(gpioPortI, 13)
	#define	IOM_MIC_BCLK			REEIO(gpioPortI, 14)
	#define	IOM_MIC_LRCLK			REEIO(gpioPortI, 15)
	#define	IOM_MIC_EN				REEIO(gpioPortD, 0)
	#define	IOM_MIC_EN_MODE			(gpioModePushPull)
	#define	IOM_MIC_EN_DEFAULT		(0)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* USB */
	#define IOM_USB_DN				REEIO(gpioPortF, 10)
	#define	IOM_USB_DP				REEIO(gpioPortF, 11)

#endif


/* -------------------------------- +
|									|
|	Analog Mapping					|
|									|
+ -------------------------------- */
#define	ANM_VCAMSNS				(adcPosSelAPORT3XCH10)		/* PA2 on BUS CX */


/* -------------------------------- +
|									|
|	Peripheral Mapping				|
|									|
+ -------------------------------- */
/* -- REEPER -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit VCOM uses USART4 on loc4 */
	#define	PEM_REEPER_DEV			(USART4)
	#define	PEM_REEPER_CLK			(cmuClock_USART4)
	#define	PEM_REEPER_IRQTX		(USART4_TX_IRQn)
	#define	PEM_REEPER_IRQRX		(USART4_RX_IRQn)
	#define	PEM_REEPER_LOCTX		(USART_ROUTELOC0_TXLOC_LOC4)
	#define	PEM_REEPER_LOCRX		(USART_ROUTELOC0_RXLOC_LOC4)

	#define	PEM_WU_CH				(2)						/* PC9 is EM4WU2 (BTN1) */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define	PEM_REEPER_DEV			(USART0)
	#define	PEM_REEPER_CLK			(cmuClock_USART0)
	#define	PEM_REEPER_IRQTX		(USART0_TX_IRQn)
	#define	PEM_REEPER_IRQRX		(USART0_RX_IRQn)
	#define	PEM_REEPER_LOCTX		(USART_ROUTELOC0_TXLOC_LOC0)
	#define	PEM_REEPER_LOCRX		(USART_ROUTELOC0_RXLOC_LOC0)

	#define	PEM_WU_CH				(1)						/* PF7 is EM4WU1 (BTN1) */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit */
	#define	PEM_REEPER_DEV			(USART1)
	#define	PEM_REEPER_CLK			(cmuClock_USART1)
	#define	PEM_REEPER_IRQTX		(USART1_TX_IRQn)
	#define	PEM_REEPER_IRQRX		(USART1_RX_IRQn)
	#define	PEM_REEPER_LOCTX		(USART_ROUTELOC0_TXLOC_LOC1)
	#define	PEM_REEPER_LOCRX		(USART_ROUTELOC0_RXLOC_LOC1)

	#define	PEM_WU_CH				(2)						/* PC9 is EM4WU2 (BTN1) */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit */
	#define	PEM_REEPER_DEV			(USART0)
	#define	PEM_REEPER_CLK			(cmuClock_USART0)
	#define	PEM_REEPER_IRQTX		(USART0_TX_IRQn)
	#define	PEM_REEPER_IRQRX		(USART0_RX_IRQn)
	#define	PEM_REEPER_LOCTX		(USART_ROUTELOC0_TXLOC_LOC1)
	#define	PEM_REEPER_LOCRX		(USART_ROUTELOC0_RXLOC_LOC1)

#endif

/* -- Sensors -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit */
	#define	PEM_I2C_DEV				(I2C2)
	#define	PEM_I2C_CLK				(cmuClock_I2C2)
	#define PEM_I2C_IRQ				(I2C2_IRQn)
	#define	PEM_I2C_LOCSCL			(I2C_ROUTELOC0_SCLLOC_LOC7)
	#define	PEM_I2C_LOCSDA			(I2C_ROUTELOC0_SDALOC_LOC7)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define	PEM_I2C_DEV				(I2C0)
	#define	PEM_I2C_CLK				(cmuClock_I2C0)
	#define PEM_I2C_IRQ				(I2C0_IRQn)
	#define	PEM_I2C_LOCSCL			(I2C_ROUTELOC0_SCLLOC_LOC15)
	#define	PEM_I2C_LOCSDA			(I2C_ROUTELOC0_SDALOC_LOC15)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 devkit */
	#define	PEM_I2C_DEV				(I2C0)
	#define	PEM_I2C_CLK				(cmuClock_I2C0)
	#define PEM_I2C_IRQ				(I2C0_IRQn)
	#define	PEM_I2C_LOCSCL			(I2C_ROUTELOC0_SCLLOC_LOC1)
	#define	PEM_I2C_LOCSDA			(I2C_ROUTELOC0_SDALOC_LOC1)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 devkit */
	#define	PEM_PDM_DEV				(PDM0)
	#define	PEM_PDM_CLK				(cmuClock_PDM0)
	#define	PEM_PDM_IRQ				(PDM0_IRQn)
	#define	PEM_PDM_LOCCLK			(PDM_ROUTELOC_CLKLOC_LOC3)
	#define	PEM_PDM_LOCDATA			(PDM_ROUTELOC_DATALOC_LOC3)

#endif

/* -- Display -- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 devkit */
	#define PEM_DISP_DEV			(USART1)
	#define	PEM_DISP_CLK			(cmuClock_USART1)
	#define	PEM_DISP_IRQ			(USART1_TX_IRQn)
	#define	PEM_DISP_LOCSCK			(USART_ROUTELOC0_SCKLOC_LOC3)
	#define	PEM_DISP_LOCMOSI		(USART_ROUTELOC0_MOSILOC_LOC6)
	#define	PEM_DISP_LOCCS			(USART_ROUTELOC1_CSLOC_LOC3)

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84) && defined(_EFM32_PEARL_FAMILY)
	/* PG12 devkit */
	#define PEM_DISP_DEV			(USART1)
	#define	PEM_DISP_CLK			(cmuClock_USART1)
	#define	PEM_DISP_IRQ			(USART1_TX_IRQn)
	#define	PEM_DISP_LOCSCK			(USART_ROUTELOC0_SCKLOC_LOC11)
	#define	PEM_DISP_LOCMOSI		(USART_ROUTELOC0_MOSILOC_LOC11)

#endif

#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* -- GG11 devkit extra stuff -- */
	/* MICs */
	#define	PEM_MIC_DEV				(USART3)
	#define	PEM_MIC_CLK				(cmuClock_USART3)
	#define	PEM_MIC_IRQRX			(USART3_RX_IRQn)
	#define	PEM_MIC_LOCRX			(USART_ROUTELOC0_RXLOC_LOC5)
	#define	PEM_MIC_LOCCLK			(USART_ROUTELOC0_CLKLOC_LOC5)
	#define	PEM_MIC_LOCCS			(USART_ROUTELOC0_CSLOC_LOC5)
#endif


/* -------------------------------- +
|									|
|	DMA Mapping						|
|									|
+ -------------------------------- */
#define	DMAM_ADC					(3)
#define	DMAM_VDAC					(2)


/* -------------------------------- +
|									|
|	Hardware constant				|
|									|
+ -------------------------------- */
#if defined(_SILICON_LABS_GECKO_INTERNAL_SDID_100)
	/* GG11 max 72MHz */
	#define	CPU_MAINOSC_FREQ		(72000000)		/* Using HFRCO @ 72MHZ, no PLL */
	#define	CPU_SLEEPOSC_FREQ		(1000000)		/* Using HFRCO @ 1MHz, no PLL */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_84)
	/* PG12 and JG12 max 38MHz (rumors says 40 is doable) */
	#define	CPU_MAINOSC_FREQ		(38000000)		/* Using HFRCO @ 38MHZ, no PLL */
	#define	CPU_SLEEPOSC_FREQ		(1000000)		/* Using HFRCO @ 1MHz, no PLL */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_103)
	/* TG11 max 48MHz */
	#define	CPU_MAINOSC_FREQ		(48000000)		/* Using HFRCO @ 48MHZ, no PLL */
	#define	CPU_SLEEPOSC_FREQ		(1000000)		/* Using HFRCO @ 1MHz, no PLL */

#elif defined(_SILICON_LABS_GECKO_INTERNAL_SDID_106)
	/* GG12 max  */
	#define	CPU_MAINOSC_FREQ		(72000000)		/* Using HFRCO @ 72MHz, no PLL */
	#define	CPU_SLEEPOSC_FREQ		(1000000)		/* Using HFRCO @ 72MHz, no PLL */

#endif


/* -------------------------------- +
|									|
|	Circuit constant				|
|									|
+ -------------------------------- */
#define	I2CADD_SI7021				(0b1000000)			/* Temperature and Humidity sensor */
#define	I2CADD_SI7210				(0b0110000)			/* Hall-effect sensor (address may be between 0x30 and 0x33 )*/


#endif	/* ifndef __TEMPLATE_HARDWARE_H__ */

