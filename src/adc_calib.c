/**************************************************************************//**
 * @file  adc_calib (extract from EFM32_example)
 * @brief ADC Examples for EFM32 Gecko Series 0
 * @version 1.1.1
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/


#include <adc_calib.h>


#ifndef ADC_CLOCK
#define ADC_CLOCK (1000000UL)
#endif

#ifndef ADC_GAIN_CAL_VALUE
#define ADC_GAIN_CAL_VALUE      0xffd0          /* ADC gain calibration value, shifted due to oversampling */
#endif


/***************************************************************************//**
 * @brief
 *   Calibrate offset and gain for the specified reference.
 *   Supports currently only single ended gain calibration.
 *   Could easily be expanded to support differential gain calibration.
 *
 * @details
 *   The offset calibration routine measures 0 V with the ADC, and adjust
 *   the calibration register until the converted value equals 0.
 *   The gain calibration routine needs an external reference voltage equal
 *   to the top value for the selected reference. For example if the 2.5 V
 *   reference is to be calibrated, the external supply must also equal 2.5V.
 *
 * @param[in] adc
 *   Pointer to ADC peripheral register block.
 *
 * @param[in] ref
 *   Reference used during calibration. Can be both external and internal
 *   references.
 *   The SCANGAIN and SINGLEGAIN calibration fields are not used when the
 *   unbuffered differential 2xVDD reference is selected.
 *
 * @return
 *   The final value of the calibration register, note that the calibration
 *   register gets updated with this value during the calibration.
 *   No need to load the calibration values after the function returns.
 ******************************************************************************/
uint32_t ADC_Calibration(ADC_TypeDef *adc, ADC_PosSel_TypeDef input, ADC_Ref_TypeDef ref){
  int32_t  sample;
  uint32_t cal;

  /* Binary search variables */
  uint8_t high;
  uint8_t mid;
  uint8_t low;

  /* Reset ADC to be sure we have default settings and wait for ongoing */
  /* conversions to be complete. */
  ADC_Reset(adc);

  ADC_Init_TypeDef       init       = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef singleInit = ADC_INITSINGLE_DEFAULT;

  /* Init common settings for both single conversion and scan mode */
  init.timebase = ADC_TimebaseCalc(0);
  init.prescale = ADC_PrescaleCalc(ADC_CLOCK, 0);

  /* Set an oversampling rate for more accuracy */
  init.ovsRateSel = adcOvsRateSel4096;
  ADC_Init(adc, &init);

  /* Init for single conversion use, measure DIFF0 with selected reference. */
  singleInit.reference = ref;
  singleInit.posSel     = adcPosSelVSS /*adcSingleInputDiff0*/;
  singleInit.acqTime   = adcAcqTime32;
  singleInit.diff      = true;
  /* Enable oversampling rate */
  singleInit.resolution = adcResOVS;

  ADC_InitSingle(adc, &singleInit);

  /* ADC is now set up for offset calibration */
  /* Offset calibration register is a 7 bit signed 2's complement value. */
  /* Use unsigned indexes for binary search, and convert when calibration */
  /* register is written to. */
  high = 128;
  low  = 0;

  /* Do binary search for offset calibration*/
  while (low < high){
    /* Calculate midpoint */
    mid = low + (high - low) / 2;

    /* Midpoint is converted to 2's complement and written to both scan and */
    /* single calibration registers */
    cal      = adc->CAL & ~(_ADC_CAL_SINGLEOFFSET_MASK | _ADC_CAL_SCANOFFSET_MASK);
    cal     |= (uint8_t)(mid - 63) << _ADC_CAL_SINGLEOFFSET_SHIFT;
    cal     |= (uint8_t)(mid - 63) << _ADC_CAL_SCANOFFSET_SHIFT;
    adc->CAL = cal;

    /* Do a conversion */
    ADC_Start(adc, adcStartSingle);
    while (adc->STATUS & ADC_STATUS_SINGLEACT);

    /* Get ADC result */
    sample = ADC_DataSingleGet(adc);

    /* Check result and decide in which part of to repeat search */
    /* Calibration register has negative effect on result */
    if (sample < 0){
      /* Repeat search in bottom half. */
      high = mid;

    }else if (sample > 0){
      /* Repeat search in top half. */
      low = mid + 1;
    }else{
      /* Found it, exit while loop */
      break;
    }
  }

  /* Now do gain calibration, only INPUT and DIFF settings needs to be changed */
  adc->SINGLECTRL &= ~(_ADC_SINGLECTRL_POSSEL_MASK | _ADC_SINGLECTRL_DIFF_MASK);
  adc->SINGLECTRL |= (input << _ADC_SINGLECTRL_POSSEL_SHIFT);
  adc->SINGLECTRL |= (false << _ADC_SINGLECTRL_DIFF_SHIFT);

  /* Gain calibration register is a 7 bit unsigned value. */
  high = 128;
  low  = 0;

  /* Do binary search for gain calibration */
  while (low < high){
    /* Calculate midpoint and write to calibration register */
    mid = low + (high - low) / 2;
    cal      = adc->CAL & ~(_ADC_CAL_SINGLEGAIN_MASK | _ADC_CAL_SCANGAIN_MASK);
    cal     |= mid << _ADC_CAL_SINGLEGAIN_SHIFT;
    cal     |= mid << _ADC_CAL_SCANGAIN_SHIFT;
    adc->CAL = cal;

    /* Do a conversion */
    ADC_Start(adc, adcStartSingle);
    while (adc->STATUS & ADC_STATUS_SINGLEACT)
      ;

    /* Get ADC result */
    sample = ADC_DataSingleGet(adc);

    /* Check result and decide in which part to repeat search */
    /* Compare with a value atleast one LSB's less than top to avoid overshooting */
    /* Since oversampling is used, the result is 16 bits, but a couple of lsb's */
    /* applies to the 12 bit result value, if 0xffd is the top value in 12 bit, this */
    /* is in turn 0xffd0 in the 16 bit result. */
    /* Calibration register has positive effect on result */
    if (sample > ADC_GAIN_CAL_VALUE){
      /* Repeat search in bottom half. */
      high = mid;

    }else if (sample < ADC_GAIN_CAL_VALUE){
      /* Repeat search in top half. */
      low = mid + 1;

    }else{
      /* Found it, exit while loop */
      break;
    }
  }
  return adc->CAL;
}
