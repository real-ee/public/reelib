/*
 * REEMember.c
 *
 *  Created on: Jun 9, 2019
 *      Author: jlecuyer
 */
#include <fletcher.h>
#include <REEMember.h>
#include <string.h>
#include <REEBUG.h>

#include <REECheck.h>

#define _UNINITIALIZED_MEMORY 	(0xFFFF)

/*
 * Return the length of an U32 array that can fit the given qty of U8
 * 	=u8-MOD(u8-1,4)+3
 *
 *  */
uint16_t __sizeRounding(uint16_t u8Count){
	uint16_t u32Count = 0;
	if(u8Count>0){
		u32Count = u8Count + 3 -((u8Count-1)%4);
	}
	return u32Count;
}




REEMember_t REEMember_save(uint8_t *data, size_t size){
	REEMember_t			reesult = REEMember_unavailable;

	uint16_t writeSize = __sizeRounding(size)+4;

	if(writeSize > REEMEMBER_SIZE){
		reesult =  REEMember_tooBig;
		REEBUGSWO_String("REEMember_save::too big");

	}else {

		MSC_Status_TypeDef 	msc;
		uint32_t temp[(writeSize/4)];

		msc = MSC_ErasePage(REEMEMBER_ADDRESS);

		memcpy(&temp[1], data, size);				// Convert array
		if(!REECheck_crc_hwU8((uint8_t *)(&temp[1]), size)) {
		    REEBUGSWO_String("REEMember_save::Check computation failed");
		    msc = REEMember_checkFailed;
		} else {
            uint16_t check = REECheck_crc_getCRC() & UINT16_MAX;

            temp[0] = (size<<16) | (check);

            if(msc != mscReturnOk){
                REEBUGSWO_String("REEMember_save::erase failed");

            }else{
                MSC_Init();
                msc = MSC_WriteWord(REEMEMBER_ADDRESS, temp, sizeof(temp));
                MSC_Deinit();
                if(msc != mscReturnOk){
                    REEBUGSWO_String("REEMember_save::write failed");
                }
            }
		}
		reesult = msc;
	}
	return reesult;
}


size_t REEMember_load(uint8_t *data, size_t maxSize){
	uint16_t checkComputed;

	/* Load meta data */
	uint32_t info  			= *((uint32_t *)REEMEMBER_ADDRESS);
	size_t size 			= info >> 16;
	uint16_t checkInFlash	= info & 0xFFFF;

	size_t sizeOnDisk = __sizeRounding(size)+4;


	/* Check if memory is initialized */
	if(size == 0xFFFF){
		REEBUGSWO_String("REEMember_load::uninitialized\n");
		size = 0;

	}else if(size > maxSize){
		REEBUGSWO_String("REEMember_load::too big");
		size = 0;

	}else if(sizeOnDisk > (REEMEMBER_SIZE)){
		REEBUGSWO_String("REEMember_load::corruption");
		size = 0;

	}else{
		uint32_t temp[sizeOnDisk/4];
		memcpy(temp, REEMEMBER_ADDRESS, sizeOnDisk);

        if(!REECheck_crc_hwU8((uint8_t *)(&temp[1]), size)) {
            REEBUGSWO_String("REEMember_save::Check computation failed");
        } else {
            checkComputed = REECheck_crc_getCRC() & UINT16_MAX;
            if(checkComputed == checkInFlash){
                memcpy(data, &temp[1], size);
            }else{
                REEBUGSWO_String("REEMember_load::bad checksum");
                size = 0;
            }
        }
	}
	return size;
}


REEMember_t REEMember_clear(void){
	return (REEMember_t) MSC_ErasePage(REEMEMBER_ADDRESS);
}

