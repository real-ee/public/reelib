/*
 * potato.c
 *
 *  Created on: 26 Mar 2019
 *      Author: lvilleneuve
 */

#include <potato.h>

/* ============================ */
/* API							*/
/* ============================ */
/* Very advanced function that answer to every problem in life */
bool areYouHappy(uint16_t patate) {
	if (patate > (UINT16_MAX/2)) {
		return true; // This is enough potato to be happy
	}
	return false;	// This isn't :/
}
