/*
 * aport_gg11.c
 *
 *  Created on: Nov. 4, 2020
 *      Author: jlecuyer
 */
#include <em_device.h>      /* For registers definitions */

#ifdef _EFM32_GIANT_FAMILY //_EFM32_TINY_FAMILY
#include <aport_gg11.h>

bool aportGG11_getAdcChannel(GPIO_Port_TypeDef port, uint8_t pin, aport_gg1_t *chan){
    bool found = false;

    aportGG1_adc_t peripheral;
    if(port == gpioPortH){
        peripheral = aportGG11_adc_1;
    }else if(port == gpioPortD){
        peripheral = aportGG11_adc_0;
    }else if((port == gpioPortA) || (port == gpioPortB) || (port == gpioPortE) || (port == gpioPortF)){
        peripheral = aportGG11_adc_both;
    }else{
        peripheral = aportGG11_adc_none;
    }

    if(peripheral != aportGG11_adc_none){
        found = true;

        switch(port){
            case gpioPortA:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT1XCH0;     break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT2XCH1;     break; }
                    case 2:{    chan->chan_pos=adcPosSelAPORT1XCH2;     break; }
                    case 3:{    chan->chan_pos=adcPosSelAPORT2XCH3;     break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT1XCH4;     break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT2XCH5;     break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT1XCH6;     break; }
                    case 7:{    chan->chan_pos=adcPosSelAPORT2XCH7;     break; }
                    case 8:{    chan->chan_pos=adcPosSelAPORT1XCH8;     break; }
                    case 9:{    chan->chan_pos=adcPosSelAPORT2XCH9;     break; }
                    case 10:{   chan->chan_pos=adcPosSelAPORT1XCH10;    break; }
                    case 11:{   chan->chan_pos=adcPosSelAPORT2XCH11;    break; }
                    case 12:{   chan->chan_pos=adcPosSelAPORT1XCH12;    break; }
                    case 13:{   chan->chan_pos=adcPosSelAPORT2XCH13;    break; }
                    case 14:{   chan->chan_pos=adcPosSelAPORT1XCH14;    break; }
                    case 15:{   chan->chan_pos=adcPosSelAPORT2XCH15;    break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            case gpioPortB:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT1XCH16;    break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT2XCH17;    break; }
                    case 2:{    chan->chan_pos=adcPosSelAPORT1XCH18;    break; }
                    case 3:{    chan->chan_pos=adcPosSelAPORT2XCH19;    break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT1XCH20;    break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT2XCH21;    break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT1XCH22;    break; }
                    // case 7:{ chan->chan_pos=adcPosSelAPORT2XCH23;    break; }
                    // case 8:{ chan->chan_pos=adcPosSelAPORT1XCH24;    break; }
                    case 9:{    chan->chan_pos=adcPosSelAPORT2XCH25;    break; }
                    case 10:{   chan->chan_pos=adcPosSelAPORT1XCH26;    break; }
                    case 11:{   chan->chan_pos=adcPosSelAPORT2XCH27;    break; }
                    case 12:{   chan->chan_pos=adcPosSelAPORT1XCH28;    break; }
                    case 13:{   chan->chan_pos=adcPosSelAPORT2XCH29;    break; }
                    case 14:{   chan->chan_pos=adcPosSelAPORT1XCH30;    break; }
                    case 15:{   chan->chan_pos=adcPosSelAPORT2XCH31;    break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            case gpioPortD:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT0XCH0;     break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT0XCH1;     break; }
                    case 2:{    chan->chan_pos=adcPosSelAPORT0XCH2;     break; }
                    case 3:{    chan->chan_pos=adcPosSelAPORT0XCH3;     break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT0XCH4;     break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT0XCH5;     break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT0XCH6;     break; }
                    case 7:{    chan->chan_pos=adcPosSelAPORT0XCH7;     break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            case gpioPortE:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT3XCH0;     break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT4XCH1;     break; }
                    // case 2:{ chan->chan_pos=adcPosSelAPORT3XCH2;     break; }
                    // case 3:{ chan->chan_pos=adcPosSelAPORT4XCH3;     break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT3XCH4;     break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT4XCH5;     break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT3XCH6;     break; }
                    case 7:{    chan->chan_pos=adcPosSelAPORT4XCH7;     break; }
                    case 8:{    chan->chan_pos=adcPosSelAPORT3XCH8;     break; }
                    case 9:{    chan->chan_pos=adcPosSelAPORT4XCH9;     break; }
                    case 10:{   chan->chan_pos=adcPosSelAPORT3XCH10;    break; }
                    case 11:{   chan->chan_pos=adcPosSelAPORT4XCH11;    break; }
                    case 12:{   chan->chan_pos=adcPosSelAPORT3XCH12;    break; }
                    case 13:{   chan->chan_pos=adcPosSelAPORT4XCH13;    break; }
                    case 14:{   chan->chan_pos=adcPosSelAPORT3XCH14;    break; }
                    case 15:{   chan->chan_pos=adcPosSelAPORT4XCH15;    break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            case gpioPortF:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT3XCH16;     break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT4XCH17;     break; }
                    case 2:{    chan->chan_pos=adcPosSelAPORT3XCH18;     break; }
                    case 3:{    chan->chan_pos=adcPosSelAPORT4XCH19;     break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT3XCH20;     break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT4XCH21;     break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT3XCH22;     break; }
                    case 7:{    chan->chan_pos=adcPosSelAPORT4XCH23;     break; }
                    case 8:{    chan->chan_pos=adcPosSelAPORT3XCH24;     break; }
                    case 9:{    chan->chan_pos=adcPosSelAPORT4XCH25;     break; }
                    case 10:{   chan->chan_pos=adcPosSelAPORT3XCH26;    break; }
                    case 11:{   chan->chan_pos=adcPosSelAPORT4XCH27;    break; }
                    case 12:{   chan->chan_pos=adcPosSelAPORT3XCH28;    break; }
                    case 13:{   chan->chan_pos=adcPosSelAPORT4XCH29;    break; }
                    case 14:{   chan->chan_pos=adcPosSelAPORT3XCH30;    break; }
                    case 15:{   chan->chan_pos=adcPosSelAPORT4XCH31;    break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            case gpioPortH:{
                switch(pin){
                    case 0:{    chan->chan_pos=adcPosSelAPORT0XCH0;     break; }
                    case 1:{    chan->chan_pos=adcPosSelAPORT0XCH1;     break; }
                    case 2:{    chan->chan_pos=adcPosSelAPORT0XCH2;     break; }
                    case 3:{    chan->chan_pos=adcPosSelAPORT0XCH3;     break; }
                    case 4:{    chan->chan_pos=adcPosSelAPORT0XCH4;     break; }
                    case 5:{    chan->chan_pos=adcPosSelAPORT0XCH5;     break; }
                    case 6:{    chan->chan_pos=adcPosSelAPORT0XCH6;     break; }
                    case 7:{    chan->chan_pos=adcPosSelAPORT0XCH7;     break; }
                    default:{   found=false;                            break; }
                }
                break;
            }
            default:{ found = false; break; }
        }
    }
    if(found){
        chan->peripheral = peripheral;
    }
    return found;
 }

#endif
