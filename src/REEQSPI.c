/*
	\name		REEQSPI.c
	\author		SBosse
	\version	1.0.0
	\brief		QSPI access
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEQSPI.h>
#include <REEutil.h>
#include <string.h>
#include <em_cmu.h>
#include "em_core.h"
#include <em_gpio.h>

#if defined(QSPI_COUNT) && QSPI_COUNT

/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
#define QSPI_FIFO            (QSPI0_MEM_BASE | 0x0F000000UL)
#define QSPI_FIFO_SIZE        1024
#define QSPI_FIFO_READ_PART    512
#define QSPI_FIFO_WRITE_PART   512
#define QSPI_QUEUE_SIZE          8
#define QSPI_STATE_READY         1
#define QSPI_STATE_SCHEDULED     2
#define QSPI_STATE_DONE          3


typedef struct {
  uint32_t addr;
  size_t size;
  const void *buffer;
  uint8_t state;
} QSPI_WriteOperation_t;

typedef struct {
  uint32_t addr;
  size_t size;
  void *buffer;
  uint8_t state;
} QSPI_ReadOperation_t;
/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
static uint8_t writeHead = 0;
static uint8_t readHead = 0;
static volatile uint8_t readTail = 0;
static volatile uint8_t writeTail = 0;

static volatile QSPI_WriteOperation_t writeQueue[QSPI_QUEUE_SIZE];
static volatile QSPI_ReadOperation_t readQueue[QSPI_QUEUE_SIZE];
volatile uint32_t *qspiFifo = (volatile uint32_t *)QSPI_FIFO;


static void _readFifo(uint8_t id);
static void _writeFifo(uint8_t id);
static uint32_t _fifoAvailable(void);
static uint32_t _fifoSpace(void);
static void _triggerReadIndirect(uint8_t id);
static void _triggerWriteIndirect(uint8_t id);


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */

/*
	\brief QSPI interrupt handler

	\detail
		The interrupt handler will handle different events from the QSPI
		hardware module. When a read or write operation is done then the next
		operation in the queue will be started. When the read partition of the
		fifo crosses the watermark level then data is copied from the fifo and
		into the destination buffer, and if the write partition of the fifo
		crosses the watermark level then data will be copied from the source
		buffer into the hardware fifo.
*/
void QSPI0_IRQHandler(void)
{
	uint32_t flags = QSPI0->IRQSTATUS;
	QSPI0->IRQSTATUS |= flags;

	if (flags & QSPI_IRQSTATUS_INDIRECTOPDONE) {
		/* Handle completed write operation */
		if ((QSPI0->INDIRECTWRITEXFERCTRL & QSPI_INDIRECTWRITEXFERCTRL_INDOPSDONESTATUS) != 0U) {
			QSPI0->INDIRECTWRITEXFERCTRL |= _QSPI_INDIRECTWRITEXFERCTRL_INDOPSDONESTATUS_MASK;
			writeQueue[writeTail].state = QSPI_STATE_DONE;
			writeTail++;
			if (writeTail >= QSPI_QUEUE_SIZE) {
				writeTail = 0;
			}

			/* Trigger next write operation if it's ready */
			if (writeQueue[writeTail].state == QSPI_STATE_READY) {
				_triggerWriteIndirect(writeTail);
			}
		}

		/* Handle completed read operation */
		if ((QSPI0->INDIRECTREADXFERCTRL & QSPI_INDIRECTREADXFERCTRL_INDOPSDONESTATUS) != 0U) {
			QSPI0->INDIRECTREADXFERCTRL |= _QSPI_INDIRECTREADXFERCTRL_INDOPSDONESTATUS_MASK;
			readQueue[readTail].state = QSPI_STATE_DONE;
			readTail++;
			if (readTail >= QSPI_QUEUE_SIZE) {
				readTail = 0;
			}

			if (readQueue[readTail].state == QSPI_STATE_READY) {
				_triggerReadIndirect(readTail);
			}
		}
	}

	if (flags & QSPI_IRQSTATUS_INDIRECTXFERLEVELBREACH) {
		/* Watermark interrupt triggered and write is in progress fill fifo */
		if ((QSPI0->INDIRECTWRITEXFERCTRL & QSPI_INDIRECTWRITEXFERCTRL_WRSTATUS) != 0U) {
			_writeFifo(writeTail);
		}
		/* Watermark interrupt triggered and read is in progress empty fifo */
		if ((QSPI0->INDIRECTREADXFERCTRL & QSPI_INDIRECTREADXFERCTRL_RDSTATUS) != 0U) {
			_readFifo(readTail);
		}
	}

	if (QSPI0->IRQSTATUS & QSPI_IRQSTATUS_INDIRECTREADREJECT) {
	}
}

/*
	\brief Get the number of free space available in the write partition of the fifo.

	\return[in] id	 Number of bytes available for reading from the fifo.
*/
static uint32_t _fifoSpace(void)
{
	uint32_t n = (QSPI0->SRAMFILL & _QSPI_SRAMFILL_SRAMFILLINDACWRITE_MASK)
			   >> _QSPI_SRAMFILL_SRAMFILLINDACWRITE_SHIFT;
	return QSPI_FIFO_WRITE_PART - (n * 4);
}

/*
	\brief Get the number of bytes available in the read partition of the fifo.

	\return[in] id	 Number of bytes available for reading from the fifo.
*/
static uint32_t _fifoAvailable(void)
{
	uint32_t n = (QSPI0->SRAMFILL & _QSPI_SRAMFILL_SRAMFILLINDACREAD_MASK)
			   >> _QSPI_SRAMFILL_SRAMFILLINDACREAD_SHIFT;
	return n * 4;
}

/*
	\brief Fill the fifo with data from an application buffer.

	\param[in] id	 Id of the write operation to start.
*/
static void _writeFifo(uint8_t id)
{
	const uint32_t *src = writeQueue[id].buffer;

	// Copy 32 bit words to the fifo
	while ((_fifoSpace() > 0) && (writeQueue[id].size >= 4)) {
		*qspiFifo = *src++;
		writeQueue[id].size -= 4;
	}

	// When there is 1-3 bytes left to write then handle it here
	if (writeQueue[id].size > 0 && writeQueue[id].size < 4) {
		*qspiFifo = *src++;
		writeQueue[id].size = 0;
	}
	writeQueue[id].buffer = src;
}

/*
	\brief Read data from the fifo into an application buffer.

	\param[in] id	 Id of the write operation to start.
*/
static void _readFifo(uint8_t id)
{
	uint32_t *dst = readQueue[id].buffer;

	/* Read 32 bit words from the fifo */
	while ((_fifoAvailable() > 0) && (readQueue[id].size >= 4)) {
		*dst++ = *qspiFifo;
		readQueue[id].size -= 4;
	}

	/* When there is 1-3 bytes left to read then handle it here */
	if ((readQueue[id].size > 0) && (readQueue[id].size < 4)) {
		uint32_t value = *qspiFifo;
		memcpy((uint8_t *)dst, (uint8_t *)&value, readQueue[id].size);
		readQueue[id].size = 0;
	}
	readQueue[id].buffer = dst;
}

/*
	\brief
		Trigger indirect write operation.

	\param[in] id	 Id of the write operation to start.
*/
static void _triggerWriteIndirect(uint8_t id)
{
	writeQueue[id].state = QSPI_STATE_SCHEDULED;

	/* Setup indirect transfer FLASH start address */
	QSPI0->INDIRECTWRITEXFERSTART = writeQueue[id].addr;

	/* Setup the number of bytes to be transferred */
	QSPI0->INDIRECTWRITEXFERNUMBYTES = writeQueue[id].size;

	/* Start the indirect write operation */
	QSPI0->INDIRECTWRITEXFERCTRL = QSPI_INDIRECTWRITEXFERCTRL_START;

	/* Fill the QSPI fifo */
	_writeFifo(id);
}

/*
	\brief
		Trigger indirect read operation
*/
static void _triggerReadIndirect(uint8_t id)
{
	readQueue[id].state = QSPI_STATE_SCHEDULED;

	/* Setup indirect transfer FLASH start address */
	QSPI0->INDIRECTREADXFERSTART = readQueue[id].addr;

	/* Setup the number of bytes to be transferred */
	QSPI0->INDIRECTREADXFERNUMBYTES = readQueue[id].size;

	/* Trigger indirect read access by setting Indirect Read transfer control register bit */
	QSPI0->INDIRECTREADXFERCTRL = QSPI_INDIRECTREADXFERCTRL_START;
}

/*
	\brief
		Wait for one specific read transaction to complete.
*/
static void _waitRead(uint8_t id)
{
	bool done = false;
	uint32_t dumbDelay = 6000000;

	while (!done && (dumbDelay--)) {
		done = readQueue[id].state == QSPI_STATE_DONE;
	}
}

/*
	\brief
		Wait for one specific write transaction to complete
*/
static void _waitWrite(int id)
{
	bool done = false;
	uint32_t dumbDelay = 6000000;

	while (!done && (dumbDelay--)) {
		done = writeQueue[id].state == QSPI_STATE_DONE;
	}
}

/*
	\brief
		Initialize QSPI for indirect access

	\detail
   		This function must be called one time before any calls to
   		QSPI_WriteIndirect() or QSPI_ReadIndirect(). This function will modify
    	configuration registers of the QSPI in order to make it ready for indirect
    	operations.
*/
void _QSPI_init_indirect(void)
{
	/* Enable indirect access by disabling direct access */
	QSPI0->CONFIG = QSPI0->CONFIG & ~_QSPI_CONFIG_ENBDIRACCCTLR_MASK;

	/* Configure QSPI FIFO address */
	QSPI0->INDAHBADDRTRIGGER = (uint32_t)qspiFifo;

	/* Setup the indirect transfers AHB trigger address range */
	QSPI0->INDIRECTTRIGGERADDRRANGE = 0xF;

	/* Set the watermark registers */
	QSPI0->INDIRECTWRITEXFERWATERMARK = 0x100;
	QSPI0->INDIRECTREADXFERWATERMARK  = 0x10;

	/* Partition the FIFO into 512 bytes read and 512 bytes write */
	QSPI0->SRAMPARTITIONCFG = QSPI_FIFO_READ_PART / 4;

	/* Enable completion and watermark interrupt */
	QSPI0->IRQMASK = QSPI_IRQMASK_INDIRECTOPDONEMASK
				   | QSPI_IRQMASK_INDIRECTXFERLEVELBREACHMASK
				   | QSPI_IRQMASK_INDIRECTREADREJECTMASK;

	for (int i = 0; i < QSPI_QUEUE_SIZE; i++) {
		writeQueue[i].state = QSPI_STATE_DONE;
		readQueue[i].state = QSPI_STATE_DONE;
	}
}

static REEQSPI_err_t _QSPI_init_config(REEQSPI_t * qspi) {
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_QSPI0, true);

	/* Make sure QSPI module is idle */
	while (!(QSPI0->CONFIG & _QSPI_CONFIG_IDLE_MASK)) {
	}


	QSPI_Init_TypeDef initQspi = QSPI_INIT_DEFAULT;
	if (qspi->ctl.divisor >= 2 && qspi->ctl.divisor <= 32) {
		initQspi.divisor = qspi->ctl.divisor;
	}
	QSPI_Init(QSPI0, &initQspi);

	/* Set the chip select peripheral line */
	QSPI0->CONFIG |= (qspi->ctl.csLine << _QSPI_CONFIG_PERIPHCSLINES_SHIFT);

	/* Configure QSPI pins. */
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.CS0),  REEIO_PIN(qspi->ctl.CS0),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.CS1),  REEIO_PIN(qspi->ctl.CS1),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.SCK),  REEIO_PIN(qspi->ctl.SCK),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.DQ0),  REEIO_PIN(qspi->ctl.DQ0),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.DQ1),  REEIO_PIN(qspi->ctl.DQ1),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.DQ2),  REEIO_PIN(qspi->ctl.DQ2),    gpioModePushPull, 0);
	GPIO_PinModeSet(REEIO_PORT(qspi->ctl.DQ3),  REEIO_PIN(qspi->ctl.DQ3),    gpioModePushPull, 0);

	/* Configure QSPI routing to GPIO. */
	QSPI0->ROUTELOC0 = qspi->ctl.QSPIlocation;

	QSPI0->ROUTEPEN  = QSPI_ROUTEPEN_SCLKPEN
					 | QSPI_ROUTEPEN_CS0PEN
					 | QSPI_ROUTEPEN_CS1PEN
					 | QSPI_ROUTEPEN_DQ0PEN
					 | QSPI_ROUTEPEN_DQ1PEN
					 | QSPI_ROUTEPEN_DQ2PEN
					 | QSPI_ROUTEPEN_DQ3PEN;

	/* Set the read configuration */
	QSPI_ReadConfig_TypeDef readConfig = QSPI_READCONFIG_DEFAULT;
	if (qspi->mem.readConfig != NULL) {
		readConfig.dummyCycles  = qspi->mem.readConfig->dummyCycles;
		readConfig.opCode       = qspi->mem.readConfig->opCode;
		readConfig.instTransfer = qspi->mem.readConfig->instTransfer;
		readConfig.addrTransfer = qspi->mem.readConfig->addrTransfer;
		readConfig.dataTransfer = qspi->mem.readConfig->dataTransfer;
	}

	QSPI_ReadConfig(QSPI0, &readConfig);

	/* Set the write configuration */
	QSPI_WriteConfig_TypeDef writeConfig = QSPI_WRITECONFIG_DEFAULT;
	if (qspi->mem.writeConfig != NULL) {
		writeConfig.dummyCycles  = qspi->mem.writeConfig->dummyCycles;
		writeConfig.opCode       = qspi->mem.writeConfig->opCode;
		writeConfig.addrTransfer = qspi->mem.writeConfig->addrTransfer;
		writeConfig.dataTransfer = qspi->mem.writeConfig->dataTransfer;
		writeConfig.autoWEL      = qspi->mem.writeConfig->autoWEL;
	}
	QSPI_WriteConfig(QSPI0, &writeConfig);

	/* Set the DEVSIZECONFIG register */
	if (qspi->mem.deviceSizeConfig != NULL) {
		QSPI0->DEVSIZECONFIG = (qspi->mem.deviceSizeConfig->memSizeCS0 << _QSPI_DEVSIZECONFIG_MEMSIZEONCS0_SHIFT)
									| (qspi->mem.deviceSizeConfig->memSizeCS1 << _QSPI_DEVSIZECONFIG_MEMSIZEONCS1_SHIFT)
									| (qspi->mem.deviceSizeConfig->bytesPerBlock << _QSPI_DEVSIZECONFIG_BYTESPERSUBSECTOR_SHIFT)
									| (qspi->mem.deviceSizeConfig->bytesPerPage << _QSPI_DEVSIZECONFIG_BYTESPERDEVICEPAGE_SHIFT)
									| (qspi->mem.deviceSizeConfig->numAddressBytes << _QSPI_DEVSIZECONFIG_NUMADDRBYTES_SHIFT);
	}

	/* Set the WRITECOMPLETIONCTRL register */
	if (qspi->mem.writeCompletionConfig != NULL) {
		QSPI0->WRITECOMPLETIONCTRL = (qspi->mem.writeCompletionConfig->pollCount << _QSPI_WRITECOMPLETIONCTRL_POLLCOUNT_SHIFT)
											| (qspi->mem.writeCompletionConfig->disablePolling << _QSPI_WRITECOMPLETIONCTRL_DISABLEPOLLING_SHIFT)
											| (qspi->mem.writeCompletionConfig->enablePollingExp << _QSPI_WRITECOMPLETIONCTRL_ENABLEPOLLINGEXP_SHIFT)
											| (qspi->mem.writeCompletionConfig->opCode << _QSPI_WRITECOMPLETIONCTRL_OPCODE_SHIFT);
	}

	/* Disable Write protection */
	QSPI0->WRPROTCTRL = 0;

	/* Set the Device Delay register */
	if (qspi->mem.devDelayConfig != NULL) {
		QSPI0->DEVDELAY = (qspi->mem.devDelayConfig->dnss << _QSPI_DEVDELAY_DNSS_SHIFT)
							| (qspi->mem.devDelayConfig->dafter<< _QSPI_DEVDELAY_DAFTER_SHIFT)
							| (qspi->mem.devDelayConfig->dbtwn << _QSPI_DEVDELAY_DBTWN_SHIFT)
							| (qspi->mem.devDelayConfig->dinit << _QSPI_DEVDELAY_DINIT_SHIFT);
	}

	if (qspi->mem.phyConfig != NULL) {
		/* Set values for the DLL (Delayed Lock-Loop) used for timing in PHY mode
		 * TX and RX DLL values copied from datasheet */
		QSPI0->PHYCONFIGURATION = (qspi->mem.phyConfig->phyRxDll << _QSPI_PHYCONFIGURATION_PHYCONFIGRXDLLDELAY_SHIFT)
								| (qspi->mem.phyConfig->phyTxDll << _QSPI_PHYCONFIGURATION_PHYCONFIGTXDLLDELAY_SHIFT);
		QSPI0->PHYCONFIGURATION |= (QSPI_PHYCONFIGURATION_PHYCONFIGRESYNC);

		/* Enable PHY */
		QSPI0->CONFIG |= QSPI_CONFIG_PHYMODEENABLE;
	}

	if(qspi->ctl.accessMode == REEQSPI_access_DAC) {
		// Enable QSPI and DAC in config
		QSPI0->CONFIG |= (QSPI_CONFIG_ENBSPI | QSPI_CONFIG_ENBDIRACCCTLR);

	} else {
		/* Enable QSPI and indirect access */
		QSPI0->CONFIG = (QSPI0->CONFIG & ~_QSPI_CONFIG_ENBDIRACCCTLR_MASK)
					  | QSPI_CONFIG_ENBSPI;


		NVIC_EnableIRQ(QSPI0_IRQn);

		_QSPI_init_indirect();
	}

	return REEQSPI_success;
}

/*
	\brief	Write data to the external flash.

	\param[in] io			Pointer to a structure containing a buffer to receive data
	\param[out] id			The return value is the id of the read operation
	\return	REEQSPI_err_t 				Code
*/
REEQSPI_err_t _REEQSPI_indirect_write_act(REEQSPI_io_t * io, uint8_t * id)
{
	*id = writeHead;
	CORE_irqState_t irqState;

	// check if there is room in the queue
	if (writeQueue[writeHead].state != QSPI_STATE_DONE) {
		return REEQSPI_queue_full;
	}

	irqState = CORE_EnterCritical();
	// Save information about the operation in an array
	writeQueue[writeHead].addr = io->address;
	writeQueue[writeHead].size = io->size;
	writeQueue[writeHead].buffer = io->buffer;
	writeQueue[writeHead].state = QSPI_STATE_READY;

	// Check that we do not have an indirect write in progress already
	if ((QSPI0->INDIRECTWRITEXFERCTRL & _QSPI_INDIRECTWRITEXFERCTRL_WRSTATUS_MASK) == 0) {
		// Can safely trigger operation to be started
		_triggerWriteIndirect(writeHead);
	}
	CORE_ExitCritical(irqState);

	// Move to next element in the queue
	writeHead++;
	if (writeHead >= QSPI_QUEUE_SIZE) {
		writeHead = 0;
	}

	return REEQSPI_success;
}

/*
	\brief	Read data from the external flash..

	\param[in] io			Pointer to a structure containing a buffer to receive data
	\param[out] id			The return value is the id of the read operation
	\return	REEQSPI_err_t 				Code
*/
REEQSPI_err_t _REEQSPI_indirect_read_act(REEQSPI_io_t * io, uint8_t * id)
{
	*id = readHead;
	CORE_irqState_t irqState;

	// check if there is room in the queue
	if (readQueue[readHead].state != QSPI_STATE_DONE) {
		return REEQSPI_queue_full;
	}

	irqState = CORE_EnterCritical();
	// Save information about the operation in an array
	readQueue[readHead].addr = io->address;
	readQueue[readHead].size = io->size;
	readQueue[readHead].buffer = io->buffer;
	readQueue[readHead].state = QSPI_STATE_READY;

	// Check that we do not have an indirect read in progress already
	if ((QSPI0->INDIRECTREADXFERCTRL & _QSPI_INDIRECTREADXFERCTRL_RDSTATUS_MASK) == 0) {
		_triggerReadIndirect(readHead);
	}
	CORE_ExitCritical(irqState);

	// Move to next element in the queue
	readHead++;
	if (readHead >= QSPI_QUEUE_SIZE) {
		readHead = 0;
	}

	return REEQSPI_success;
}

/**
 * Perform a direct read from the memory. Every access are aligned by 32bits
 */
static REEQSPI_err_t _REEQSPI_direct_read_act(REEQSPI_io_t * io) {

	/* Read data if any */
	if (io->size) {
		volatile uint32_t * memory = (volatile uint32_t *) QSPI0_MEM_BASE; /* TODO : Figure out why is this behaving differently than QSPI0_CODE_MEM_BASE*/
		uint32_t data = 0;
		uint32_t addr = io->address / 4;
		uint8_t * dst = io->buffer;
		uint8_t * src = (uint8_t *)&data;
		uint16_t i, j;
		for (i = 0; i < io->size; i+=4) {
			/* Read 32 bit of data from memory */
			data = memory[addr];

			/* Increment address offset */
			addr++;

			/* Convert 32bit of data into bytes. */
			for (j = 0; (j+i) < io->size && j < 4; j++) {
				dst[j + i] = src[j];
			}
		}
	}
	return REEQSPI_success;
}

/**
 * Perform a direct write to the memory. Every access are aligned by 32bits
 */
static REEQSPI_err_t _REEQSPI_direct_write_act(REEQSPI_io_t * io) {

	/* Write data if any */
	if (io->size) {
		volatile uint32_t * memory = (volatile uint32_t *) QSPI0_CODE_MEM_BASE; /* TODO : Figure out why is this behaving differently than QSPI0_MEM_BASE*/
		uint32_t data = 0;
		uint32_t addr = io->address / 4;
		uint8_t * src = io->buffer;
		uint8_t * dst = (uint8_t *)&data;
		uint16_t i, j;
		for (i = 0; i < io->size; i+=4) {
			data = 0;

			/* Convert bytes to 32bit data. */
			for (j = 0; (j+i) < io->size && j < 4; j++) {
				dst[j] = src[j + i];
			}

			/* Write 32 bit of data from memory */
			memory[addr] = data;

			/* Increment address offset */
			addr++;
		}
	}
	return REEQSPI_success;
}

/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */

// TODO : Currently, there is no support for RTOS, but
// boilerplate is here for the future integration.
REEQSPI_err_t REEQSPI_take(REEQSPI_t * qspi) {

	// Take control of the hardware
	if (qspi != NULL) {

		// ACQUIRE MUTEX HERE
		REEQSPI_err_t code;

		code = _QSPI_init_config(qspi);

		if (code != REEQSPI_success) {
			return code;
		}
	}
	return REEQSPI_success;
}

// TODO : Currently, there is no support for RTOS, but
// boilerplate is here for the future integration.
REEQSPI_err_t REEQSPI_release(REEQSPI_t * qspi) {
	/* find the correct mutex */
	return REEQSPI_success;
}

REEQSPI_err_t REEQSPI_ioctl(REEQSPI_t * qspi, REEQSPI_ioctl_t * arg) {
	if (qspi != NULL && arg != NULL) {
		REEQSPI_err_t code;
		switch (arg->action) {
			case REEQSPI_act_stigCmd: {
				QSPI_ExecStigCmd(QSPI0, arg->stigCmd);
				code = REEQSPI_success;
				break;
			}
			case REEQSPI_act_direct_write: {
				code = _REEQSPI_direct_write_act(arg->io);
				break;
			}
			case REEQSPI_act_direct_read: {
				code = _REEQSPI_direct_read_act(arg->io);
				break;
			}
			case REEQSPI_act_indirect_read: {
				uint8_t id;
				code = _REEQSPI_indirect_read_act(arg->io, &id);
				/* In blocking state until the read operation finish */
				if (qspi->ctl.blocking && code == REEQSPI_success) {
					_waitRead(id);
				}
				break;
			}
			case REEQSPI_act_indirect_write: {
				uint8_t id;
				code = _REEQSPI_indirect_write_act(arg->io, &id);
				/* In blocking state until the write operation finish */
				if (qspi->ctl.blocking && code == REEQSPI_success) {
					_waitWrite(id);
				}
				break;
			}
			default: {
				return REEQSPI_param;
			}
		}
		return code;
	} else {
		return REEQSPI_param;
	}
}

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"

#endif 	/*QSPI_COUNT*/
