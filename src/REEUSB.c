/***************************************************************************//**
 * @file REEUSB.c
 * @brief USB Communication Device Class (CDC) driver for a VCOM echo based on cdc.c example from silabs
 * application.
 * @version 1.0
 *
 ******************************************************************************/


// Generic includes
#include "em_device.h"
#include "em_core.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_chip.h"
#include "em_usb.h"

// USB specific includes
#include "REEUSB.h"
#include "descriptors.h"

#include <em_usbtypes.h>	/* Endpoint status type*/
#include <em_usbd.h>		/*Get endpoint status*/
#include "ringbuf.h"
#include "REEutil.h"

#if defined(USB_PRESENT) && (USB_COUNT == 1)

// The serial port LINE CODING data structure, used to carry information
// about serial port baudrate, parity, etc. between host and device.
SL_PACK_START(1)
typedef struct {
  uint32_t dwDTERate;   // Baudrate
  uint8_t  bCharFormat; // Stop bits: 0 = one stop bit, 1 = 1.5 stop bits, 2 = two stop bits
  uint8_t  bParityType; // Parity: 0 = none, 1 = odd, 2 = even, 3 = mark, 4 = space
  uint8_t  bDataBits;   // Data bits: 5, 6, 7, 8, or 16
  uint8_t  dummy;       // To ensure size is a multiple of 4 bytes
} SL_ATTRIBUTE_PACKED cdcLineCoding_TypeDef;
SL_PACK_END()

// The LineCoding variable must be 4-byte aligned
SL_ALIGN(4)
SL_PACK_START(1)
static cdcLineCoding_TypeDef SL_ATTRIBUTE_ALIGN(4) cdcLineCoding = {
  115200, // Baud rate
  0,      // One stop bit
  0,      // No parity bits
  8,      // 8 data bits
  0       // Dummy value to ensure size is a multiple of 4 bytes
};
SL_PACK_END()

// Note: change this to change the receive buffer size
// By default, the receive buffer size is same size as the max size of a full speed bulk endpoint
#define CDC_USB_BUF_SIZE  (1020)
//#define CDC_USB_BUF_SIZE  (USB_FS_BULK_EP_MAXSIZE)

// Create a 4-byte aligned uint8_t array for the USB receive buffer
STATIC_UBUF(usbRxBuffer, CDC_USB_BUF_SIZE);
STATIC_UBUF(usbTxBuffer, CDC_USB_BUF_SIZE);

#ifndef USB_INTF_TX_BUF_SIZE
    #define USB_INTF_TX_BUF_SIZE   (CDC_USB_BUF_SIZE*2)
#endif
#ifndef USB_INTF_RX_BUF_SIZE
    #define USB_INTF_RX_BUF_SIZE   (CDC_USB_BUF_SIZE*2)
#endif


static uint8_t usbIntf_txBuffer[USB_INTF_TX_BUF_SIZE];
static uint8_t usbIntf_rxBuffer[USB_INTF_RX_BUF_SIZE];
rBuf_t usbIntf_txRbufHandle,usbIntf_rxRbufHandle;


volatile uint8_t isConnected = 0;

// Global for letting us know if USB data transmission is currently in progress or not
volatile uint8_t usbTxActive;

// Function prototypes for receiving/transmitting data over USB
static int usbDataReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);
static int usbDataTransmitted(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);


/*
 * Custom callback on receive and transmit
 * */
static volatile REEUSB_onReceive_cb_t REEUSB_onReceive_cb 	= NULL;

uint32_t stat_timestamp=0;
uint32_t stat_tx_count=0;
uint32_t stat_rx_count=0;

/**************************************************************************//**
* @brief
*    Callback that gets called when the data stage of a CDC_SET_LINECODING
*    setup command has completed
*
* @param[in] status
*    Transfer status code.
*
* @param[in] xferred
*    Number of bytes transferred.
*
* @param[in] remaining
*    Number of bytes not transferred.
*
* @return
*    USB_STATUS_OK if data accepted.
*    USB_STATUS_REQ_ERR if data calls for modes we can not support.
*****************************************************************************/
static int lineCodingReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining)
{
 (void) remaining;
 uint32_t frame = 0;

 // We have received new serial port communication settings from USB host
 if ((status == USB_STATUS_OK) && (xferred == 7)) {

   // Check bDataBits, valid values are: 5, 6, 7, 8 or 16 bits
   if (cdcLineCoding.bDataBits == 5) {
     frame |= USART_FRAME_DATABITS_FIVE;
   } else if (cdcLineCoding.bDataBits == 6) {
     frame |= USART_FRAME_DATABITS_SIX;
   } else if (cdcLineCoding.bDataBits == 7) {
     frame |= USART_FRAME_DATABITS_SEVEN;
   } else if (cdcLineCoding.bDataBits == 8) {
     frame |= USART_FRAME_DATABITS_EIGHT;
   } else if (cdcLineCoding.bDataBits == 16) {
     frame |= USART_FRAME_DATABITS_SIXTEEN;
   } else {
     return USB_STATUS_REQ_ERR;
   }

   // Check bParityType, valid values are: 0=None 1=Odd 2=Even 3=Mark 4=Space
   if (cdcLineCoding.bParityType == 0) {
     frame |= USART_FRAME_PARITY_NONE;
   } else if (cdcLineCoding.bParityType == 1) {
     frame |= USART_FRAME_PARITY_ODD;
   } else if (cdcLineCoding.bParityType == 2) {
     frame |= USART_FRAME_PARITY_EVEN;
   } else if (cdcLineCoding.bParityType == 3) {
     return USB_STATUS_REQ_ERR;
   } else if (cdcLineCoding.bParityType == 4) {
     return USB_STATUS_REQ_ERR;
   } else {
     return USB_STATUS_REQ_ERR;
   }

   // Check bCharFormat, valid values are: 0=1 1=1.5 2=2 stop bits
   if (cdcLineCoding.bCharFormat == 0) {
     frame |= USART_FRAME_STOPBITS_ONE;
   } else if (cdcLineCoding.bCharFormat == 1) {
     frame |= USART_FRAME_STOPBITS_ONEANDAHALF;
   } else if (cdcLineCoding.bCharFormat == 2) {
     frame |= USART_FRAME_STOPBITS_TWO;
   } else {
     return USB_STATUS_REQ_ERR;
   }

   return USB_STATUS_OK;
 }
 return USB_STATUS_REQ_ERR;
}

/**************************************************************************//**
 * @brief
 *    Callback that gets called whenever a USB setup command is received from
 *    the host.
 *
 * @param[in] setup
 *    Pointer to a USB setup packet
 *
 * @return
 *    USB_STATUS_OK --> if command was accepted
 *    USB_STATUS_REQ_UNHANDLED --> when command is unknown, the USB device
 *                                 stack will handle the request.
 *****************************************************************************/
int cdcSetupCmd(const USB_Setup_TypeDef *setup)
{
  int retVal = USB_STATUS_REQ_UNHANDLED;
  if ((setup->Type == USB_SETUP_TYPE_CLASS) && (setup->Recipient == USB_SETUP_RECIPIENT_INTERFACE)) {

    // Determine the type of setup request
    switch (setup->bRequest) {

      // USB host is trying to get the line coding settings from the device
      case USB_CDC_GETLINECODING:
        if ((setup->wValue == 0)
              && (setup->wIndex == CDC_CTRL_INTERFACE_NO) // Interface number
              && (setup->wLength == 7)                    // Length of cdcLineCoding
              && (setup->Direction == USB_SETUP_DIR_IN))  // Transfer direction (from host perspective)
        {
          USBD_Write(0, (void*) &cdcLineCoding, 7, NULL); // Send current settings to the host
          retVal = USB_STATUS_OK;
        }
        break;

      // USB host is trying to set the device's line coding settings
      case USB_CDC_SETLINECODING:
        if ((setup->wValue == 0)
              && (setup->wIndex == CDC_CTRL_INTERFACE_NO) // Interface number
              && (setup->wLength == 7)                    // Length of cdcLineCoding
              && (setup->Direction == USB_SETUP_DIR_OUT)) // Transfer direction (from host perspective)
        {
          USBD_Read(0, (void*) &cdcLineCoding, 7, lineCodingReceived); // Get new settings from the host
          retVal = USB_STATUS_OK;
        }
        break;

      // RS-232 signal used to tell the DCE device the DTE device is now present
      case USB_CDC_SETCTRLLINESTATE:
        if ((setup->wIndex == CDC_CTRL_INTERFACE_NO) // Interface number
              && (setup->wLength == 0))              // No data
        {
          retVal = USB_STATUS_OK; // Do nothing (non-compliant behaviour)
        }
        break;
    }
  }

  return retVal;
}

/**************************************************************************//**
 * @brief
 *    Callback that gets called each time the USB device state is changed.
 *    Also starts CDC operation once the device has been configured by the
 *    USB host.
 *
 * @details
 *    This example doesn't do anything with the state transition from the
 *    suspended state to the configured state. The code checks for this
 *    transition only so that the user could add functionality if they so
 *    desired. The same goes for the the state transition from configured to
 *    anything but the suspended state. The same also goes for any state
 *    transition to the suspended state.
 *
 * @note
 *    Refer to section 4 of the AN0065 USB Device application note for the
 *    USB stack's state machine
 *
 * @param[in] oldState
 *    The old USB device state
 *
 * @param[in] newState
 *    The new (current) USB device state
 *****************************************************************************/
void cdcStateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState){

	#ifdef DEBUG
		REEBUGSWO_String("USB STATE:");
		REEBUGSWO_String(USBD_GetUsbStateName(newState));
		REEBUGSWO_String("\n");
	#endif

	isConnected = (newState == USBD_STATE_CONFIGURED) ||  (newState == USBD_STATE_ADDRESSED);

	if (newState == USBD_STATE_CONFIGURED) {
		// If the USB device was configured
		isConnected = true;
		// If we transitioned from the suspended state to the configured state due to bus activity
		if (oldState == USBD_STATE_SUSPENDED) {} // Currently does nothing

		// Initially, we are waiting to receive data from the USB host over USB
		usbTxActive = false;

		// Setup a new USB receive transfer on the USB host's OUT endpoint
		USBD_Read(CDC_EP_DATA_OUT, (void*) usbRxBuffer, CDC_USB_BUF_SIZE, usbDataReceived);


	}else if ((oldState == USBD_STATE_CONFIGURED) && (newState != USBD_STATE_SUSPENDED)) {
		// Else if we have been de-configured


	}else if (newState == USBD_STATE_SUSPENDED) {
		// Else if we have been suspended
	}
}

void cdcUsbReset(void){
    REEBUGSWO_String("USBD_CDC_RESET_RECEIVED\n");
}

/**************************************************************************//**
 * @brief
 *    Callback function that gets called whenever data is received from the
 *    host over USB
 *
 * @param[in] status
 *    Transfer status code
 *
 * @param[in] xferred
 *    Number of bytes transferred
 *
 * @param[in] remaining
 *    Number of bytes not transferred
 *
 * @return
 *    USB_STATUS_OK
 *****************************************************************************/
static int usbDataReceived(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining){
	(void) remaining; // Unused parameter

	// If the status is OK and we actually received data
	if ((status == USB_STATUS_OK) && (xferred > 0)){
		remaining = rBufArrayPush(&usbIntf_rxRbufHandle, usbRxBuffer, xferred);

	    if(REEUSB_onReceive_cb){
            REEUSB_onReceive_cb();
		}

		USBD_Read(CDC_EP_DATA_OUT, (void*) usbRxBuffer, CDC_USB_BUF_SIZE, usbDataReceived);
	}
	return USB_STATUS_OK;
}

/**************************************************************************//**
 * @brief
 *    Callback function that gets called whenever a packet with data has
 *    been transmitted over USB to the USB host
 *
 * @param[in] status
 *    Transfer status code
 *
 * @param[in] xferred
 *    Number of bytes transferred
 *
 * @param[in] remaining
 *    Number of bytes not transferred
 *
 * @return
 *    USB_STATUS_OK.
 *****************************************************************************/
static int usbDataTransmitted(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining){
	(void) xferred;   // Unused parameter
	(void) remaining; // Unused parameter
	// Mark that the USB transmit transaction has been completed
	static uint16_t _qty=0;
	 USBD_Ep_TypeDef *ep = USBD_GetEpFromAddr(CDC_EP_DATA_IN);
	 if(ep->state != D_EP_IDLE){
		 return status;
	 }

	if(status == USB_STATUS_OK){
	    if(rBufIsEmpty(&usbIntf_txRbufHandle)){
            /*Everything sent*/
            usbTxActive = false;
	    }else{
            /*Extracted next usb frame*/
            usbTxActive = true;

            /* Copy content to statically allocated transmit buffer*/
            _qty =  rBufArrayPull(&usbIntf_txRbufHandle, usbTxBuffer, sizeof(usbTxBuffer));

            /* Setup a USB transmit transfer    (In usb standard, DATA_IN = to_host) */
            USBD_Write(CDC_EP_DATA_IN, (void*) usbTxBuffer, _qty, usbDataTransmitted);
	    }

#ifdef DEBUG
	}else{
		REEBUGSWO_String("usbDataTransmitted:");
		switch(status){
			case USB_STATUS_OK:						{ REEBUGSWO_String("OK");					break;  }
			case USB_STATUS_REQ_ERR:				{ REEBUGSWO_String("REQ_ERR");				break;  }
			case USB_STATUS_EP_BUSY:				{ REEBUGSWO_String("EP_BUSY");				break;  }
			case USB_STATUS_REQ_UNHANDLED:			{ REEBUGSWO_String("REQ_UNHANDLED");		break;  }
			case USB_STATUS_ILLEGAL:				{ REEBUGSWO_String("ILLEGAL");				break;  }
			case USB_STATUS_EP_STALLED:				{ REEBUGSWO_String("EP_STALLED");			break;  }
			case USB_STATUS_EP_ABORTED:				{ REEBUGSWO_String("EP_ABORTED");			break;  }
			case USB_STATUS_EP_ERROR:				{ REEBUGSWO_String("EP_ERROR");				break;  }
			case USB_STATUS_EP_NAK:					{ REEBUGSWO_String("EP_NAK");				break;  }
			case USB_STATUS_DEVICE_UNCONFIGURED:	{ REEBUGSWO_String("DEVICE_UNCONFIGURED");	break;  }
			case USB_STATUS_DEVICE_SUSPENDED:		{ REEBUGSWO_String("DEVICE_SUSPENDED");		break;  }
			case USB_STATUS_DEVICE_RESET:			{ REEBUGSWO_String("DEVICE_RESET");			break;  }
			case USB_STATUS_TIMEOUT:				{ REEBUGSWO_String("TIMEOUT");				break;  }
			case USB_STATUS_DEVICE_REMOVED:			{ REEBUGSWO_String("DEVICE_REMOVED");		break;  }
			case USB_STATUS_HC_BUSY:				{ REEBUGSWO_String("HC_BUSY");				break;  }
			case USB_STATUS_DEVICE_MALFUNCTION:		{ REEBUGSWO_String("DEVICE_MALFUNCTION");	break;  }
			case USB_STATUS_PORT_OVERCURRENT:		{ REEBUGSWO_String("PORT_OVERCURRENT");		break;  }
			default:								{ REEBUGSWO_String("UNDEF");				break;  }
		}
#endif
	}
	return USB_STATUS_OK;
}



uint16_t REEUSB_tx(uint8_t *buffer, uint16_t len){
    uint16_t queuedLen = 0;

    if((buffer != NULL) && (len>0)){
        queuedLen = rBufArrayPush(&usbIntf_txRbufHandle, buffer, len);
        stat_tx_count += queuedLen;
    }

	if(REEUSB_isReady() && !REEUSB_isBusy()) {
		usbDataTransmitted(USB_STATUS_OK, 0, 0);
	}

	return queuedLen;
}

uint16_t REEUSB_rx(uint8_t *buffer, uint16_t maxLen){
    uint16_t queuedLen = 0;

    queuedLen = rBufArrayPull(&usbIntf_rxRbufHandle, buffer, maxLen);
    stat_rx_count += queuedLen;

    return queuedLen;
}


// Set the callback functions (see src/cdc.c)
 static const USBD_Callbacks_TypeDef callbacks = {
	.usbReset        = cdcUsbReset,         // Called whenever USB reset signaling is detected on the USB port.
	.usbStateChange  = cdcStateChangeEvent, // Called when the device changes state
	.setupCmd        = cdcSetupCmd,         // Called on each setup request from the host
	.isSelfPowered   = NULL,
	.sofInt          = NULL
 };

 // Set the initialization struct descriptors (see src/descriptors.c)
 static const USBD_Init_TypeDef usbInitStruct = {
	.deviceDescriptor    = &USBDESC_deviceDesc,
	.configDescriptor    = USBDESC_configDesc,
	.stringDescriptors   = USBDESC_strings,
	.numberOfStrings     = sizeof(USBDESC_strings) / sizeof(void*),
	.callbacks           = &callbacks,
	.bufferingMultiplier = USBDESC_bufferingMultiplier,
	.reserved            = 0
 };


#define __u2h(u)  ( (u<10) ? (u+'0') : ((u-10)+'A'))

 /*len(26), type(str), #,0,#,0,#,...  */
volatile uint8_t iSerialNumber[24] = {
    22, 0x03,
    '0', 0,     /*char, 0?*/
    'x', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/

    '0', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/

    '0', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/
    '0', 0,     /*char, 0?*/
     0,  0      /*char, 0?*/
};

void REEUSB_init(REEUSB_onReceive_cb_t onReceive_cb){

    uint8_t nib;
    char c;
    uint64_t unique_id = SYSTEM_GetUnique();
    uint8_t *serial = (uint8_t *)iSerialNumber;

    for(uint8_t i=0; i<8; i++){
        nib = ((unique_id>>(28-(4*i))) & 0x0F);
        c = __u2h(nib);
        serial[((2*i)+6)] = c;
    }

	USB_Status_TypeDef status = USB_STATUS_DEVICE_UNCONFIGURED;
	REEUSB_onReceive_cb = onReceive_cb;

    if(rBufSetup(&usbIntf_rxRbufHandle, usbIntf_rxBuffer, sizeof(usbIntf_rxBuffer)) == false){
        REEBUGSWO_String("usbIntf_rxRbufHandle init failed\n");
    }

    if(rBufSetup(&usbIntf_txRbufHandle, usbIntf_txBuffer, sizeof(usbIntf_txBuffer)) == false){
        REEBUGSWO_String("usbIntf_txRbufHandle init failed\n");
    }

	status = USBD_Init(&usbInitStruct);  // Initialize and start USB device stack
#ifdef DEBUG
	if(status != USB_STATUS_OK){
		REEBUGSWO_String("USBD_Init failed");
	}
#endif
    stat_timestamp = 0;
}


uint8_t REEUSB_isBusy(void) {
	return usbTxActive;
}

uint8_t REEUSB_isReady(void) {
    return USBD_GetUsbState() == USBD_STATE_CONFIGURED;
}
uint8_t REEUSB_isSuspended(void) {
    return USBD_GetUsbState() == USBD_STATE_SUSPENDED;
}

uint8_t REEUSB_isConnected(void) {
	return isConnected;
}

void REEUSB_disconnect(void){
	usbTxActive=false;
	USBD_Disconnect();
	usbTxActive=false;
    stat_timestamp = 0;
}

void REEUSB_connect(void){
    REEUSB_clear();
	USBD_Connect();
    stat_timestamp = 0;
}

void REEUSB_clear(void){
    rBufFlush(&usbIntf_rxRbufHandle);
    rBufFlush(&usbIntf_txRbufHandle);
	USBD_AbortAllTransfers();
	usbTxActive=false;
    stat_timestamp = 0;
}
#endif


bool REEUSB_txBufferFull(void){
    return rBufIsFull(&usbIntf_txRbufHandle);
}

bool REEUSB_rxBufferEmpty(void){
    return rBufIsEmpty(&usbIntf_rxRbufHandle);
}

uint16_t REEUSB_rxBufferQueued(void){
    return rBufUsedSpaceGet(&usbIntf_rxRbufHandle);
}

uint16_t REEUSB_txBufferAvailable(void){
    return rBufFreeSpaceGet(&usbIntf_txRbufHandle);
}

uint16_t REEUSB_txBufferSize(void){
    return rBufTotalSpaceGet(&usbIntf_txRbufHandle);
}

uint16_t REEUSB_rxBufferSize(void){
    return rBufTotalSpaceGet(&usbIntf_rxRbufHandle);
}

bool REEUSB_wakeUp(void){
    bool ok = false;
    USB_Status_TypeDef status = USBD_RemoteWakeup();
    switch(status){
        case USB_STATUS_ILLEGAL:    { REEBUGSWO_String("REEUSB_wakeup::illegal\n"); break; }
        case USB_STATUS_OK:         { REEBUGSWO_String("REEUSB_wakeup::done\n");    break; }
        default:                    { REEBUGSWO_String("REEUSB_wakeup::undef\n");   break; }
    }
    return ok;
}

/*  Fetch the average communication rate since last call
 *
 * \note    returned rate is in [bytes/millisecond] -> which represent the same as [KB/sec]
 * \arg     Pointer to write the communication rate ( in KBps ) [null pointer accepted to single-out a metric or to reset counters]
 *
 * Return value is True if the time interval was large enough (pointer were written to), false otherwise
 * */
bool REEUSB_statGetRxRate(uint16_t *tx_kbps, uint16_t * rx_kbps){

    bool valid = false;
    uint32_t dt = systickDelta(stat_timestamp);
    uint32_t temp;

    if((REEUSB_isReady() && tx_kbps && rx_kbps)==false){
        stat_timestamp = systickGet();
        stat_tx_count = 0;
        stat_rx_count = 0;

    }else if(dt > 1){

        temp = stat_tx_count / dt;
        *tx_kbps = (uint16_t) MIN(UINT16_MAX, temp);

        temp = stat_rx_count / dt;
        *rx_kbps = (uint16_t) MIN(UINT16_MAX, temp);

        stat_tx_count = 0;
        stat_rx_count = 0;
        stat_timestamp = systickGet();
        valid = true;
    }
    return valid;
}
