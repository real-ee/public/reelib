/*
	\name		REEcolor.c
	\author		Laurence DV
	\version	0.2.0
	\brief		Color Palettes and color types for all kind of purposes
	\note		
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEcolor.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */


void u32_to_rgb888(uint32_t in, rgb888_t *out){
    out->red = (in>>(RGB888_BLUEBITNB+RGB888_GREENBITNB)) & RGB888_REDMASK;
    out->green = (in>>RGB888_BLUEBITNB) & RGB888_GREENMASK;
    out->blue = (in>>0) & RGB888_BLUEMASK;
}

void u32_to_rgb332(uint32_t in, rgb332_t *out){
    out->red = (in>>(RGB332_BLUEBITNB+RGB332_GREENBITNB)) & RGB332_REDMASK;
    out->green = (in>>RGB332_BLUEBITNB) & RGB332_GREENMASK;
    out->blue = (in>>0) & RGB332_BLUEMASK;
}


rgb888_t rgb332_to_rgb888(rgb332_t in) {
	rgb888_t converted = {};

	converted.red =	(uint8_t)(((uint32_t)in.red  * RGB888_REDMAX)  / RGB332_REDMAX)  & RGB888_REDMASK;
    converted.green=(uint8_t)(((uint32_t)in.green* RGB888_GREENMAX)/ RGB332_GREENMAX)& RGB888_GREENMASK;
    converted.blue= (uint8_t)(((uint32_t)in.blue * RGB888_BLUEMAX) / RGB332_BLUEMAX) & RGB888_BLUEMASK;

	return converted;
}

rgb332_t rgb888_to_rgb332(rgb888_t in) {
	rgb332_t converted = {};

    converted.red = (uint8_t)(((uint32_t)in.red  * RGB332_REDMAX)  / RGB888_REDMAX)  & RGB332_REDMASK;
    converted.green=(uint8_t)(((uint32_t)in.green* RGB332_GREENMAX)/ RGB888_GREENMAX)& RGB332_GREENMASK;
    converted.blue= (uint8_t)(((uint32_t)in.blue * RGB332_BLUEMAX) / RGB888_BLUEMAX) & RGB332_BLUEMASK;


	return converted;
}


rgb888_t rgb888_blend(rgb888_t * inA, rgb888_t * inB, uint8_t ratio) {
	rgb888_t blended = {{0,0,0}};

	/* A. 2 pixel to blend */
	if ((inA != NULL) && (inB != NULL)) {
		blended.red =	(((uint32_t)inA->red * (uint32_t)ratio) + ((uint32_t)inB->red) * (uint32_t)(UINT8_MAX-ratio)) /UINT8_MAX;
		blended.green =	(((uint16_t)inA->green * (uint32_t)ratio) + ((uint16_t)inB->green) * (uint32_t)(UINT8_MAX-ratio)) /UINT8_MAX;
		blended.blue =	(((uint16_t)inA->blue * (uint32_t)ratio) + ((uint16_t)inB->blue) * (uint32_t)(UINT8_MAX-ratio)) /UINT8_MAX;
	}
	return blended;
}

rgb888_t rgb888_alpha(rgb888_t * in, uint8_t alpha) {
	rgb888_t work = {{0,0,0}};
	if (in != NULL) {
		work.red =		(uint8_t)(((float)alpha / (float)UINT8_MAX) * (float)in->red);
		work.green =	(uint8_t)(((float)alpha / (float)UINT8_MAX) * (float)in->green);
		work.blue =		(uint8_t)(((float)alpha / (float)UINT8_MAX) * (float)in->blue);
	}

	return work;
}

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
