/*
 * fletcher.c
 *
 *  Created on: May 4, 2020
 *      Author: jlecuyer
 */


#include <fletcher.h>

uint16_t fletcher_checksum(uint8_t *ptr, uint16_t len){
	uint8_t cka = 0;
	uint8_t ckb = 0;
	uint16_t i;
	for(i=0; i< len; i++){
		cka += ptr[i];
		ckb += cka;
	}
	return (ckb<<8) | cka;
}
