/*
 * REEutil.c
 *
 *  Created on: Aug 28, 2019
 *      Author: jlecuyer2
 */


#include <REEutil.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdint.h>

uint32_t systickDelta(uint32_t t0){
	uint32_t now = systickGet();
	if(now >= t0){
		return now - t0;
	}else{
		return (UINT32_MAX - t0) +  now;
	}
}


/*https://www.crockford.com/base32.html*/
size_t u64ToBase32_encode(char *ptr, uint64_t number, size_t max_len){

	if(max_len < BASE32_MAX_LEN){
		return 0;
	}else{
		memset(ptr, '0', BASE32_MAX_LEN+1);
	}

	unsigned int offset = BASE32_MAX_LEN;

	do {
		ptr[--offset] = BASE32[number % BASE_32_BASE];
	} while (number /= BASE_32_BASE);

	return BASE32_MAX_LEN;
}


/*SNPRINTF that return inserted character instead of ideal required buffer*/
int snprintf_try(char * ptr, size_t size, char *fmt, ...){
	if(size<1){
		return size;
	}
	int ret;
	va_list argp;
	va_start(argp, fmt);
	ret = vsnprintf(ptr, size, fmt, argp);
	va_end(argp);
	if(ret <1){
		return 0;
	}else{
		return MIN(((uint32_t)((uint32_t)size-1U)), (uint32_t) ret);	/*size -1 because of null ending */
	}
}



uint8_t REESCOPE_TO_U8(uint16_t x, uint16_t x1, uint16_t x2){
	/*Default to 0 for lower bound and div/0	*/
	uint32_t temp=0;

	if (x>=x2){
		temp = UINT8_MAX;

	}else if((x > x1) && (x1 != x2)){
		/*	256 * ((x-x1)/(x2-x1) */
		temp = x-x1;
		temp <<= 8;
		temp /= (x2-x1);
	}
	return (uint8_t) (temp & UINT8_MAX);
}

uint16_t REESCOPE_TO_U16(uint16_t x, uint16_t x1, uint16_t x2){
	/*Default to 0 for lower bound and div/0	*/
	uint32_t temp=0;

	if (x>=x2){
		temp = UINT16_MAX;

	}else if((x > x1) && (x1 != x2)){
		/*	65536 * ((x-x1)/(x2-x1) */
		temp = x-x1;
		temp <<= 16;
		temp /= (x2-x1);
	}
	return (uint16_t) (temp & UINT16_MAX);
}



// Return a value bound by 0 and U8MAX/U16MAX,
// eg: scale_range_to_u16(5, 0, 10)		--> UINT16_MAX/2
// eg: scale_range_to_u16(100, 50, 100)	--> UINT16_MAX
// eg: scale_range_to_u16(75, 75, 100)	--> 0
// Basically UINT16_MAX * (x-x1) / (x2-x1)
uint16_t scale_range_to_u16(uint16_t x, uint16_t x1, uint16_t x2){

	uint16_t scale;
	x = MIN(x, x2);	// Limit scope of x
	x = MAX(x, x1);	// Limit scope of x

	if(x1 < x2){		scale = x2-x1;			// Proper order
	}else if(x1 > x2){	scale = x1-x2;			// Error avoidance
	}else{				scale = UINT16_MAX;		// Div/0 avoidance (x1 == x2)
	}

	uint32_t temp  =(x-x1);
	temp *= UINT16_MAX;

	return  temp/scale;	// ((((uint32_t)((MAX(MIN(x,x2),x1))-(x1)))<<16)) / (MAX(1,((x2)-(x1)))) )
}


////////////////////////////////////////////////////////////////////////////////
// Interpolation with 2 point
//		|
// y1	|*******(P1)
//		|		*
//		|			*
//		|			*
//		|				*
// y2	|				(P2)**********
//		|_______________________________
//				x1		x2
//
uint16_t interpolation_2p(uint16_t x, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2){

	if (x <= x1){
		return y1;

	}else if (x >= x2){
		return y2;

	}else if (x2 <= x1){
		return 0;

	}else{
		uint16_t i = scale_range_to_u16(x, x1, x2) ;		// [x1--x--x2]		->  [0--i--255]
		return scale_u16_to_range((uint16_t)i, y1, y2);		// [0--i--255]	->	[y1--y--y2]
	}
}

uint16_t interpolation_4p(uint16_t x, uint16_t x1, uint16_t x2, uint16_t x3, uint16_t x4, uint16_t y1, uint16_t y2, uint16_t y3, uint16_t y4){
	if (x <= x1) {			return y1;				// <=0
	}else if (x >= x4){		return y4;				// >=4
	}else if (x == x2){		return y2;				// @2
	}else if (x == x3){		return y3;				// @3
	}else if ((x < x2) && (x2 > x1)){				// 1..2
		return interpolation_2p(x, x1, x2, y1, y2);

	}else if ((x > x2) && (x < x3) && (x3 > x2)){	// 2..3
		return interpolation_2p(x, x2, x3, y2, y3);

	}else if ((x > x3) && (x < x4) && (x4 > x3)){	// 3..4
		return interpolation_2p(x, x3, x4, y3, y4);

	}else{
		return 0;
	}
}

//#define REESCOPE_TO_U16(x, x1, x2)	 (x<=x1 ? 0 : (x>=x2 ? UINT16_MAX : ( (((uint32_t)((x)-(x1)))<<16)/((x2)-(x1)))))


