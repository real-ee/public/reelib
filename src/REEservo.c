/*
	@name		REEservo.c
	@author		Laurence DV
	@version	1.0.0
	@brief		Baremetal Servo driving with hw timer
	@note		
	@license	SEE $REElib_root/LICENSE.md
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEservo.h>
#include <REEcore.h>		/* core & clock related utilities */
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */

#include <em_device.h>		/* for HW module addresses */
#include <em_cmu.h>			/* Clock access */


/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Prototype				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Bare Metal API					|
|									|
+ -------------------------------- */
void REEservo_start(REEservo_t * servo) {
	if (servo != NULL) {
		/* Parse initial config */
		if (servo->clk == NULL) {
			REEcore_findCMUClock(servo->timer);
		}

		/* Save state */

		/* Init IO */
		CMU_ClockEnable(cmuClock_GPIO, true);

		/* Init timer */

	}
}

void REEservo_stop(REEservo_t * servo) {
	/* Restore state */

}

void REEservo_irq(void) {

}

void REEservo_setAngle(REEservo_t * servo, int32_t newAngle) {

}

int32_t REEservo_getAngle(REEservo_t * servo) {
	return servo->cur_uDeg;
}

REEservoState_t REEservo_getState(REEservo_t * servo) {
	return servo->state;
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */



#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
