/*
	@name		crc_hardware.h
	@author		Jonathan Lécuyer
	@version	1.0.0
	@brief		Use the General Purpose CRC hardware to compure a CRC32
	@note
	@license	E-SMART control 2020
*/


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */

#include <REECheck.h>
#include <em_gpcrc.h>

#include <stdbool.h>
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>
#include <REEutil.h>					/* For bit def and util */


#if defined(REELIB_USE_CMSISRTOS2)
	#include <cmsis_os2.h>
#endif

/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Prototype				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */

#if defined(REELIB_USE_CMSISRTOS2)
	osMutexId_t __crc_mutex = NULL;
	const osMutexAttr_t  __crc_mutex_att= {
		"gpCRC_mutex",     // human readable mutex name
		0U,      			 // attr_bits
		NULL,              // memory for control block
		0U                 // size for control block
	};
#endif

static bool initialized = false;


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */

#if defined(REELIB_USE_CMSISRTOS2)
	bool _crc_mutex_get(uint32_t timeout){
		if(__crc_mutex == 0){
			/* Initialize data and create mutex*/
			__crc_mutex =osMutexNew(&__crc_mutex_att);
		}
		return osOK == osMutexAcquire(__crc_mutex, timeout);
	}


	void _crc_mutex_release(void){
		osMutexRelease(__crc_mutex);
	}
#endif


bool _crc_prepare(void){
	bool ready = true;
	#if defined(REELIB_USE_CMSISRTOS2)
		ready &= _crc_mutex_get(50);
	#endif
	if(ready){
		/* The GPCRC is a high frequency peripheral so we need to enable the
		 * HFPER clock in addition to the GPCRC clock. */
		CMU_ClockEnable(cmuClock_HFPER, true);
		CMU_ClockEnable(cmuClock_GPCRC, true);

		/* Initialize GPCRC, 32-bit fixed polynomial is default */
		GPCRC_Init_TypeDef init = GPCRC_INIT_DEFAULT;
		init.initValue = 0xFFFFFFFF; // Standard CRC-32 init value
		GPCRC_Init(GPCRC, &init);
		GPCRC_Start(GPCRC);
	}
	return ready;
}


uint32_t _crc_free(void){
	uint32_t checksum;
    initialized = false;

	/* According to the CRC-32 specification, the end result should be inverted */
	checksum = ~GPCRC_DataRead(GPCRC);

	/* Reset peripheral for next use */
	GPCRC_Reset(GPCRC);

	#if defined(REELIB_USE_CMSISRTOS2)
		/* Release the mutex */
		_crc_mutex_release();
	#endif

	return checksum;
}


bool REECheck_crc_hwU8(uint8_t *data, uint16_t size){
    if(!initialized) {
        initialized = _crc_prepare();
    }

	if(initialized){
		for(uint16_t i=0; i<size; i++){
			GPCRC_InputU8(GPCRC, data[i]);
		}
	}

	return initialized;
}


bool REECheck_crc_hwU32(uint32_t *data, uint16_t size){
    if(!initialized) {
        initialized = _crc_prepare();
    }

	if(initialized) {
		for(uint16_t i=0; i<size; i++){
			GPCRC_InputU32(GPCRC, data[i]);
		}
	}

	return initialized;
}

uint32_t REECheck_crc_getCRC() {
    return _crc_free();
}
