/*
 * RingBuffer.c
 *
 *  Created on: 2015-02-20
 *      Author: Laurence DV
 */
#include "ringbuf.h"

/* -------------------------------- +
|   Global Variable                 |
+ ---------------------------------*/

/* -------------------------------- +
|   Function                        |
+ ---------------------------------*/
// Setup rbuf without any alloc
bool rBufSetup(rBuf_t * rbufHandle, uint8_t *buffer, uint16_t size){
    bool valid = false;
    // Check if the size is valid
    if( (rbufHandle != NULL) && (buffer != NULL) && (size != 0)){
        rbufHandle->buffer = buffer;
        rbufHandle->bufferSize = size;
        rbufHandle->pulled = 0;
        rbufHandle->pushed = 0;
        rbufHandle->in = buffer;
        rbufHandle->out = buffer;
        rbufHandle->end = buffer+size-1;
        valid = true;
    }
    return valid;
}


// Management functions
rBuf_t * rBufCreate(uint16_t size){
    // Check if the size is valid
    if(size == 0){
        return NULL;

    }else{
        rBuf_t * tempPtr;

        // -- Allocate the control -- //
        tempPtr = (rBuf_t*)malloc(sizeof(rBuf_t));
        if (tempPtr == NULL){
            return NULL;
        }
        // -------------------------- //

        // -- Allocate the buffer -- //
        tempPtr->buffer = (uint8_t*)malloc(size);
        if (tempPtr->buffer == NULL){
            free(tempPtr);
            return NULL;
        }
        // ------------------------- //

        // -- Init the buffer -- //
        tempPtr->bufferSize = size;
        tempPtr->pulled = 0;
        tempPtr->pushed = 0;
        tempPtr->in = tempPtr->buffer;
        tempPtr->out = tempPtr->buffer;
        tempPtr->end = (tempPtr->buffer)+size-1;
        // --------------------- //

        return tempPtr;
    }
}


void rBufDestroy(rBuf_t * rBuf)
{
    if (rBuf == NULL){
        return;
    }else{
        // -- Desallocate the buffer -- //
        free((void *)(rBuf->buffer));
        // ---------------------------- //

        // -- Desallocate the control -- //
        free(rBuf);
        // ----------------------------- //
    }
}

//THIS FUNCTION IS NOT THREAD SAFE
void rBufFlush(rBuf_t * rBuf){
    // -- Flush the content -- //
    rBuf->pushed = 0;
        rBuf->pulled = 0;
    rBuf->in = rBuf->buffer;
    rBuf->out = rBuf->buffer;
    // ----------------------- //
}


// Space functions
uint16_t rBufUsedSpaceGet(rBuf_t * rBuf){
    uint16_t nBytes = 0;
    uint32_t pushed = rBuf->pushed;
    uint32_t pulled = rBuf->pulled;
    if(pushed < pulled){
        nBytes = (UINT32_MAX - pulled) + pushed;
    }else{
        nBytes = pushed - pulled;
    }
    return nBytes;
}

uint16_t rBufFreeSpaceGet(rBuf_t * rBuf){
    return ((rBuf->bufferSize)-(rBufUsedSpaceGet(rBuf)));
}

uint16_t rBufTotalSpaceGet(rBuf_t * rBuf){
    return (rBuf->bufferSize);
}


uint8_t rBufIsFull(rBuf_t * rBuf){
    if (rBufUsedSpaceGet(rBuf) == rBuf->bufferSize){
        return 1;
    }else{
        return 0;
    }
}


uint8_t rBufIsEmpty(rBuf_t * rBuf){
    if (rBufUsedSpaceGet(rBuf) == 0){
        return 1;
    }else{
        return 0;
    }
}


// Data functions
uint16_t rBufBytePush(rBuf_t * rBuf, uint8_t * srcPtr){
    uint16_t bytePushed = 0;

    if (srcPtr != NULL){
        if ((rBufUsedSpaceGet(rBuf)) < (rBuf->bufferSize)){

            // Push the byte
            *(rBuf->in) = *srcPtr;

            // Handle boundaries
            if (rBuf->in == rBuf->end){
                rBuf->in = rBuf->buffer;
            }else{
                rBuf->in++;
            }

            // Count the byte
            rBuf->pushed++;
            bytePushed = 1;
        }
    }
    return bytePushed;
}

uint8_t rBufBytePull(rBuf_t * rBuf, uint8_t * dstPtr){
    uint8_t bytePulled = 0;

    if (dstPtr != NULL){
        if (rBufUsedSpaceGet(rBuf)){

            // Push the byte
            *dstPtr = *(rBuf->out);

            // Handle boundaries
            if (rBuf->out == rBuf->end){
                rBuf->out = rBuf->buffer;
            }else{
                rBuf->out++;
            }

            // Count the byte
            bytePulled = 1;
            rBuf->pulled++;
        }
    }
    return bytePulled;
}

uint16_t rBufArrayPush(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb){
    uint16_t bytePushed = 0;
    for (; bytePushed < byteNb; bytePushed++){
        if (!rBufBytePush(rBuf, &srcPtr[bytePushed])){
            return bytePushed;
        }
    }
    return bytePushed;
}

uint16_t rBufArrayPull(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb){
    uint16_t bytePulled = 0;

    for (; bytePulled < byteNb; bytePulled++){
        if (!rBufBytePull(rBuf, &dstPtr[bytePulled])){
            return bytePulled;
        }
    }
    return bytePulled;
}

#if defined (interruptGlobalGet) && defined(interruptGlobalDisable) && defined(interruptGlobalSet)


void rBufFlushProtected(rBuf_t * rBuf){
    uint8_t intState = 0;
    intState = interruptGlobalGet();
    interruptGlobalDisable();
    // -- Flush the content -- //
    rBuf->pulled = 0;
        Rbuf->pushed = 0;
    rBuf->in = rBuf->buffer;
    rBuf->out = rBuf->buffer;
    // ----------------------- //
    interruptGlobalSet(intState);
}



// Data functions
uint8_t rBufBytePushProtected(rBuf_t * rBuf, uint8_t * srcPtr){
    uint8_t bytePushed = 0;

    if (srcPtr != NULL){
        uint8_t intState = interruptGlobalGet();
        interruptGlobalDisable();
        if ((rBufUsedSpaceGet(rBuf)) < (rBuf->bufferSize)){

            // Push the byte
            *(rBuf->in) = *srcPtr;

            // Handle boundaries
            if (rBuf->in == rBuf->end){
                rBuf->in = rBuf->buffer;
            }else{
                rBuf->in++;
            }

            // Count the byte
            rBuf->pushed++;
            bytePushed = 1;
        }

        //Restore interrupt state
        interruptGlobalSet(intState);
    }
    return bytePushed;
}




uint8_t rBufBytePullProtected(rBuf_t * rBuf, uint8_t * dstPtr){
    uint8_t bytePulled = 0;

    if (dstPtr != NULL){
        uint8_t intState = interruptGlobalGet();
        interruptGlobalDisable();

        if (rBufUsedSpaceGet(rBuf)){

            // Push the byte
            *dstPtr = *(rBuf->out);

            // Handle boundaries
            if (rBuf->out == rBuf->end){
                rBuf->out = rBuf->buffer;
            }else{
                rBuf->out++;
            }

            // Count the byte
            bytePulled = 1;
            rBuf->pulled++;
        }
        //Restore interrupt state
        interruptGlobalSet(intState);
    }
    return bytePulled;
}



uint8_t rBufArrayPushProtected(rBuf_t * rBuf, uint8_t * srcPtr, uint8_t byteNb){
    uint8_t intState = 0;
    uint8_t returnVal;
    intState = interruptGlobalGet();
    interruptGlobalDisable();

    if (byteNb > (rBuf->bufferSize)-(rBufUsedSpaceGet(rBuf))){
        returnVal = 0;
    }else{
        returnVal = rBufArrayPush(rBuf, srcPtr, byteNb);
    }
    interruptGlobalSet(intState);
    return returnVal;
}


uint8_t rBufArrayPullProtected(rBuf_t * rBuf, uint8_t * dstPtr, uint8_t byteNb){
    uint8_t intState = 0;
    uint8_t returnVal;
    intState = interruptGlobalGet();
    interruptGlobalDisable();

    if (byteNb > (rBufUsedSpaceGet(rBuf))){
        returnVal = 0;
    }else{
        returnVal = rBufArrayPull(rBuf, dstPtr, byteNb);
    }
    interruptGlobalSet(intState);
    return returnVal;
}
#endif
