/*
    @name       REECAN.h
    @version    2.0.0
    @note       CAN Peripheral lib
    @license    SEE $REElib_root/LICENSE.md
*/


#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <em_device.h>      /* For registers definitions */
#ifdef CAN_PRESENT


/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/

#include <REECAN.h>
#include <REEcore.h>		/* core & clock related utilities */
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */
#include <string.h>			/* Debug*/

/* hardware */
#include <em_chip.h>		/* Chip's register definitions */
#include <em_cmu.h>
#include <em_gpio.h>


/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */

#ifndef REECAN_ENABLE_SWO_DEBUG
	#define REECAN_ENABLE_SWO_DEBUG  (false)		/*Set to false when dev completed*/
#endif

#ifndef REECAN_RXMSG_QTY
    #define REECAN_RXMSG_QTY    (30)             /*Configure 24 receive pipe*/
#endif

#define REECAN_TXMSG_NUMBER (31U)                /*Using the last message for transmission*/

#if (REECAN_RXMSG_QTY >= REECAN_TXMSG_NUMBER)
    #error Invalid REECAN_RXMSG_QTY
#endif

#if defined(REECAN_ENABLE_SWO_DEBUG) && REECAN_ENABLE_SWO_DEBUG
    #define REECAN_DBG(s)   (REEBUGSWO_String(s))
#else
    #define REECAN_DBG(s)   ((void) s)
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/

/* CMSIS RTOS2 Stuff */
#if defined(REELIB_USE_CMSISRTOS2)
	osRtxMutex_t __REECAN_mutexMem[CAN_COUNT];

	const osMutexAttr_t __REECANMutexConf[CAN_COUNT] = {
		{
			.name 		= "REECAN0_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REECAN_mutexMem[0],
			.cb_size	= sizeof(osRtxMutex_t),
		},

		#if CAN_COUNT > 1
		{
			.name 		= "REECAN1_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REECAN_mutexMem[1],
			.cb_size	= sizeof(osRtxMutex_t),
		}
		#endif
	};

#endif


/* CAN TIMING CONFIGURATION
 * CAN supports bit rates in the range of lower than 1 kBit/s up to 1000 kBit/s.
 * Each member of the CAN network has its own clock generator, usually a quartz oscillator.
 * The timing parameter of the bit time (i.e. the reciprocal of the bit rate) can be configured individually for each CAN node,
 * creating a common bit rate even though the CAN nodes’ oscillator periods osc may be different.
 *
 * According to the CAN specification, the bit time is divided into four segments.
 * The Synchronization Segment, the Propagation Time Segment, the Phase Buffer Segment 1, and the Phase Buffer Segment 2.
 * Each segment consists of a specific, programmable number of time quanta
 * The length of the time quantum (t q ) , which is the basic time unit of the bit time, is defined by the CAN controller's
 * system clock f sys and the Baud Rate Prescaler (BRP): t_q = BRP / f_sys
 *
 * The Synchronization Segment Sync_Seg is that part of the bit time where edges of the CAN bus level are expected to occur;
 *  the distance between an edge that occurs outside of Sync_Seg and the Sync_Seg is called the phase error of that edge.
 *  The Propagation Time Segment (Prop_Seg) is intended to compensate for the physical delay times within the CAN network.
 *  The Phase Buffer Segments (Phase_Seg1) and (Phase_Seg2) surround the Sample Point.
 *  The (Re-)Synchroniztion Jump Width (SJW) defines how far a resynchronization may move the Sample Point inside the
 *  limits defined by the Phase Buffer Segments to compensate for edge phase errors.
 * */
#define propagationTimeSegment      (1) /* [0..8] Offset/qty of tq following a egde raising before seg1 start  */
#define phaseBufferSegment1         (5) /* [0..8] max compensation width of the seg1 (left part of the sample point of the bit  */
#define phaseBufferSegment2         (5) /* [0..8] max compensation width of the seg2 (right part of the sample point of the bit */
#define synchronisationJumpWidth    (1) /* Sync jumb step */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ ---------------------------------*/

typedef struct {

    CAN_TypeDef * const reg;					/* hw register address 0 (constant pointer to variable data/register)*/

    #ifdef REELIB_USE_CMSISRTOS2
        osThreadId_t			thread;				/* Last/current thread that used this hw */
        REECAN_t *				handle;				/* Actual handle given to owner */
        osMutexId_t	     		mutex;				/* hw protection mutex */
	#endif

	const CMU_Clock_TypeDef	clk;				/* Peripheral clock control*/
	const IRQn_Type			irq;				/* Peripheral IRQ identifier*/
    uint8_t                 id;                 /* CAN offset identifier (0:CAN0, 1:CAN1...)*/
    bool                    tx_on;
    bool                    tx_busy;
    uint32_t                busy_timestamp;
}__REECANhw_t;

typedef union{
	uint32_t rawVal;
	struct{
		uint32_t 		:29;	/* Message Identifier */
		uint8_t dir		:1;		/* Message Direction */
		uint8_t xtd		:1;		/* Extended Identifier */
		uint8_t valid	:1;		/* Message Valid */
	};
	struct{
		uint32_t id 	:29;	/* Message Identifier */
		uint8_t 		:3;
	}extended;
	struct{
		uint32_t 		:18;
		uint16_t id 	:11;	/* Message Identifier */
		uint8_t 		:3;
	}standard;
}REE_MIR_ARB_t;


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ ---------------------------------*/


__REECANhw_t __REECAN_hardware[CAN_COUNT] = {
    #if CAN_COUNT >0
        {	.id = 		0,
            .reg=		CAN0,
        #ifdef REELIB_USE_CMSISRTOS2
            .thread=	NULL,
            .handle=	NULL,
            .mutex=		NULL,
        #endif

            .clk =		cmuClock_CAN0,
            .irq = 		CAN0_IRQn,
            .tx_on = false,
        },
    #endif

    #if CAN_COUNT >1
        {	.id = 1,
            .reg=		CAN1,
        #ifdef REELIB_USE_CMSISRTOS2
            .thread=	NULL,
            .handle=	NULL,
            .mutex=		NULL,
        #endif

            .clk =		cmuClock_CAN1,
            .irq = 		CAN1_IRQn,
            .tx_on = false,
        },
    #endif
};


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/

/* Valid for basic mode */

#define REECAN_MIR_INTERFACE_IRQ (0) /* MIR interface used to read from register */
#define REECAN_MIR_INTERFACE_THREAD (1) /* MIR interface used to read from register */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/

static __REECANhw_t * __REECAN_findHardware(REECAN_t * can) {
    uint8_t id;
    __REECANhw_t * hw;
    __REECANhw_t * retval = NULL;

    for (id=0; id<CAN_COUNT; id++) {
        hw = &__REECAN_hardware[id];
        if(can->candev == (hw->reg)){	// do CAN base address match ? same peripheral
            retval = hw;
        }
    }
	return retval;
}

/* Return true if calling thread is owner of the peripheral at the moment of calling */
static bool __REECAN_isOwner(REECAN_t * can) {
    __REECANhw_t * hw = NULL;

    hw 	= __REECAN_findHardware(can);	// Search hardware

    return ((hw != NULL) && (hw->handle == can));
}


/* -------------------------------- +
|									|
|	API								|
|									|
+ ---------------------------------*/

/* Take exclusive control of a can peripheral until released */

REECAN_err_t REECAN_open(REECAN_t * can){
    __REECANhw_t * hw 	= NULL;

    /* CAN device pointer must be initiated*/
    if((can == NULL) || (can->candev == NULL)) {
        return REECAN_null;
    }

    /* Search for the control structure*/
    hw = __REECAN_findHardware(can);
    if(hw == NULL){
        return REECAN_notFound;	//
    }

    if(hw->mutex == NULL){
        hw->mutex = osMutexNew(&__REECANMutexConf[hw->id]);
    }

    if (osOK != osMutexAcquire(hw->mutex, can->maxWaitTime_ms)) {
        return REECAN_timeout;
    }

    /* save user information */
    hw->handle = can;
    hw->thread = osThreadGetId();
    hw->tx_busy = false;
    hw->busy_timestamp = 0;


    /* Clock */
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(hw->clk, true);

    /* Reset the CAN device */
    CAN_Enable((CAN_TypeDef*)hw->reg, false);
    CAN_Reset((CAN_TypeDef*)hw->reg);

    REECAN_updateConfig(can);

    can->candev->IF0IEN = 0;    /*Disable Message Object Interrupt Enable Register*/
    can->candev->IF1IEN = 1;    /*Enable Status Interrupt Enable Register*/

    /* Clear MIR*/
    CAN_ResetMessages((CAN_TypeDef*)hw->reg,  REECAN_MIR_INTERFACE_THREAD);	//Reset all MIR

    NVIC_ClearPendingIRQ(hw->irq);
    NVIC_EnableIRQ(hw->irq);

    /* IO and route*/
    GPIO_Unlock();
    CAN_SetRoute((CAN_TypeDef*)hw->reg, true, can->locationRx, can->locationTx);
    GPIO_PinModeSet(REEIO_PORT(can->tx), REEIO_PIN(can->tx), gpioModePushPull,1);
    GPIO_PinModeSet(REEIO_PORT(can->rx), REEIO_PIN(can->rx), gpioModeInput,1);

    CAN_Enable((CAN_TypeDef*)hw->reg, true);

    const bool isValid = true;
    const bool remoteTransfer = false;
    const bool endOfBuffer = false;      /*unsure -> Jonathan*/
    const bool wait = true;
    const bool use_mask = true;

    CAN_MessageObject_TypeDef rxMsgConfig = {
        .msgNum = 1,

        .extended = false,
        .extendedMask=false,            // Discard extended masking (allow-all)

        .id = 0,						// ID is ignored (for match-all)
        .mask = 0,                      // Mask is Match-all (0)

        .directionMask = 0,             // 0:rx, 1:tx

        .dlc = 8,                       // Data len = 8
	};

	for(uint8_t msgnum=0; msgnum < REECAN_RXMSG_QTY; msgnum++){
        rxMsgConfig.msgNum = msgnum+1;
        if(can->rx_filters[msgnum] == 0){
            /* ALLOW all*/
            rxMsgConfig.extended = false;
            rxMsgConfig.id = 0;
            rxMsgConfig.mask = 0;
            rxMsgConfig.extendedMask = false;
        }else{
            /* Filter on PGN*/
            rxMsgConfig.extended = true;
            rxMsgConfig.extendedMask = true;
            rxMsgConfig.id      = can->rx_filters[msgnum],
            rxMsgConfig.mask    = 0x00FFFF00;
        }
        CAN_ConfigureMessageObject(	(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_THREAD, rxMsgConfig.msgNum, isValid, 0, remoteTransfer, endOfBuffer, wait);
        CAN_SetIdAndFilter(			(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_THREAD, use_mask,			&rxMsgConfig, wait);
	}


   return REECAN_success;
}

REECAN_err_t REECAN_close(REECAN_t * can) {
	__REECANhw_t * hw 	= NULL;

	if (can == NULL) {
		return REECAN_null;
	}

    hw = __REECAN_findHardware(can);

    if(!__REECAN_isOwner(can)){
		return REECAN_denied;
	}

    /* Clean CAN peripheral */
    NVIC_DisableIRQ(hw->irq);                                               /* Disable Interrupt */
    NVIC_ClearPendingIRQ(hw->irq);

    CAN_ResetMessages((CAN_TypeDef*)hw->reg,  REECAN_MIR_INTERFACE_THREAD); /* Reset all MIR */
    CAN_Reset((CAN_TypeDef*)hw->reg);                                       /* Reset Peripheral */
    CMU_ClockEnable(hw->clk, false);                                        /* Turn off peripheral clock */

	/* Clean os service */
	hw->handle = NULL;
	hw->thread = NULL;
	hw->tx_on = false;
	hw->tx_busy = false;

	/* release control */
    if (osOK != osMutexRelease(hw->mutex)) {
        return REECAN_fail;
    }

	return REECAN_success;
}

REECAN_err_t REECAN_tx(REECAN_t * can, CAN_MessageObject_TypeDef *message, bool remoteTransfer){

	REECAN_err_t ret = REECAN_fail;

    __REECANhw_t * hw = __REECAN_findHardware(can);

	if((can==0) || (message==0)){
		ret = REECAN_null;

	}else if (!__REECAN_isOwner(can) || (hw==NULL) || (can->txEnable == false)){
		ret = REECAN_denied;

	}else if(REECAN_isTxReady(can) == false){
        ret = REECAN_busy;

	}else{
        REECAN_DBG("TX\t");

        if(hw->handle->txEnable != hw->tx_on){
            hw->tx_on = hw->handle->txEnable;
            if(hw->tx_on){
                CAN_SetMode(can->candev, canModeNormal);
             }
        }

        message->msgNum = (REECAN_TXMSG_NUMBER+1);


        /* Configure valid, TX/RX, remoteTransfer for a specific Message Object.*/
        CAN_ConfigureMessageObject(	can->candev,					/* A pointer to the CAN peripheral register block. */
                                    REECAN_MIR_INTERFACE_THREAD,    /* Indicate which Message Interface Register to use. */
                                    (message->msgNum),				/* A message number of this Message Object, [1 - 32].*/
                                    true,							/* True if the Message Object is valid, false otherwise. */
                                    true,							/* True if the Message Object is used for transmission, false if used for reception.*/
                                    remoteTransfer,					/* True if the Message Object is used for remote transmission, false otherwise. */
                                    true,							/* True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */
                                    false);							/* If true, wait for the end of the transfer between the MIRx registers and the RAM to exit. If false, exit immediately, the transfer can still be in progress. */
        CAN_SendMessage(	can->candev,				/* A pointer to the CAN peripheral register block.*/
                            REECAN_MIR_INTERFACE_THREAD,/* Indicate which Message Interface Register to use.*/
                            message,					/* Message to send */
                            false);						/* If true, wait for the end of the transfer between the MIRx registers and the RAM to exit. If false, exit immediately, the transfer can still be in progress. */
        hw->tx_busy = true;
        hw->busy_timestamp = systickGet();

        ret = REECAN_success;
	}

	return ret;	//TODO: add unblocking mechanism
}


REECAN_err_t REECAN_rx(REECAN_t * can, REECAN_messageObject_t * message){

    REECAN_err_t ret = REECAN_fail;

    /*Interrupt only static variable*/
    uint8_t i;
    CAN_MessageObject_TypeDef irq_message;
    REE_MIR_ARB_t             _mirArb;

    uint8_t msgNumFifoId = 0;

    if((can==0) || (message==0)){
        ret = REECAN_null;

    }else if (!__REECAN_isOwner(can)){
        ret = REECAN_denied;

    }else{
        ret = REECAN_empty;
        if((can->candev->MESSAGEDATA & (UINT32_MAX>>1)) > 0){

        for(i=0; (i<REECAN_RXMSG_QTY) && (ret !=REECAN_success); i++){
            /** Iterate over all received msgNum*/

                if((can->candev->MESSAGEDATA & (1<<msgNumFifoId)) == 0){
                    /* no valid message here, check for next*/
                    msgNumFifoId = (msgNumFifoId+1) % REECAN_RXMSG_QTY;

                }else{

                    irq_message.msgNum = msgNumFifoId+1; /* Point to the message number with valid content [1..32] */
                    msgNumFifoId = (msgNumFifoId+1) % REECAN_RXMSG_QTY;

                    CAN_ReadMessage(can->candev, REECAN_MIR_INTERFACE_THREAD, &irq_message);
                    _mirArb = (REE_MIR_ARB_t) can->candev->MIR[REECAN_MIR_INTERFACE_THREAD].ARB;

                    message->extended = _mirArb.xtd;
                    if(message->extended){
                        message->id = _mirArb.extended.id;
                    }else{
                        message->id = _mirArb.standard.id;
                    }
                    message->mask = irq_message.mask;
                    message->canIntf = can->canIntf;
                    message->dlc = irq_message.dlc;
                    message->msgNum = irq_message.msgNum;
                    message->direction_tx = false;
                    memcpy(message->data, irq_message.data, 8);
                    ret =REECAN_success;
                }
            }
        }

        if(ret == REECAN_empty){
            /* no msg, reset fifo head*/
            msgNumFifoId = 0;
        }
    }
    return ret; //TODO: add unblocking mechanism
}

bool REECAN_isTxReady(REECAN_t *can){
    bool ready = false;
    __REECANhw_t * hw = __REECAN_findHardware(can);
    if(hw){
        if(hw->tx_busy == false){
            ready = true;

        }else if(hw->busy_timestamp == 0){
            hw->busy_timestamp = systickGet();

        }else if(systickDelta(hw->busy_timestamp) > 5){
            /* Last resort for when peripheral stop responding (won't transmit) */
            REECAN_close(can);
            REECAN_open(can);
            ready = true;
        }
    }
    return ready;
}


REECAN_err_t REECAN_updateConfig(REECAN_t *can){

    if(can == NULL){
      return REECAN_null;

    }else if(!__REECAN_isOwner(can)){
      return REECAN_denied;
    }

    __REECANhw_t * hw = __REECAN_findHardware(can);
    if(hw == NULL){
        return REECAN_notFound;
    }

    can->canIntf = hw->id;

    /* Clean pre-existing condition to avoid messing with the bus during reconfiguration
     * */
    can->candev->CTRL |= CAN_CTRL_DAR;          /* Disable retransmission */
    CAN_AbortSendMessage(can->candev, REECAN_MIR_INTERFACE_IRQ, 1+REECAN_TXMSG_NUMBER, false);
    hw->tx_busy = false;
    hw->busy_timestamp = 0;

    CAN_SetMode(can->candev, canModeSilent);    /* Set mode to silent, set to normal on RXOK or tx_call (if flag is set) */
    hw->tx_on = false;

    if(can->bitrate > 0){
        // https://docs.silabs.com/mcu/5.6/efm32gg11/group-CAN
        CAN_SetBitTiming(can->candev, can->bitrate, propagationTimeSegment,
                phaseBufferSegment1, phaseBufferSegment2, synchronisationJumpWidth);
    }

    if(can->txAutoRetry){
        can->candev->CTRL &= ~CAN_CTRL_DAR;
    }

    return REECAN_success;
}

uint8_t REECAN_getRxErrorCount(REECAN_t * can){
    uint8_t cnt = UINT8_MAX;
	if(can != NULL){
		cnt = ((can->candev->ERRCNT) & _CAN_ERRCNT_REC_MASK) >> _CAN_ERRCNT_REC_SHIFT;
	}
	return cnt;
}

uint8_t REECAN_getTxErrorCount(REECAN_t * can){
    uint8_t cnt = UINT8_MAX;
    if(can != NULL){
        cnt = ((can->candev->ERRCNT) & _CAN_ERRCNT_TEC_MASK) >> _CAN_ERRCNT_TEC_SHIFT;
	}
    return cnt;
}

void REECAN_printErrorCode(REECAN_err_t err){

    switch(err){
        case REECAN_success:   { REEBUGSWO_String("REECAN_success");   break; }
        case REECAN_fail:      { REEBUGSWO_String("REECAN_fail");      break; }
        case REECAN_timeout:   { REEBUGSWO_String("REECAN_timeout");   break; }
        case REECAN_collision: { REEBUGSWO_String("REECAN_collision"); break; }
        case REECAN_nack:      { REEBUGSWO_String("REECAN_nack");      break; }
        case REECAN_full:      { REEBUGSWO_String("REECAN_full");      break; }
        case REECAN_notFound:  { REEBUGSWO_String("REECAN_notFound");  break; }
        case REECAN_denied:    { REEBUGSWO_String("REECAN_denied");    break; }
        case REECAN_empty:     { REEBUGSWO_String("REECAN_empty");     break; }
        case REECAN_busy:      { REEBUGSWO_String("REECAN_busy");      break; }
        case REECAN_param:     { REEBUGSWO_String("REECAN_param");     break; }
        case REECAN_null:      { REEBUGSWO_String("REECAN_null");      break; }
        default:               { REEBUGSWO_String("REECAN_unknown");   break; }
    }
    /* REECAN error code */


}

/* -------------------------------- +
|									|
|	Interrupt service routine		|
|									|
+ ---------------------------------*/
__STATIC_INLINE void CANx_IRQ(CAN_TypeDef * CAN, __REECANhw_t * hw){
    static uint32_t status;
	static uint8_t temp;
    static bool abort_tx;
    static bool error_tx;
    static bool error_rx;

    abort_tx = false;
    error_tx = false;
    error_rx = false;

    REECAN_DBG("<IRQ>\t");

    if((hw!=NULL) && (hw->handle!=NULL)){

        status = CAN->STATUS; // It get cleared when read, but we clear it just in case
        CAN_StatusClear(CAN, status);

        if(status & CAN_STATUS_RXOK){
            REECAN_DBG("RXOK\t");
            if(hw->handle->txEnable != hw->tx_on){
                hw->tx_on = hw->handle->txEnable;
                if(hw->tx_on){
                    CAN_SetMode(CAN, canModeNormal);
                }else{
                    CAN_SetMode(CAN, canModeSilent);
                }
            }

            if((hw->handle->onReceiveMsg && ((CAN->MESSAGEDATA & (UINT32_MAX>>1)) > 0))){
                hw->handle->onReceiveMsg();
            }

        }else if(status & CAN_STATUS_TXOK){
            REECAN_DBG("TXOK\t");
            abort_tx = true;
            if(hw->handle->onTransmitMsg){
                hw->handle->onTransmitMsg();
            }

        }else{
            if(status & CAN_STATUS_BOFF){
                REECAN_DBG("BOFF\t");
                hw->tx_on = false;
                CAN_SetMode(CAN, canModeSilent);
                abort_tx =true;
            }
            if(status & CAN_STATUS_EWARN){ REECAN_DBG("EWARN\t"); }
            if(status & CAN_STATUS_EPASS){ REECAN_DBG("EPASS\t"); }

            temp = (status & _CAN_STATUS_LEC_MASK) >> _CAN_STATUS_LEC_SHIFT;

            /* On any error, cancel transmit*/
            abort_tx |= (temp != CAN_STATUS_LEC_NONE);

            switch(temp){
                default:
                 /* Unused / reserved value */
                case CAN_STATUS_LEC_UNUSED:{ REECAN_DBG("UNUSED\t");error_rx = true;    break; }

                /* No error occurred during last CAN bus event.*/
                case CAN_STATUS_LEC_NONE:{              break;  }

                /* More than 5 equal bits in a sequence have occurred in a part of a received message where this is not allowed. */
                case CAN_STATUS_LEC_STUFF:{ REECAN_DBG("STUFF\t");  error_rx = true;    break; }

                /* A fixed format part of a received frame has the wrong format. */
                case CAN_STATUS_LEC_FORM:{  REECAN_DBG("FORM\t");   error_rx = true;    break; }

                /* The message this CAN Core transmitted was not acknowledged by another node.*/
                case CAN_STATUS_LEC_ACK:{   REECAN_DBG("NACK\t");   error_tx = true;    break;  }

                /* During the transmission of a message, the device wanted to send a recessive level
                 * (bit of logical value 1), but the monitored bus value was dominant.*/
                case CAN_STATUS_LEC_BIT1:{  REECAN_DBG("BIT1\t");   error_tx = true;    break;  }

                /* The device wanted to send a dominant level (logical value 0), but the monitored Bus value was recessive.
                 * During Bus Off recovery, this status is set each time a sequence of 11 recessive bits has been monitored.
                 * This enables the CPU to monitor the proceeding of the Bus Off recovery sequence
                 * (indicating the bus is not stuck at dominant or continuously disturbed)*/
                case CAN_STATUS_LEC_BIT0:{  REECAN_DBG("BIT0\t");   error_tx = true;    break;  }

                /* The CRC check sum was incorrect in the message received; the CRC received for an incoming message
                 * does not match with the calculated CRC for the received data.*/
                case CAN_STATUS_LEC_CRC:{   REECAN_DBG("CRC\t");    error_rx = true;    break;  }
            }

            if(error_rx && (hw->handle->onReceiveErr)){ hw->handle->onReceiveErr(temp); }
            if(error_tx && hw->handle->onTransmitErr){  hw->handle->onTransmitErr(temp);}
        }


        if(abort_tx && hw->tx_busy){
            CAN_AbortSendMessage(CAN, REECAN_MIR_INTERFACE_IRQ, 1+REECAN_TXMSG_NUMBER, false);
            hw->reg->CTRL |= CAN_CTRL_DAR;
            hw->tx_busy = false;
            hw->busy_timestamp = 0;
        }
    }else if(CAN){
        /*
         * Peripheral was closed, but IRQ was called or pointer invalid.
         * (Unexpected ISR call -> disabling everything....)
         * */

        /* IF0IF */
        CAN_MessageIntDisable(CAN, UINT32_MAX);
        CAN_MessageIntClear(CAN,   UINT32_MAX);

        /* IF1IF */
        CAN_StatusIntDisable(CAN, UINT32_MAX);
        CAN_StatusIntClear(CAN,   UINT32_MAX);
    }
    REECAN_DBG("<IRQ>\n");
}

#if CAN_COUNT > 0
    void CAN0_IRQHandler(void){
        CANx_IRQ(CAN0, &__REECAN_hardware[0]);
    }
#endif


#if CAN_COUNT > 1
    void CAN1_IRQHandler(void){
        CANx_IRQ(CAN1, &__REECAN_hardware[1]);
    }
#endif

#endif  /*CAN_PRESENT*/

#pragma GCC diagnostic warning "-Wunused-function"			/* Re-enable those warning for future files */
#pragma GCC diagnostic warning "-Wunused-parameter"			/* Re-enable those warning for future files */
