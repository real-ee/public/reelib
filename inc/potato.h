/*
 * potato.h
 *
 *  Created on: 26 Mar 2019
 *      Author: lvilleneuve
 */

#ifndef INC_POTATO_H_
#define INC_POTATO_H_

/* ============================ */
/* INCLUDE						*/
/* ============================ */
#include <stdint.h>
#include <stdbool.h>
#include <hardware.h>
#include <conf.h>

/* ============================ */
/* PROTO						*/
/* ============================ */
bool areYouHappy(uint16_t patate);

#endif /* INC_POTATO_H_ */
