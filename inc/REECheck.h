/*
	@name		REECheck.h
	@author		Jonathan Lecuyer
	@version	1.0.0
	@brief		Use the General Purpose CRC hardware to compure a CRC32
	@note
	@license	All right reserved RealEE inc. (2019)
*/

#ifndef __REECHECK_H_
#define __REECHECK_H_

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public Global					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */

/*	@brief	Input word/u8 in CRC peripheral
	@note	If running on RTOS, a mutex is used to protect access to the peripheral
	@param	uint8_t *data	Pointer to the u8 array to compute the CRC from
	@param	uint16_t size	Size of the given array
    @return bool            True if successful
*/
bool REECheck_crc_hwU8(uint8_t *data, uint16_t size);

/*	@brief	Input word/u32 in CRC peripheral
	@note	If running on RTOS, a mutex is used to protect access to the peripheral
	@param	uint8_t *data	Pointer to the u8 array to compute the CRC from
	@param	uint16_t size	Size of the given array
	@return	bool            True if successful
*/
bool REECheck_crc_hwU32(uint32_t *data, uint16_t size);

/*  @brief  Return a CRC32 for the given array of word/u32
    @note   If running on RTOS, a mutex is used to protect access to the peripheral
    @return uint32_t        Computed value of the CRC for the given array
*/
uint32_t REECheck_crc_getCRC();

#ifdef __cplusplus
}
#endif
#endif /* __REECHECK_H_ */
