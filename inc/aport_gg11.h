/*
 * aport_gg1.h
 *
 *  Created on: Nov. 4, 2020
 *      Author: jlecuyer
 */

#ifndef DEP_REELIB_INC_APORT_GG11_H_
#define DEP_REELIB_INC_APORT_GG11_H_

#include <REEutil.h>        /* For GPIO tricks */
#include <em_device.h>      /* For registers definitions */
#include <em_cmu.h>         /* For registers definitions */
#include <em_gpio.h>        /* For registers definitions */
#include <em_adc.h>         /* For analog mapping       */

typedef enum{
    aportGG11_adc_0,
    aportGG11_adc_1,
    aportGG11_adc_both,
    aportGG11_adc_none,
}aportGG1_adc_t;

typedef struct{
    aportGG1_adc_t peripheral;
    ADC_PosSel_TypeDef chan_pos;
}aport_gg1_t;


bool aportGG11_getAdcChannel(GPIO_Port_TypeDef port, uint8_t pin, aport_gg1_t *chan);

#endif /* DEP_REELIB_INC_APORT_GG11_H_ */
