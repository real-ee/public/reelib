/*
 * fletcher.h
 *
 *  Created on: May 4, 2020
 *      Author: jlecuyer
 */

#ifndef DEP_COMMON_INC_FLETCHER_H_
#define DEP_COMMON_INC_FLETCHER_H_

#include <stdint.h>

typedef struct __attribute__((__packed__)){
	uint16_t check;
	struct{
		uint8_t cka;
		uint8_t ckb;
	};
}fletcher_check_t;


uint16_t fletcher_checksum(uint8_t *ptr, uint16_t len);


#endif /* DEP_COMMON_INC_FLETCHER_H_ */
