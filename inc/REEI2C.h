/*
	@name		REEI2C.h
	@author		Laurence DV
	@version	1.0.0
	@note		I2C Peripheral lib, largely inspired by i2cspm driver from SiLabs
				Only single-master mode currently implemented
				Not yet thread-safe
	@license	SEE $REElib_root/README.md
*/
#ifndef REEI2C_H_
#define	REEI2C_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>						/* Because stdint mofo */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* Universal types */
#include <REEutil.h>					/* Utilities */
#include <cmsis_os2.h>					/* RTOS API */
#include <em_chip.h>					/* Chip's register definitions */

#include <em_cmu.h>
#include <em_gpio.h>
#include <em_i2c.h>


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define those before you include	|
|	this file, if you want to		|
|	customize them					|
+ -------------------------------- */
#ifndef REEI2C_TIMEOUTPERBYTE_MS
	#define		REEI2C_TIMEOUTPERBYTE_MS		(1)	/* Maximum time allowed to send/receive a byte, after which a time error is returned */
#endif
#ifndef REEI2C_RTOS_TIMEOUT
	#define		REEI2C_RTOS_TIMEOUT_MS			(10)	/* Maximum time allowed to access RTOS ressources (in millisec) */
#endif
#ifndef REEI2C_AUTOSTART
	#define		REEI2C_AUTOSTART				(1)		/* Enable the automatic start of the lib at the expense of a small check at each take/release */
#endif
#ifndef REEI2C_HOOK_MAXCNT
	#define		REEI2C_HOOK_MAXCNT				(1)		/* Maximum number of callbacks attached to a single hook (excluding data hooks) */
#endif
#ifndef REEI2C_DATAHOOK_MAXCNT
	#define		REEI2C_DATAHOOK_MAXCNT			(8)		/* Maximum number of special data hooks (ie: masked data, address) */
#endif

/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
/* I2C handle (the owner of the peripheral should have this) */
typedef struct {
	REEtype_t					dataType;		/* Data access type used by tx,rx,trx functions */
	struct {
		bool					blocking;		/* 1: functions are blocking | 0: will return immediately */
		bool					master;			/* 1: to be master | 0: to be slave */
		uint16_t				address;		/* I2C address (own address when slave, destination address when master) */
		I2C_TypeDef *			dev;			/* HW Peripheral register address */
		CMU_Clock_TypeDef		clk;			/* Peripheral clock */
		uint8_t					scl;			/* SCL pin encoded with REEIO() */
		uint8_t					sda;			/* SDA pin encoded with REEIO() */
		uint8_t					locationScl;	/* Port location of SCL signal */
		uint8_t					locationSda;	/* Port location of SDA signal */
		uint32_t				refClk;			/* I2C reference clock */
		uint32_t				bitrate;		/* I2C operating bitrate (in bps) */
		I2C_ClockHLR_TypeDef	clhr;			/* Clock low/high ratio control */

	}ctl;
}REEI2C_t;

/* Sequence type (for convience) */
typedef I2C_TransferSeq_TypeDef REEI2C_Sequence_t;

/* REEI2C error code */
typedef enum {
	REEI2C_success =		0,
	REEI2C_fail =			-1,
	REEI2C_timeout =		-2,
	REEI2C_collision =		-3,
	REEI2C_nack =			-4,
	REEI2C_param =			-127,
	REEI2C_null =			-128,
}REEI2C_err_t;

/* REEI2C hook list */
typedef enum {
	REEI2C_hook_busIdle =	1,					/* Called once as soon as the bus is idle */
	REEI2C_hook_slaveRxReq =2,					/* As a slave, called once an I2C Read request as been received */
	REEI2C_hook_slaveTxReq =3,					/* As a slave, called once an I2C Write request as been received */
}REEI2C_hook_t;

/* REEI2C IOctl structure */
typedef enum {
	REEI2C_act_resetBus =		1,				/* Generate a Reset sequence on the bus [no args] */
	REEI2C_act_detectSlave =	2,				/* Detect the presence of a specific I2C slave on the bus [value = slave address] */
}REEI2C_ioctl_actionList_t;

typedef struct {
	REEI2C_ioctl_actionList_t	action;			/* Action to do */
	union {
		uint32_t				value;			/* Action's data */
		void *					ptr;			/* Action's pointer*/
	};
}REEI2C_ioctl_t;


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	REEUSI API						|
|									|
+ -------------------------------- */
/*
	\brief	Try to take/release exclusive control of an I2C hw peripheral
	\note	This function will block if $i2c.ctl.blocking == TRUE, otherwise it
			will return immediately
	\usage	Useful to ensure multiple calls of tx/rx will be sequencial on the hw bus

	\param	REEI2C_t * i2c						I2C virtual peripheral handle
	\return	REEI2C_err_t status					REEI2C_success if everything went well
												REEI2C_null if i2c is NULL
												REEI2C_param is invalid parameters
												REEI2C_fail if couldn't take/release the hw
*/
REEI2C_err_t REEI2C_take(REEI2C_t * i2c);
REEI2C_err_t REEI2C_release(REEI2C_t * i2c);

/*
	\brief	Send/Receive a transaction on the I2C bus
	\note	For now those function are always blocking (upto REEI2C_RTOS_TIMEOUT_MS)
			TODO: Only Master mode implemented
			TRX will do WriteToSlave then ReadFromSlave will the same len (interface requirements)
	\usage	For master mode, specify the slave address in $i2c.ctl.address

	\param	REEI2C_t * i2c						I2C virtual peripheral handle
	\param	void * src/dst						Data source/destination pointer
	\param	uint32_t len						Data lenght
	\return	uint32_t actualLen					Actual data lenght transfered
*/
uint32_t REEI2C_tx(REEI2C_t * i2c, void * src, uint32_t len);
uint32_t REEI2C_rx(REEI2C_t * i2c, void * dst, uint32_t len);
uint32_t REEI2C_trx(REEI2C_t * i2c, void * src, void * dst, uint32_t len);

/*
	\brief	Execute a special IO action on the I2C bus
	\note	For now those function are always blocking (upto REEI2C_RTOS_TIMEOUT_MS)
			TODO: Only Master mode implemented
	\usage

	\param	REEI2C_t * i2c						I2C virtual peripheral handle
	\param	REEI2C_ioctl_t * args				IO action arguments
	\return	REEI2C_err_t status					REEI2C_success if everything went well
												REEI2C_null if $i2c or $args is NULL
												REEI2C_param is invalid parameters
												REEI2C_fail if generic error
*/
REEI2C_err_t REEI2C_ioctl(REEI2C_t * i2c, REEI2C_ioctl_t * args);

/*
	\brief	Attach/Detach a function to a specific hook on the I2C bus
	\note	TODO: No hook implemented yet
	\usage

	\param	REEI2C_t * i2c						I2C virtual peripheral handle
	\param	REEI2C_hook_t * hook				Hook type to attach/detach
	\param	void(*REEcb_t)(void *)				Callback function to call when the hook is triggered
	\return	REEI2C_err_t status					REEI2C_success if everything went well
												REEI2C_null if $i2c or $args is NULL
												REEI2C_param is invalid parameters
												REEI2C_fail if generic error
*/
REEI2C_err_t REEI2C_hook(REEI2C_t * i2c, REEI2C_hook_t * hook, void(*REEcb_t)(void *));
REEI2C_err_t REEI2C_unhook(REEI2C_t * i2c, REEI2C_hook_t * hook, void(*REEcb_t)(void *));


/* -------------------------------- +
|									|
|	CUSTOM API						|
|									|
+ -------------------------------- */




#ifdef __cplusplus
}
#endif
#endif	/* REEI2C_H_ */

