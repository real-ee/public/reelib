/*
	\name		render.h
	\author		Laurence DV
	\date		2018-09-13
	\version	0.1.0
	\brief		Rendering functions for raw gbr pixel animations
	\note		IMPORTANT:	everything is built to be asynchronous-capable, as such
				all timing are not time period, but rather function's call number.
				ie: VUmeter.decay = 3   --> you need to call 3 time render_VUmeter
				This permit simple real time implementation in sync system like REEF
	\license	All right reserved RealEE inc. (2018)
*/

#ifndef RENDER_H_
#define	RENDER_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <cmsis_os2.h>
#include <REEcolor.h>


/* -------------------------------- +
|									|
|	Config							|
|	MUST BE SET						|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
typedef rgb888_t renderrgb_t;			/* RGB type used for all render functions, this should always be the same as REEFrgb_t */

/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */



/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
typedef enum {
	render_success =	0,
	render_fail =		-1,
	render_null =		-2,
}render_err_t;

/* VUmeter control and params */
typedef struct {
	uint8_t				alpha;			/* Global luminosity control (DEF: 0xFF) */
	renderrgb_t			activeColor;	/* Color of the VUmeter when pixel are "active" */
	renderrgb_t			inactiveColor;	/* Background color of the VUmeter */
	uint8_t				decay;			/* Fadeout time when switch from active to inactive colors (DEF: 0) */
	uint8_t *			control;		/* Control value of the VUmeter, (*control=0 -> OFF, *control=0xFF -> FullON)*/
	renderrgb_t *		dest;			/* VUmeter actual display buffer, will be written to */
	uint16_t 			startID;		/* Start of the VUmeter in $dest */
	uint16_t			stopID;			/* Stop of the VUmeter in $dest */
	uint8_t				_cnt;			/* internal use */
}VUmeter_t;

/* Generic rendering callback */
typedef render_err_t(*renderCb_t)(void *);

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */
#define RENDER_VU_DEFAULT						\
	{											\
		.alpha = 0xFF,							\
		.activeColor = REEpalette[REE_ORANGE],	\
		.inactiveColor = REEpalette[REE_BLACK],	\
		.decay = 2,								\
		.control = NULL,						\
		.dest =	NULL,							\
		.startID = 0,							\
		.stopID = 0,							\
		._cnt =	0,								\
	}


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */





/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/*
	\brief	Framed Human Interface control thread
	\note	will call REEF_init() at boot
	\usage
	{
		// Pepare necessary stuff
		uint8_t myCtl = 0x80;
		renderrgb_t dispBuffer[10] = {};
		VUmeter_t	myVU = RENDER_VU_DEFAULT;
		myVU.dest = dispBuffer;
		myVU.control = &myCtl;
		myVU.stopID = 9;

		while(1) {
			render_VUmeter(&myVU);					//render in buffer
			actual_displayFunction(&dispBuffer);	//display content of buffer
			delay(100);								//wait for next refresh period
			myCtl++;
		}
	}

 	\param	void const *argument		nothing to do with this paramter
	\return	void
*/
render_err_t render_VUmeter(VUmeter_t * handle); 



#ifdef __cplusplus
}
#endif
#endif	/* RENDER_H_ */
