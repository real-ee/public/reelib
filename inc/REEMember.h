/*
 * REEMember.h
 *
 *  Created on: Jun 9, 2019
 *      Author: jlecuyer
 */

#ifndef INC_REEMEMBER_H_
#define INC_REEMEMBER_H_

#include <em_msc.h>
#include <stddef.h>

/* 0x0fe00000 - 0x0fe00400 */
#define REEMEMBER_ADDRESS	((uint32_t*)USERDATA_BASE)
#define REEMEMBER_SIZE		(FLASH_PAGE_SIZE)				/*4KB on EFM32GG11, 2KB on EFM32TG11*/


typedef enum{
	REEMember_ok			= mscReturnOk,			/**  0,	Flash write/erase successful.							*/
	REEMember_eraseFailed	= mscReturnInvalidAddr,	/** -1, Invalid address. Write to an address that is not Flash. */
	REEMember_locked		= mscReturnLocked,		/** -2, Flash address is locked. 								*/
	REEMember_timeOut	 	= mscReturnTimeOut,		/** -3, Timeout while writing to Flash. 						*/
	REEMember_unaligned		= mscReturnUnaligned,	/** -4  Unaligned access to Flash. 								*/
	REEMember_tooBig		= -5,					/** -5	Requested size over expected user data spec (signed page)*/
	REEMember_checkFailed	= -6,					/** -6	fletcher check mismatch									*/
	REEMember_unavailable	= -7,					/** -6	No available data */
}REEMember_t;


/*
 * Save data specified by pointer (up to a Flash-page of data) in the dedicated userData (UD) space
 * */
REEMember_t REEMember_save(uint8_t *data, size_t size);	/**/

size_t REEMember_load(uint8_t *data, size_t size);

REEMember_t REEMember_clear(void);


#endif /* INC_REEMEMBER_H_ */
