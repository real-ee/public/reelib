/*
 * REEUSB.h
 *
 *  Created on: Aug 20, 2019
 *      Author: jlecuyer2
 */

#ifndef REELIB_INC_REEUSB_H_
#define REELIB_INC_REEUSB_H_

#include <em_usb.h>

#if defined(USB_PRESENT) && (USB_COUNT == 1)

/*Largest byte exchange*/
#define REEUSB_MAX_SIZE 	(USB_FS_BULK_EP_MAXSIZE)


/***************************************************************************//**
 * @brief
 *  Usb data receive callback function.
 *
 * @details
 *  Called whenever the device receive a usb frame (notify thread to poll buffer).
 *
 ******************************************************************************/
typedef void (*REEUSB_onReceive_cb_t)(void);

/***************************************************************************//**
 * @brief
 *  Usb data transmit callback function.
 *
 * @details
 *  Called whenever the device succesfully transmitted a usb frame .
 *
 ******************************************************************************/
uint8_t  REEUSB_isBusy(void);
uint8_t REEUSB_isReady(void);
uint8_t REEUSB_isSuspended(void);

uint8_t REEUSB_isConnected(void);

uint16_t REEUSB_tx(uint8_t *buffer, uint16_t len);
uint16_t REEUSB_rx(uint8_t *buffer, uint16_t maxLen);

/*You may force re-enumeration by calling disconnect, waiting 1sec, and calling connect*/
void REEUSB_clear(void);
void REEUSB_connect(void);
void REEUSB_disconnect(void);

bool REEUSB_txBufferFull(void);
bool REEUSB_rxBufferEmpty(void);
uint16_t REEUSB_rxBufferQueued(void);
uint16_t REEUSB_txBufferAvailable(void);
uint16_t REEUSB_txBufferSize(void);
uint16_t REEUSB_rxBufferSize(void);
bool REEUSB_statGetRxRate(uint16_t *tx_kbps, uint16_t * rx_kbps);
bool REEUSB_wakeUp(void);

void cdcInit(void);
int  cdcSetupCmd(const USB_Setup_TypeDef *setup);
void cdcStateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);
void cdcUsbReset(void);

void REEUSB_init(REEUSB_onReceive_cb_t onReceive_cb);


#endif

#endif /* REELIB_INC_REEUSB_H_ */
