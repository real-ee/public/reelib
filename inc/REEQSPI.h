/*
	\name		REEQSPI.h
	\author		SBosse
	\version	1.0.0
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef REEQSPI_H_
#define	REEQSPI_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include "em_qspi.h"


/* -------------------------------- +
|									|
|	Config							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
#if defined(QSPI_COUNT) && QSPI_COUNT

typedef QSPI_TransferType_TypeDef REEQSPI_Transfer_t;
typedef QSPI_StigCmd_TypeDef REEQSPI_StigCmd_t;

/* REEQSPI error code */
typedef enum {
	REEQSPI_success =		0,
	REEQSPI_fail =			-1,
	REEQSPI_timeout =		-2,
	REEQSPI_collision =		-3,
	REEQSPI_nack =			-4,
	REEQSPI_queue_full =	-5,
	REEQSPI_param =			-127,
	REEQSPI_null =			-128,
}REEQSPI_err_t;

typedef struct {
	uint8_t dummyCycles; /* Number of dummy clock cycles required by device for read instruction. */
	uint8_t opCode; /* Read Opcode to use when not in XIP mode */
	REEQSPI_Transfer_t instTransfer; /* Intruction Type 0: Single mode. 1: Dual mode. 2: Quad mode. 3: Octal mode. T */
	REEQSPI_Transfer_t addrTransfer; /* Address Transfer Type for Standard SPI Modes. 0: Single mode. 1: Dual mode. 2: Quad mode. 3: Octal mode. T */
	REEQSPI_Transfer_t dataTransfer; /* Data Transfer Type for Standard SPI Modes. 0: Single mode. 1: Dual mode. 2: Quad mode. 3: Octal mode. T */
} REEQSPI_ReadConfig_t;

typedef struct {
	uint8_t dummyCycles; /* Number of dummy clock cycles required by device for write instruction. */
	uint8_t opCode; /* Write Opcode */
	REEQSPI_Transfer_t addrTransfer; /* Address Transfer Type for Standard SPI Modes. 0: Single mode. 1: Dual mode. 2: Quad mode. 3: Octal mode. T */
	REEQSPI_Transfer_t dataTransfer; /* Data Transfer Type for Standard SPI Modes. 0: Single mode. 1: Dual mode. 2: Quad mode. 3: Octal mode. T */
	bool autoWEL; /*  Auto issuing a WEL Command before write operation for DAC or INDAC */
} REEQSPI_WriteConfig_t;

/* Helper enum to set the memSize field */
typedef struct {
	uint8_t opCode; /* Opcode issued when polling after a direct write */
	uint8_t	pollCount; /* Number of times the controller should expect to see a true result from the polling.*/
	bool disablePolling; /* This switches off the automatic polling function */
	bool enablePollingExp;
} REEQSPI_WriteCompletionConfig_t;

/* Helper enum to set the memSize field */
typedef enum {
	REEQSPI_memSize64MB = 0,
	REEQSPI_memSize1Gb  = 1,
	REEQSPI_memSize2Gb  = 2,
	REEQSPI_memSize4Gb  = 3
} REEQSPI_MemSize_t;

/* Helper enum to set the blockSize field . 2^REEQSPI_blockSizeXXXX
 * represents the total bytes per block */
typedef enum {
	REEQSPI_blockSize32k  = 15,
	REEQSPI_blockSize64K  = 16,
	REEQSPI_blockSize128k = 17,
	REEQSPI_blockSize256k = 18
} REEQSPI_BlockSize_t;

/* Helper structure to configure the Device Size Configuration register */
typedef struct {
	REEQSPI_MemSize_t memSizeCS0; /* Size of Flash Device Connected to CS[0] Pin */
	REEQSPI_MemSize_t memSizeCS1; /*  Size of Flash Device Connected to CS[1] Pin */
	REEQSPI_BlockSize_t bytesPerBlock; /* Number of bytes per Block. Must be a power of 2 number */
	uint16_t bytesPerPage; /* Number of bytes per device page*/
	uint8_t	numAddressBytes; /* Number of bytes representing an address. 0 indicate 1 byte*/
} REEQSPI_DeviceSizeConfig_t;

/* Helper structure to configure Device Delay Register */
typedef struct {
	uint8_t phyTxDll; /* This field determines the number of delay elements to insert on RX clock. */
	uint8_t phyRxDll; /*  This field determines the number of delay elements to insert on TX clock. */
} REEQSPI_PhyConfig_t;

/* Helper structure to configure PHY configuration */
typedef struct {
	uint8_t dnss;  /*Clock Delay for Chip Select Deassert */
	uint8_t dbtwn; /* Clock Delay Between Two Chip Selects */
	uint8_t dafter; /*  Clock Delay for Last Transaction Bit */
	uint8_t dinit; /*  Clock Delay for CS */
} REEQSPI_DevDelayConfig_t;

/*
 * REEQSPI_access_direct : The QSPI controller is access in direct mode (DAC)
 * REEQSPI_access_indirect : The QSPI controller is access in indirect mode (INDAC)
 * */
typedef enum {
	REEQSPI_access_DAC	   =   1,
	REEQSPI_access_INDAC   =   2,
} REEQSPI_access_mode;

/*
 * REEQSPI_act_stigCmd : Send a Software Triggered Instruction Generator
 * REEQSPI_act_direct_write : Execute a direct write to the external flash
 * REEQSPI_act_direct_read : Execute a direct read from the external flash
 * */
typedef enum {
	REEQSPI_act_stigCmd		   =   1,
	REEQSPI_act_direct_write   =   2,
	REEQSPI_act_direct_read    =   3,
	REEQSPI_act_indirect_write =   4,
	REEQSPI_act_indirect_read  =   5,
}REEQSPI_ioctl_actionList_t;

typedef struct {
	uint32_t address;
	uint32_t size;
	uint8_t * buffer;
} REEQSPI_io_t;

typedef struct {
	REEQSPI_ioctl_actionList_t action;
	union {
		REEQSPI_StigCmd_t * stigCmd;
		REEQSPI_io_t * 		io;
	};
} REEQSPI_ioctl_t;

/* Specific configuration interface for the memory */
typedef struct {
	const REEQSPI_ReadConfig_t * readConfig; /* Setup the direct read */
	const REEQSPI_WriteConfig_t* writeConfig; /* Setup the direct write */
	const REEQSPI_DeviceSizeConfig_t * deviceSizeConfig; /* Setup the device size config to match the connected device*/
	const REEQSPI_WriteCompletionConfig_t * writeCompletionConfig;
	const REEQSPI_DevDelayConfig_t * devDelayConfig;
	const REEQSPI_PhyConfig_t * phyConfig; /* Setup the Phy configuration register and enable PHY mode */
} REEQSPI_MemoryInterface_t;

/* QSPI handle (the owner of the peripheral should have this) */
typedef struct {
	struct {
		QSPI_TypeDef * dev;
		uint8_t	CS0;			/* CS0 pin encoded with REEIO() */
		uint8_t	CS1;			/* CS1 pin encoded with REEIO() */
		uint8_t	DQ0;			/* DQ0 pin encoded with REEIO() */
		uint8_t	DQ1;			/* DQ1 pin encoded with REEIO() */
		uint8_t	DQ2;			/* DQ2 pin encoded with REEIO() */
		uint8_t	DQ3;			/* DQ3 pin encoded with REEIO() */
		uint8_t	SCK;			/* SCK pin encoded with REEIO() */
		uint8_t	QSPIlocation;	/* Port location of QSPI peripheral */
		uint8_t csLine; 		/* Peripheral Chip Select Line */
		uint8_t divisor; 		/* Master mode baude rate divisor. Value between [2 - 32] inclusive */
		REEQSPI_access_mode accessMode; /*DAC : 1, INDAC : 2*/
		bool	blocking;		/* INDAC only. Whether action RW should block or not */
	}ctl;
	REEQSPI_MemoryInterface_t  mem;
} REEQSPI_t;

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */

/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */


/*
	\brief	Function to call by thread to takes control of the hardware.
	\note	TODO: Not thread safe yet
	\usage
	\param	REESPI_t * qspi				REEQSPI peripheral handle
	\return	REEQSPI_err_t 				Code
*/
REEQSPI_err_t REEQSPI_take(REEQSPI_t * qspi);


/*
	\brief	Function to call by thread to release control of the hardware.
	\note	TODO: Not thread safe yet
	\usage
	\param	REESPI_t * qspi				REEQSPI peripheral handle
	\return	REEQSPI_err_t 				Code
*/
REEQSPI_err_t REEQSPI_release(REEQSPI_t * qspi);


/*
	\brief	Function to call in order to execute action with the peripheral
	\note	TODO: Not thread safe yet
	\usage
	\param	REESPI_t * qspi				REEQSPI peripheral handle
	\param  REEQSPI_ioctl_t * arg		Arguments for the executed action
	\return	REEQSPI_err_t 				Code
*/
REEQSPI_err_t REEQSPI_ioctl(REEQSPI_t * qspi, REEQSPI_ioctl_t * arg);

/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */

#endif	/* QSPI_COUNT */


#ifdef __cplusplus
}
#endif
#endif	/* REESPI_H_ */

