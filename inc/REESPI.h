/*
	\name		REESPI.h
	\author		JLecuyer
	\version	1.0.0
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef REESPI_H_
#define	REESPI_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>						/* Because stdint mofo */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* Universal types */
#include <REEutil.h>					/* Utilities */
#ifdef REELIB_USE_CMSISRTOS2
#include <cmsis_os2.h>					/* RTOS API */
#endif
#include <em_chip.h>					/* Chip's register definitions */

#include <string.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <em_usart.h>


/* -------------------------------- +
|									|
|	Config							|
|									|
+ -------------------------------- */
#ifndef REESPI_RXTIMEOUT_MS
	#define	REESPI_RXTIMEOUT_MS		(1)		/* Period to receive a character, after which error is thrown */
#endif


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */

/* SPI handle (the owner of the peripheral should have this) */
/* REESPI control block */
typedef struct {
	bool					master;			/* spi operation mode (master/slave) */

	#ifdef CAN_PRESENT
	USART_TypeDef *			spidev;			/* Pointer to the CAN peripheral register block. */
	#endif

	uint8_t					locationTx;		/* Port location of tx signal */
	uint8_t					locationRx;		/* Port location of rx signal */
	uint8_t					locationSck;	/* Port location of sckrx signal */
	uint8_t					locationSlaveCS;/* Port location of cs signal (If device is slave) */
	uint8_t					hookQty;		/* Number of hook pointed by hookList*/

	uint32_t				maxWaitTime_ms;	/* max wait time if device/mutex isn't available */
	uint32_t				bitrate;		/* SPI operating bitrate (in bps) */
	uint16_t				buffer_tx_len;
	uint16_t				buffer_rx_len;
	bool					non_blocking;	/* Operation return when completed*/
}REESPI_t;





/* Sequence type (for convience) */
//typedef I2C_TransferSeq_TypeDef REEI2C_Sequence_t;

/* REESPI lib error code */
typedef enum {
	REESPI_success =		0,
	REESPI_fail =			-1,
	REESPI_timeout =		-2,
	REESPI_collision =		-3,
	REESPI_nack =			-4,
	REESPI_full =			-5,
	REESPI_notFound =		-6,
	REESPI_denied =			-7,

	REESPI_param =			-127,
	REESPI_null =			-128,
}REESPI_err_t;




typedef enum {
	REESPI_ioctl_flush		= 0, /* Clear/flush the slave reply buffer	*/
	REESPI_ioctl_set 		= 1, /* Set the slave reply buffer			*/
	REESPI_ioctl_append 	= 2, /* Append to the slave reply buffer	*/
	REESPI_ioclt_slave_cs	= 3, /* Set the Slave chip_select*/
}REESPI_ioctl_type_t;

typedef struct {
	REESPI_ioctl_type_t type;	/* Type of IOCTL*/
	uint8_t *data;				/* Datapointer for the IOTCL*/
}REESPI_ioctl_t;


typedef enum {
	REESPI_hook_cs_taken 		= 0,	/* Chip select  activated 	*/
	REESPI_hook_cs_released 	= 1,	/* Chip select released		*/

	REESPI_hook_first 			= 2,	/* First byte exchanged		*/
	REESPI_hook_last 			= 3,	/* Last byte exchanged		*/

	REESPI_hook_tx_empty 		= 4,	/* Transmit buffer empty				*/
	REESPI_hook_tx_ready 		= 5,	/* Transmit buffer ready for new byte	*/

	REESPI_hook_rx_ready		= 6,	/* Rx buffer ready*/
}REESPI_hook_type_t;


typedef struct {
	REESPI_hook_type_t type;
}REESPI_hook_t;

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
REESPI_err_t REESPI_open(REESPI_t * SPI);
REESPI_err_t REESPI_close(REESPI_t * SPI);

uint32_t REESPI_tx(REESPI_t * SPI, void * src, uint32_t len);
uint32_t REESPI_rx(REESPI_t * SPI, void * dst, uint32_t len);
uint32_t REESPI_trx(REESPI_t * SPI, void * src, void * dst, uint32_t len);

REESPI_err_t REESPI_ioctl(REESPI_t * SPI, REESPI_ioctl_t * args);

REESPI_err_t REESPI_hook(REESPI_t * SPI, REESPI_hook_t * hook, void(*REEcb_t)(void *));
REESPI_err_t REESPI_unhook(REESPI_t * SPI, REESPI_hook_t * hook, void(*REEcb_t)(void *));



















/*
	\brief	Initialize and configure an USART as a SPI hw peripheral
	\note	Ready to operate when this function return
	\usage	The "owner" of the peripheral should create a REESPI_t variable with
			the desired configuration and pass it to the lib. Hold on to that variable as
			it's the main control struct for the lib and it's uses for all function call.
			Use only REESPI API function to access the handle AFTER initalization
	\param	REESPI_t * SPI				SPI peripheral handle
	\return	REESPI_err_t status				REESPI_success if everything went well
												REESPI_timeout if no response
												REESPI_null if given a NULL pointer
												REESPI_fail if general failure
*/
REESPI_err_t REESPI_Init(REESPI_t * SPI);

/*
	\brief	Data transmission of an array of bytes
	\note	Blocking, but uses CMSIS RTOS sleeping if available (only the calling thread will be blocked)
	\usage
	\param	REESPI_t * SPI				SPI peripheral handle
			uint8_t * src						Pointer to the data to send
			uint16_t len						Lenght of the data to send
	\return	uint16_t count						Actual number of byte transfered
*/
//uint16_t REESPI_Send(REESPI_t * SPI, uint8_t * src, uint16_t len);


//uint16_t REESPI_Receive(REESPI_t * SPI, uint8_t * dst, uint16_t len);

uint16_t REESPI_GetRxPending(REESPI_t * SPI);

/*
 * Return available space in transmit buffer
 * */
uint16_t REESPI_GetTxAvailable(REESPI_t * SPI);


/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */



#ifdef __cplusplus
}
#endif
#endif	/* REESPI_H_ */

