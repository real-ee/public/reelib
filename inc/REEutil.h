/*
*	\name		REEutil.h
*	\author		Laurence DV
*	\version	1.3.0
*	\brief		General utilities and macro tools
*	\note
*	\warning	None
*	\license	All right reserved RealEE inc. (2019)
*/

#ifndef __REEUTIL_H__
#define __REEUTIL_H__

#include <stddef.h>
#include <stdint.h>

#ifdef REELIB_USE_CMSISRTOS2
#include <cmsis_os2.h>
#endif

/* -------------------------------- +
|									|
|	Define							|
|									|
+ -------------------------------- */
#ifndef BIT0
#define	BIT0		(1 << 0)
#define	BIT1		(1 << 1)
#define	BIT2		(1 << 2)
#define	BIT3		(1 << 3)
#define	BIT4		(1 << 4)
#define	BIT5		(1 << 5)
#define	BIT6		(1 << 6)
#define	BIT7		(1 << 7)
#define	BIT8		(1 << 8)
#define	BIT9		(1 << 9)
#define	BIT10		(1 << 10)
#define	BIT11		(1 << 11)
#define	BIT12		(1 << 12)
#define	BIT13		(1 << 13)
#define	BIT14		(1 << 14)
#define	BIT15		(1 << 15)
#define	BIT16		(1 << 16)
#define	BIT17		(1 << 17)
#define	BIT18		(1 << 18)
#define	BIT19		(1 << 19)
#define	BIT20		(1 << 20)
#define	BIT21		(1 << 21)
#define	BIT22		(1 << 22)
#define	BIT23		(1 << 23)
#define	BIT24		(1 << 24)
#define	BIT25		(1 << 25)
#define	BIT26		(1 << 26)
#define	BIT27		(1 << 27)
#define	BIT28		(1 << 28)
#define	BIT29		(1 << 29)
#define	BIT30		(1 << 30)
#define	BIT31		(1 << 31)
#define	BIT32		(1 << 32)
#define	BIT33		(1 << 33)
#define	BIT34		(1 << 34)
#define	BIT35		(1 << 35)
#define	BIT36		(1 << 36)
#define	BIT37		(1 << 37)
#define	BIT38		(1 << 38)
#define	BIT39		(1 << 39)
#define	BIT40		(1 << 40)
#define	BIT41		(1 << 41)
#define	BIT42		(1 << 42)
#define	BIT43		(1 << 43)
#define	BIT44		(1 << 44)
#define	BIT45		(1 << 45)
#define	BIT46		(1 << 46)
#define	BIT47		(1 << 47)
#define	BIT48		(1 << 48)
#define	BIT49		(1 << 49)
#define	BIT50		(1 << 50)
#define	BIT51		(1 << 51)
#define	BIT52		(1 << 52)
#define	BIT53		(1 << 53)
#define	BIT54		(1 << 54)
#define	BIT55		(1 << 55)
#define	BIT56		(1 << 56)
#define	BIT57		(1 << 57)
#define	BIT58		(1 << 58)
#define	BIT59		(1 << 59)
#define	BIT60		(1 << 60)
#define	BIT61		(1 << 61)
#define	BIT62		(1 << 62)
#define	BIT63		(1 << 63)
#define	BIT69		("hehehe")
#endif


/*--------------------------------- +
|									|
|	Data manipulation macro			|
|									|
+ -------------------------------- */
#define __XCAT(a,b)						(a##b)
#define	CONCAT(a,b)						(__XCAT(a,b))								/* concatenate 2 preprocessor string */
#define CONCAT3(a,b,c)					(__XCAT(__XCAT(a,b),c))						/* concatenate 3 preprocessor string */
#define	CONCAT4(a,b,c,d)				(__XCAT(__XCAT(a,b),__XCAT(c,d)))			/* concatenate 4 preprocessor string */
#define SWAP(a, b)						(((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))	/* swap in memory value a with value b */
#define FORCE_BIT(target, mask, value)	((target) ^= ((value) ^ (target)) & (mask))	/* force the value on the target through the masked bits */
#define SET_BIT(target, mask)			((target) |= (mask))						/* force the masked bit to 1 */
#define CLR_BIT(target, mask)			((target) &= ~(mask))						/* force the masked bit to 0 */
#define TOG_BIT(target, mask)			((target) ^= (mask))						/* inverse the masked bit value */
#define GET_BIT(target, offset)			((target>>offset)&0x01)						/* return the bit designated by the offset*/


/*--------------------------------- +
|									|
|	Arithmetic macro				|
|									|
+ -------------------------------- */

/*
 * Type-safe macro function with no side-effect.
 *  For example, if you call MIN(x++, y++) the preprocessor will generate the following code (((x++) < (y++)) ? (x++) : (y++)).
 *  So, x and y will be incremented twice. this won't happen with those call :)  */
#define	MAX(a,b)						__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); (_a > _b) ? _a : _b;})		/* taken from em_common.h, protect again */
#define	MIN(a,b)						__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); (_a < _b) ? _a : _b;})		/* taken from em_common.h */
#define	ABS(a)							__extension__({__typeof__(a) _a = (a); ((_a) > 0) ? (_a) : (-_a);})																	/* Absolute value of a number */
#define ABSDELTA(a,b)					__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); (_a)>(_b) ? (_a-_b) : (_b-_a);})																/* Absolute difference between two number */



/*
 * Change the scope of a given value from U8 to given scope
 * example:
 * 		REESCOPE_FROM_U8(x,0,100)
 * 		The scope of U8 is 0..255, when REESCOPED within 0..100
 * 		0->0
 * 		63->25
 * 		127->50
 * 		191->75
 * 		255->100
 * */
#define	REESCOPE_FROM_U8(u8, 	sMin, sMax)	(((uint16_t)sMin)+ ((((uint16_t)(sMax)-(sMin))*((uint16_t)u8))>>8))
#define	REESCOPE_FROM_U16(u16, 	sMin, sMax)	(((uint32_t)sMin)+ ((((uint32_t)(sMax)-(sMin))*((uint32_t)u16))>>16))

/*
 * Change the scope of a given value fom given scope to U16
 * example:
 * 		REESCOPE_TO_U8(x, xMin, xMax)
 * 		The scope of x is xMin..xMax, when REESCOPED to U8 0..255
 * 		x<=x1  		-> 0
 * 		x>=x2		-> 255
 * 		x1..x..x2	-> 0..u..255
 * */
uint8_t REESCOPE_TO_U8(uint16_t x, uint16_t x1, uint16_t x2);
uint16_t REESCOPE_TO_U16(uint16_t x, uint16_t x1, uint16_t x2);






/*--------------------------------- +
|									|
|	Execution macro					|
|									|
+ -------------------------------- */
#define nop()							__NOP()  								/* NOP instruction call __ASM("nop")  */
#define	Nop()							(nop())
#define	NOP()							(nop())
#define	EATJOULES(howMany)				(while((--((int32_t)howMany)) > 0))			/* Uses the CPU as an entropy creation device using energy to do absolutely nothing useful. (aka delay) */
																					/* WARNING: By using this function you are moraly obligated to write a comment using "nom nom"
																					 * 	or "rob" describing why you didn't use an rtos service for your particular usage, not a joke */
#define	REEbreak()						__BKPT(1)									/* Add a breakpoint */
#define	REEBreak()						(REEbreak())
#define	REEBREAK()						(REEbreak())
#define	reebreak()						(REEbreak())
#define	reeBreak()						(REEbreak())


/*--------------------------------- +
|									|
|	GPIO Tricks						|
|									|
+ -------------------------------- */

#define reeio_t                     uint8_t
#define	REEIO(port, pin)			((uint8_t)(((port)<<5) + ((pin)&0x1F)))
#define	REEIO_PORT(gpio)			((uint8_t)((gpio)>>5))
#define	REEIO_PIN(gpio)				((uint8_t)((gpio)&0x1F))


/*--------------------------------- +
|									|
|	SYSTICK Handler					|
|									|
+ -------------------------------- */

/* Return actual systick value (ms since boot)*/
#define systickGet()  			(osKernelGetTickCount())

/* Return the ms since the given timestamp in ms */
uint32_t systickDelta(uint32_t t0);


// Return a value bound by min and max, indexed by i/range;
//	eg:	50 == scale_u8_to_range(128, 0,   100)
//	eg:	50 == scale_u8_to_range(128, 100, 0)
#define	scale_u8_to_range(i, min, max)	(((uint16_t)min)+ ((((uint16_t)(max)-(min))*((uint16_t)i))>>8))
#define	scale_u16_to_range(i, min, max)	(((uint32_t)min)+ ((((uint32_t)(max)-(min))*((uint32_t)i))>>16))


// Return a value bound by 0 and U8MAX/U16MAX,
// eg: scale_range_to_u16(5, 0, 10)		--> UINT16_MAX/2
// eg: scale_range_to_u16(100, 50, 100)	--> UINT16_MAX
// eg: scale_range_to_u16(75, 75, 100)	--> 0
// Basically UINT16_MAX * (x-x1) / (x2-x1)
uint16_t scale_range_to_u16(uint16_t x, uint16_t x1, uint16_t x2);


////////////////////////////////////////////////////////////////////////////////
// Interpolation with 2 point
//		|
// y1	|*******(P1)
//		|		*
//		|			*
//		|			*
//		|				*
// y2	|				(P2)**********
//		|_______________________________
//				x1		x2
//
uint16_t interpolation_2p(uint16_t x, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2);

uint16_t interpolation_4p(uint16_t x, uint16_t x1, uint16_t x2, uint16_t x3, uint16_t x4, uint16_t y1, uint16_t y2, uint16_t y3, uint16_t y4);



/*--------------------------------- +
|									|
|	Warning sushing					|
|	Sadly those cant be macros...	|
+ -------------------------------- */
/* To stop warning for unused function and parameters */
/* For others see: https://gcc.gnu.org/onlinedocs/gcc-4.1.2/gcc/Warning-Options.html */

/* To ignore : */
/* #pragma GCC diagnostic ignored "-Wunused-function" */
/* #pragma GCC diagnostic ignored "-Wunused-parameter" */

/* To re-enable: */
/* #pragma GCC diagnostic warning "-Wunused-function" */
/* #pragma GCC diagnostic warning "-Wunused-parameter" */


#define BASE_32_BASE		(32)
#define BASE32_MAX_LEN 		(13)	/* log(2**64) / log(32) = 12.8 => max 13 char */
static const char BASE32[BASE_32_BASE] = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";	/*Alphanum w/o  {I,L,O,U} */

size_t u64ToBase32_encode(char *ptr, uint64_t number, size_t max_len);
int snprintf_try(char * ptr, size_t size, char *fmt, ...);


#endif

