/*
	@name		REEF.h
	@author		Laurence DV
	@version	2.0.0
	@brief		REEF Human Interface, enable simple human interaction within an CMSIS RTOS
				through peripherals, currently supported:
					* Muxed momentary push-button	(NOT PORTED TO REEF2)
					* Muxed LEDs					(NOT PORTED TO REEF2)
					* Muxed RGB LEDs				(NOT PORTED TO REEF2)
					* RGB Addressable string		(NOT PORTED TO REEF2)
					* OLED matrix display			(NOT IMPLEMENTED)
				This file is the core definition file for the REEF system, all code using any part of REEF
				should also include this header in addition to the specific part it is using (ex: REEFled.h)
	@note		
	@license	SEE $REElib_root/README.md
*/
#ifndef __REEF_H_
#define	__REEF_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>
#include <conf.h>
#include <REEtype.h>
#include <REEutil.h>
#include <REEcolor.h>



/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
typedef enum REEFcoralType_e{
	REEFcoral_LED,
	REEFcoral_BTN,
	REEFcoral_RGB,
	REEFcoral_RGBW,
	REEFcoral_RGBstring,
	REEFcoral_RGBWstring,
	REEFcoral_OLEDdispBW,
	REEFcoral_OLEDdispRGB,
	REEFcoral_LCDdispRGBILI1193,
}REEFcoralType_t;

/* REEF coral static configuration, used by project specifics functions */
typedef struct REEFcoralConf_s{
	const REEFcoralType_t	type;		/* Type of this coral */
	void * const			hw;			/* HW module register address (coral type specific) */
	void * const			hwConf;		/* Hw module configuration  */
	void * const			ioConf;		/* coral type specific io configuration, see REEFxxx_ioConf_t for specifics */
}REEFcoralConf_t;

/* REEF coral object definition, used by sub-section of REEF, must be public */
typedef struct __attribute__((packed)) REEFcoral_s {
	const REEFcoralConf_t *	conf;
	REEcode_t 				(*init)(void);
	REEcode_t 				(*update)(void);
	REEcode_t 				(*deinit)(void);
}REEFcoral_t;









/* Standard REEF pixel operating mode */
typedef enum REEFpixMode_e{
	REEFpixOff =	0x00,
	REEFpixOn =		0x01,
}REEFpixMode_t;

/* Common REEF pixel attributes */
/* This must be present in all pixel sub-lib custom type */
typedef struct __attribute__((packed)) REEFpix_s{
	REEFpixMode_t	mode;		/* Current operating mode of this pixel */
	uint8_t			alpha;		/* Current alpha level of this pixel */
}REEFpix_t;

typedef enum REEFinputType_e {
	REEFinputNone =			0xFF,

	REEFinputDataVal =		0x11,		/* pointer, mask and value style */
	REEFinputRevCB =		0x12,		/* reverse callback (return a fct pointer to execute when you want to trig the input) */
	REEFinputPRS =			0x81,		/* PRS signal trigger */
}REEFinputType_t;

typedef enum REEFoutputType_e {
	REEFoutputNone =		0xFF,

	REEFoutput1D1C = 		0x11,		/* 1 dimension 1 color */
	REEFoutput1D2C =		0x12,		/* 1 dimension 2 colors */
	REEFoutput1D3C =		0x13,		/* 1 dimension 3 colors */
	REEFoutput1D4C =		0x14,		/* etc... */
	REEFoutput2D1C =		0x21,
	REEFoutput2D2C =		0x22,
	REEFoutput2D3C =		0x23,
	REEFoutput2D4C =		0x24,
	REEFoutput3D1C =		0x31,
	REEFoutput3D2C =		0x32,
	REEFoutput3D3C =		0x33,
	REEFoutput3D4C =		0x34,
}REEFoutputType_t;

typedef struct __attribute__((packed)) REEFinput_s{
	REEFinputType_t			type;
}REEFinput_t;

typedef struct __attribute__((packed)) REEFoutput_s{
	REEFoutputType_t		type;
}REEFoutput_t;

typedef enum REEFflag_e {
	REEFflag_ALL =			0xEFFFFFFF,
	REEFflag_stop =			0x00000001,
	REEFflag_newFrame =		0x00000100,
}REEFflag_t;


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define any of these in conf.h,	|
|	If not defined, these default 	|
|	will be used instead			|
+ -------------------------------- */
#define		REEF_INITIAL_FPS_DEF		(30)
#define		REEF_CORAL_TOTALNB_DEF		(8)


/* -------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Public Global					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Public BareMetal API			|
|									|
+ -------------------------------- */
/*
	@brief	Configure and initialize all the necessary peripherals
	@note	
	@param	void
	@return	void
*/
void REEF_init(void);

/*
	@brief	Execute, render, display, pulse, all-the-things a new frame
	@note	Used automatically by the REEF thread when using RTOS API
	@param	void
	@return	void
*/
void REEF_newFrame(void);


/* -------------------------------- +
|									|
|	Public CMSIS-RTOS API			|
|									|
+ -------------------------------- */
/*
	@brief	Launch the REEF thread in a CMSIS-RTOS2 with static config
	@note	This is the only needed call to start REEF in a CMSIS-RTOS
	@param	void
	@return	void
*/
void REEF_startThread(void);



/* -------------------------------- +
|									|
|	Public ISR						|
|	(wtf would be a private one...)	|
+ -------------------------------- */


#ifdef __cplusplus
}
#endif
#endif	/* __REEF_H_ */
