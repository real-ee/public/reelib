/*
	\name		REEbuf.h
	\author		Laurence DV
	\version	1.0.0
	\brief		Buffer type, functions and utilities
	\note		
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef __REEBUF_H__
#define __REEBUF_H__
/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEutil.h>					/* For bit def and util */


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define those before you include	|
|	this file, if you want to		|
|	customize them					|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Ring Buffer						|
|									|
+ -------------------------------- */

#endif

