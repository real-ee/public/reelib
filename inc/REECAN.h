/*
    @name       REECAN.h
    @version    2.0.0
    @note       CAN Peripheral lib
    @license    SEE $REElib_root/LICENSE.md
*/
#ifndef REECAN_H_
#define REECAN_H_   1

#ifdef __cplusplus
    extern "C" {
#endif


#include <em_device.h>      /* For registers definitions */
#ifdef CAN_PRESENT


/* -------------------------------- +
|                                   |
|   Include                         |
|                                   |
+ ---------------------------------*/

#include <hardware.h>                   /* Global hardware configuration file */
#include <conf.h>                       /* Global configuration file */
#include <REEtype.h>                    /* General Type, ready to serve! */
#include <REEutil.h>                    /* For bit def and util */
#include <stdio.h>
#include <em_can.h>


/* -------------------------------- +
|   !WARNING!                       |
|   Default Config                  |
|   Define any of these in conf.h,  |
|   If not defined, these default   |
|   will be used instead            |
+ -------------------------------- */


/* -------------------------------- +
|                                   |
|   Type                            |
|                                   |
+ ---------------------------------*/

/* REECAN message format */
typedef struct {
    uint8_t         canIntf;            /* 0: CAN0, 1:CAN1      */
    uint32_t        id;                 /* ID of the message with 11 bits (standard) or 28 bits (extended). LSBs are used for both. */
    uint32_t        mask;               /* A mask for ID filtering. */
    uint8_t         data[8];            /* A pointer to data, [0 - 8] bytes. */
    struct {
        uint8_t     dlc             :4; /* Data Length Code [0 - 8]. */
        uint8_t     msgNum          :5; /* msgNum A message number of this Message Object, [1 - 32]. */
        bool        direction_tx    :1; /* Direction of the message, transmit if true, receive if false */
        bool        extended        :1; /* ID extended if true, standard if false. */
        bool        extendedMask    :1; /* Enable the use of 'extended' value for filtering. */
        bool        remoteTransfer  :1; /* True if the Message Object is used for remote transmission, false otherwise. */
        bool        endOfBuffer     :1; /* True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */
        bool        useMask         :1; /* A boolean to choose whether or not to use the masks. */
    };
}REECAN_messageObject_t;

/*  REECAN_callback -> type for callback pointer used for function hook */
typedef void(*REECAN_callbackMsg)(void);
typedef void(*REECAN_callbackLec)(uint32_t);

/* REECAN control block */
typedef struct {
    #ifdef CAN_PRESENT
    CAN_TypeDef *           candev;         /* Pointer to the CAN peripheral register block.                  */
    #endif

    uint32_t                bitrate;        /* CAN operating bitrate (in bps)                                 */
    uint32_t                maxWaitTime_ms;
    uint8_t                 canIntf;        /* Integer representation of the CAN interface [0:CAN0, 1:CAN1]   */
    uint8_t                 tx;             /* tx pin encoded with REEIO()          */
    uint8_t                 rx;             /* rx pin encoded with REEIO()          */
    uint8_t                 locationTx;     /* Port location of tx signal           */
    uint8_t                 locationRx;     /* Port location of rx signal           */
    bool txEnable;                          /* Enable message transmission, false=SilentMode            */
    bool txAutoRetry;                       /* Enable Automatic retransmission on failure               */

    uint32_t           rx_filters[32];
    REECAN_callbackMsg onReceiveMsg;              /* Called when msg is received            */
    REECAN_callbackMsg onTransmitMsg;             /* Called when msg is sent                */
    REECAN_callbackLec onReceiveErr;              /* Called when msg reception failed       */
    REECAN_callbackLec onTransmitErr;              /* Called when msg transmission failed    */
}REECAN_t;


/* REECAN error code */
typedef enum {
    REECAN_success =         0,
    REECAN_fail =           -1,
    REECAN_timeout =        -2,
    REECAN_collision =      -3,
    REECAN_nack =           -4,
    REECAN_full =           -5,
    REECAN_notFound =       -6,
    REECAN_denied =         -7,
    REECAN_empty =          -8,
    REECAN_busy=            -9,

    REECAN_param =          -127,
    REECAN_null =           -128,
}REECAN_err_t;


/* -------------------------------- +
|                                   |
|   Constant                        |
|                                   |
+ ---------------------------------*/


/* -------------------------------- +
|                                   |
|   Global                          |
|                                   |
+ ---------------------------------*/


/* -------------------------------- +
|                                   |
|   API                             |
|                                   |
+ ---------------------------------*/
/*
    @brief  Try to take/release exclusive control of an CAN hw peripheral
    @note   This function will block if $can.ctl.blocking == TRUE, otherwise it
            will return immediately
    @usage  Useful to ensure multiple calls of tx/rx will be sequencial on the hw bus

    @param  REECAN_t * can                      CAN virtual peripheral handle
    @return REECAN_err_t status                 REECAN_success if everything went well
                                                REECAN_null if CAN is NULL
                                                REECAN_param is invalid parameters
                                                REECAN_fail if couldn't take/release the hw
*/
REECAN_err_t REECAN_open(REECAN_t * can);
REECAN_err_t REECAN_close(REECAN_t * can);

/*
    @brief  Send/Receive a transaction on the CAN bus
    @param  REECAN_t * can                      CAN virtual peripheral handle

    @param CAN_MessageObject_TypeDef *message   CAN message
    @return REECAN_err_t                        reecan return code
*/
REECAN_err_t REECAN_tx(REECAN_t * can, CAN_MessageObject_TypeDef *message, bool remoteTransfer);

REECAN_err_t REECAN_rx(REECAN_t * can, REECAN_messageObject_t * message);


REECAN_err_t REECAN_updateConfig(REECAN_t *can);
bool REECAN_isTxReady(REECAN_t *can);
void REECAN_printErrorCode(REECAN_err_t err);



#endif  /* CAN_PRESENT */

#ifdef __cplusplus
}
#endif
#endif  /* REECAN_H_ */

